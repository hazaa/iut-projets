# IUT-PROJETS 

Bienvenue sur cette page de ressources pour projets de l'IUT de Sénart-Fontainebleau.
Vous y trouverez un [dépot de fichiers](https://gitlab.com/hazaa/iut-projets/tree/master), dont les répertoires ont le contenu suivant:

*   code: des programmes dans différents langages (python,C,...) ef différents micro-contrôleurs et autres appareils utilisés (raspberry munie de caméra, moteurs,...)
*   docs: documents pédagogiques (sujets de TP, modèles de rapports, etc...) et [notices techniques répertoire](https://gitlab.com/hazaa/iut-projets/tree/master/docs)

Sommaire:

*    Projets [ER3](#er3), 2ème année.
*    Projets [ER2](#er2), 1ère année.
*    Module [MCTR](#mctr), 2ème année.

Ressources communes
------

Apprendre les bases de:

*    langage [Python](docs/tp-python.md)
*    langage [C](docs/tp-C.md) 
*    système d'exploitation [Unix/Linux](docs/tp-linux.md) 
*    coder un flottant, norme [IEEE754](docs/tp-ieee754.md)

# ER3 

Les sujets: 

*    2021-2022 : [Energimètre/Tableau de bord](docs/modbus/README.md)
*    2020-2021 : [Bilan carbone IUT](docs/tp-bilan-carbone.md) 
*    2018-2019 : [robot mobile](docs/mobile_robot2.md) (groupe A1), [seed watch](docs/seed_watch.md)(groupe B1). 
*    2017-2018 : [Convoyeur et tri visuel d'objets](docs/convoyeur.md)
*    (TODO:  [NILM](docs/nilm.md), [BLDC](docs/tp-motor-bldc.md) )

| Projet | Video |
| -------- | -------- | 
|  Convoyeur et tri visuel |  [![](https://peertube.mastodon.host/static/previews/4bd83389-688d-4063-a6c7-805330c10e11.jpg)]( https://peertube.mastodon.host/videos/watch/4bd83389-688d-4063-a6c7-805330c10e11 "Tri visuel") |
| Tracking visuel | [![](https://peertube.mastodon.host/static/previews/9b00ee6c-8a91-4a86-ac44-bf546138f6ea.jpg)]( https://peertube.mastodon.host/videos/watch/9b00ee6c-8a91-4a86-ac44-bf546138f6ea "Tracking visuel")   |


# ER2 

Les sujets :

*   2017-2018: [robot mobile](docs/mobile_robot.md) (groupe C1), [air hockey](docs/air-hockey.md) (groupe C2).
*   TP commun: [découpe laser](docs/lasercut.md)
*   2016-2017: alimentation ATX (groupe B2) pré-étude [pdf](https://gitlab.com/hazaa/iut-projets/tree/master/docs/2017_ER2_ALIMENTATION_ATX_1page_v1.pdf), [odt](https://gitlab.com/hazaa/iut-projets/tree/master/docs/2017_ER2_ALIMENTATION_ATX_1page_v1.odt), [documents techniques](docs/alimentation_atx.md),  bras robot MeArm (groupe C2): pré-étude [pdf](https://gitlab.com/hazaa/iut-projets/tree/master/docs/2017_ER2_MEARM_1page_v1.pdf), [odt](https://gitlab.com/hazaa/iut-projets/tree/master/docs/2017_ER2_MEARM_1page_v1.odt), [documents techniques](docs/me_arm.md)


---



# MCTR 

Parcours [SIRO](https://eprel.u-pec.fr/eprel/claroline/course/index.php?cid=4529): Système Informatique embarqué et RObotique mobile.
Concevoir, commander et évaluer un système embarqué.

[Module Complémentaire Temps Réel](docs/MCTR.md)




NOTE: les projets et TP sont réalisés avec des [logiciels libres](https://fr.wikipedia.org/wiki/Logiciel_libre)!
