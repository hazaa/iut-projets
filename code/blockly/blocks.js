'use strict';

Blockly.Blocks['spidey_move'] = {
    init: function() {
        this.setColour(0);
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage('spidey.png', 25, 25, ''))
            .appendField('Metabot avance');
        this.appendValueInput('X')
            .setCheck('Number')
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField('tout droit');
        this.appendValueInput("Y")
            .setCheck('Number')
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField('sur le coté');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('Metabot avance devant et sur le coté');
    }
};
Blockly.JavaScript['spidey_move'] = function(block) {
    var value_x = Blockly.JavaScript.valueToCode(
        block, 'X', Blockly.JavaScript.ORDER_ATOMIC) || '0';
    var value_y = Blockly.JavaScript.valueToCode(
        block, 'Y', Blockly.JavaScript.ORDER_ATOMIC) || '0';
    var code = 'metabot_move('+ value_x +', '+ value_y +');\n';
    return code;
};

Blockly.Blocks['spidey_rotation'] = {
    init: function() {
        this.setColour(0);
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage('spidey.png', 25, 25, ""))
            .appendField('Metabot tourne');
        this.appendValueInput("ANGLE")
            .setCheck('Number')
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField('vers la gauche');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('Metabot tourne sur lui même');
    }
};
Blockly.JavaScript['spidey_rotation'] = function(block) {
    var value_angle = Blockly.JavaScript.valueToCode(
        block, 'ANGLE', Blockly.JavaScript.ORDER_ATOMIC) || '0';
    var code = 'metabot_rotate('+ value_angle +');\n';
    return code;
};

Blockly.Blocks['spidey_control'] = {
    init: function() {
        this.setColour(0);
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage('spidey.png', 25, 25, ''))
            .appendField('Metabot avance et tourne');
        this.appendValueInput('X')
            .setCheck('Number')
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField('avance tout droit');
        this.appendValueInput("Y")
            .setCheck('Number')
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField('avance sur le coté');
        this.appendValueInput("A")
            .setCheck('Number')
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField('tourne sur lui même');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('Metabot avance et tourne');
    }
};
Blockly.JavaScript['spidey_control'] = function(block) {
    var value_x = Blockly.JavaScript.valueToCode(
        block, 'X', Blockly.JavaScript.ORDER_ATOMIC) || '0';
    var value_y = Blockly.JavaScript.valueToCode(
        block, 'Y', Blockly.JavaScript.ORDER_ATOMIC) || '0';
    var value_a = Blockly.JavaScript.valueToCode(
        block, 'A', Blockly.JavaScript.ORDER_ATOMIC) || '0';
    var code = 'metabot_move('+ value_x +', '+ value_y +');\n';
    code += 'metabot_rotate('+ value_a +');\n';
    return code;
};

Blockly.Blocks['spidey_height'] = {
    init: function() {
        this.setColour(0);
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage('spidey.png', 25, 25, ""))
            .appendField('Metabot hauteur');
        this.appendValueInput("HEIGHT")
            .setCheck('Number')
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField('entre 0 et 100');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('Metabot change la hauteur de son corps');
    }
};
Blockly.JavaScript['spidey_height'] = function(block) {
    var value_height = Blockly.JavaScript.valueToCode(
        block, 'HEIGHT', Blockly.JavaScript.ORDER_ATOMIC) || '70';
    var code = 'metabot_height('+ value_height +');\n';
    return code;
};

Blockly.Blocks['spidey_radius'] = {
    init: function() {
        this.setColour(0);
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage('spidey.png', 25, 25, ""))
            .appendField('Metabot rayon');
        this.appendValueInput("RADIUS")
            .setCheck('Number')
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField('entre 50 et 120');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('Metabot change le rayon ses pattes');
    }
};
Blockly.JavaScript['spidey_radius'] = function(block) {
    var value_radius = Blockly.JavaScript.valueToCode(
        block, 'RADIUS', Blockly.JavaScript.ORDER_ATOMIC) || '70';
    var code = 'metabot_radius('+ value_radius +');\n';
    return code;
};

Blockly.Blocks['spidey_stop'] = {
    init: function() {
        this.setColour(0);
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage('spidey.png', 25, 25, ""))
            .appendField('Metabot stop');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('Metabot s\'arrête');
    }
};
Blockly.JavaScript['spidey_stop'] = function(block) {
    var code = 'metabot_rotate(0); metabot_move(0, 0);\n';
    return code;
};

Blockly.Blocks['spidey_wait'] = {
    init: function() {
        this.setColour(45);
        this.appendValueInput('WAIT')
            .setCheck('Number')
            .appendField('Attendre');
        this.appendDummyInput()
            .appendField('seconde(s)');
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('Ne fait rien');
    }
};
Blockly.JavaScript['spidey_wait'] = function(block) {
    var value_name = Blockly.JavaScript.valueToCode(
        block, 'WAIT', Blockly.JavaScript.ORDER_ATOMIC) || '0';
    var code = 'metabot_wait('+ value_name +');\n';
    return code;
};

Blockly.Blocks['spidey_print'] = {
    init: function() {
        this.setColour(0);
        this.appendValueInput('STR')
            .setCheck(['String', 'Number'])
            .appendField('Afficher sur le terminal');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('Affiche un message sur le terminal');
    }
};
Blockly.JavaScript['spidey_print'] = function(block) {
    var value_str = Blockly.JavaScript.valueToCode(
        block, 'STR', Blockly.JavaScript.ORDER_ATOMIC) || '';
    var code = 'metabot_print('+ value_str +');\n';
    return code;
};

Blockly.Blocks['spidey_button1'] = {
    init: function() {
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Bouton 1');
        this.setOutput(true, 'Number');
        this.setTooltip('Valeur du bouton 1');
    }
};
Blockly.JavaScript['spidey_button1'] = function(block) {
    var code = 'metabot_button1()';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};
Blockly.Blocks['spidey_button2'] = {
    init: function() {
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Bouton 2');
        this.setOutput(true, 'Number');
        this.setTooltip('Valeur du bouton 2');
    }
};
Blockly.JavaScript['spidey_button2'] = function(block) {
    var code = 'metabot_button2()';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};
Blockly.Blocks['spidey_button3'] = {
    init: function() {
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Bouton 3');
        this.setOutput(true, 'Number');
        this.setTooltip('Valeur du bouton 3');
    }
};
Blockly.JavaScript['spidey_button3'] = function(block) {
    var code = 'metabot_button3()';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};
Blockly.Blocks['spidey_button4'] = {
    init: function() {
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Bouton 4');
        this.setOutput(true, 'Number');
        this.setTooltip('Valeur du bouton 4');
    }
};
Blockly.JavaScript['spidey_button4'] = function(block) {
    var code = 'metabot_button4()';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};
Blockly.Blocks['spidey_button5'] = {
    init: function() {
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Bouton 5');
        this.setOutput(true, 'Number');
        this.setTooltip('Valeur du bouton 5');
    }
};
Blockly.JavaScript['spidey_button5'] = function(block) {
    var code = 'metabot_button5()';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};
Blockly.Blocks['spidey_button6'] = {
    init: function() {
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Bouton 6');
        this.setOutput(true, 'Number');
        this.setTooltip('Valeur du bouton 6');
    }
};
Blockly.JavaScript['spidey_button6'] = function(block) {
    var code = 'metabot_button6()';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.Blocks['spidey_slider1'] = {
    init: function() {
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Slider 1');
        this.setOutput(true, 'Number');
        this.setTooltip('Valeur du slider 1');
    }
};
Blockly.JavaScript['spidey_slider1'] = function(block) {
    var code = 'metabot_slider1()';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};
Blockly.Blocks['spidey_slider2'] = {
    init: function() {
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Slider 2');
        this.setOutput(true, 'Number');
        this.setTooltip('Valeur du slider 2');
    }
};
Blockly.JavaScript['spidey_slider2'] = function(block) {
    var code = 'metabot_slider2()';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};
Blockly.Blocks['spidey_slider3'] = {
    init: function() {
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Slider 3');
        this.setOutput(true, 'Number');
        this.setTooltip('Valeur du slider 3');
    }
};
Blockly.JavaScript['spidey_slider3'] = function(block) {
    var code = 'metabot_slider3()';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};
Blockly.Blocks['spidey_slider4'] = {
    init: function() {
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Slider 4');
        this.setOutput(true, 'Number');
        this.setTooltip('Valeur du slider 4');
    }
};
Blockly.JavaScript['spidey_slider4'] = function(block) {
    var code = 'metabot_slider4()';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.Blocks['spidey_joystick1X'] = {
    init: function() {
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Joystick 1 X');
        this.setOutput(true, 'Number');
        this.setTooltip('Valeur du joystick 1 X');
    }
};
Blockly.JavaScript['spidey_joystick1X'] = function(block) {
    var code = 'metabot_joystick1X()';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};
Blockly.Blocks['spidey_joystick1Y'] = {
    init: function() {
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Joystick 1 Y');
        this.setOutput(true, 'Number');
        this.setTooltip('Valeur du joystick 1 Y');
    }
};
Blockly.JavaScript['spidey_joystick1Y'] = function(block) {
    var code = 'metabot_joystick1Y()';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};
Blockly.Blocks['spidey_joystick2X'] = {
    init: function() {
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Joystick 2 X');
        this.setOutput(true, 'Number');
        this.setTooltip('Valeur du joystick 2 X');
    }
};
Blockly.JavaScript['spidey_joystick2X'] = function(block) {
    var code = 'metabot_joystick2X()';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};
Blockly.Blocks['spidey_joystick2Y'] = {
    init: function() {
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Joystick 2 Y');
        this.setOutput(true, 'Number');
        this.setTooltip('Valeur du joystick 2 Y');
    }
};
Blockly.JavaScript['spidey_joystick2Y'] = function(block) {
    var code = 'metabot_joystick2Y()';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.Blocks['spidey_color'] = {
    init: function() {
        this.setColour(0);
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage('spidey.png', 25, 25, ''))
            .appendField("Metabot couleur écriture")
            .appendField(new Blockly.FieldColour("#000000"), "COLOR");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('Couleur du trait de Metabot');
    }
};
Blockly.JavaScript['spidey_color'] = function(block) {
    var color = block.getFieldValue('COLOR');
    var code = 'metabot_color(\'' + color + '\');\n';
    return code;
};

Blockly.Blocks['spidey_start_write'] = {
    init: function() {
        this.setColour(0);
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage('spidey.png', 25, 25, ""))
            .appendField('Metabot écrit');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('Metabot commence à écrite');
    }
};
Blockly.JavaScript['spidey_start_write'] = function(block) {
    var code = 'metabot_start_write();\n';
    return code;
};
Blockly.Blocks['spidey_stop_write'] = {
    init: function() {
        this.setColour(0);
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage('spidey.png', 25, 25, ""))
            .appendField('Metabot arrête d\'écrire');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('Metabot arrête d\'écrite');
    }
};
Blockly.JavaScript['spidey_stop_write'] = function(block) {
    var code = 'metabot_stop_write();\n';
    return code;
};

