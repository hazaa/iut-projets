function SpideyExec() {
    'use strict';

    this.wait = 0;
    this.isStop = false;
    this.timer = null;
    
    this.odoX = 1.0;
    this.odoY = 1.0;
    this.odoA = 1.0;

    this.realRobot = false;

    var self = this;
    var interpreter;
    var lastRedraw;

    function initApi(interpreter, scope) {
        var wrapper;

        wrapper = function(xx, yy) {
            return interpreter.createPrimitive(
                (function(x,y) {
                    terminal.print('Metabot avance x='+x+', y='+y, 'cyan');
                    simulator.setVelMove(x, y);
                    if (self.realRobot) {
                        robot.command('dx='+self.odoX*xx);
                        robot.command('dy='+self.odoY*(-yy));
                    }
                })(xx, yy));
        };
        interpreter.setProperty(scope, 'metabot_move',
            interpreter.createNativeFunction(wrapper));
        
        wrapper = function(angle) {
            return interpreter.createPrimitive(
                (function(a) {
                    terminal.print('Metabot tourne '+a, 'cyan');
                    simulator.setVelRotate(a);
                    if (self.realRobot) {
                        robot.command('turn='+self.odoA*(-a));
                    }
                })(angle));
        };
        interpreter.setProperty(scope, 'metabot_rotate',
            interpreter.createNativeFunction(wrapper));
        
        wrapper = function(height) {
            return interpreter.createPrimitive(
                (function(h) {
                    if (h < 0) h = 0;
                    if (h > 100) h = 100;
                    terminal.print('Metabot hauteur '+h, 'cyan');
                    if (self.realRobot) {
                        robot.command('h='+(-h));
                    }
                })(height));
        };
        interpreter.setProperty(scope, 'metabot_height',
            interpreter.createNativeFunction(wrapper));
        wrapper = function(radius) {
            return interpreter.createPrimitive(
                (function(r) {
                    if (r < 50) r = 50;
                    if (r > 120) r = 120;
                    terminal.print('Metabot rayon '+r, 'cyan');
                    if (self.realRobot) {
                        robot.command('r='+r);
                    }
                })(radius));
        };
        interpreter.setProperty(scope, 'metabot_radius',
            interpreter.createNativeFunction(wrapper));
        
        wrapper = function(time) {
            return interpreter.createPrimitive(
                (function(t) { 
                    self.wait = t; 
                })(time));
        };
        interpreter.setProperty(scope, 'metabot_wait',
            interpreter.createNativeFunction(wrapper));
        
        wrapper = function(line) {
            return interpreter.createPrimitive(
                terminal.print(line));
        };
        interpreter.setProperty(scope, 'metabot_print',
            interpreter.createNativeFunction(wrapper));
        
        wrapper = function() {
            return interpreter.createPrimitive(
                (function() {
                    terminal.print('Metabot commence à écrire ', 'cyan');
                    simulator.setWriting(true);
                })());
        };
        interpreter.setProperty(scope, 'metabot_start_write',
            interpreter.createNativeFunction(wrapper));
        wrapper = function() {
            return interpreter.createPrimitive(
                (function() {
                    terminal.print('Metabot arrête d\'écrire ', 'cyan');
                    simulator.setWriting(false);
                })());
        };
        interpreter.setProperty(scope, 'metabot_stop_write',
            interpreter.createNativeFunction(wrapper));
        
        wrapper = function(color) {
            return interpreter.createPrimitive(
                (function(c) {
                    terminal.print('Metabot change de couleur', 'cyan');
                    simulator.setPathColor(c);
                })(color));
        };
        interpreter.setProperty(scope, 'metabot_color',
            interpreter.createNativeFunction(wrapper));
        
        wrapper = function() {
            return interpreter.createPrimitive(
                controls.button1);
        };
        interpreter.setProperty(scope, 'metabot_button1',
            interpreter.createNativeFunction(wrapper));
        wrapper = function() {
            return interpreter.createPrimitive(
                controls.button2);
        };
        interpreter.setProperty(scope, 'metabot_button2',
            interpreter.createNativeFunction(wrapper));
        wrapper = function() {
            return interpreter.createPrimitive(
                controls.button3);
        };
        interpreter.setProperty(scope, 'metabot_button3',
            interpreter.createNativeFunction(wrapper));
        wrapper = function() {
            return interpreter.createPrimitive(
                controls.button4);
        };
        interpreter.setProperty(scope, 'metabot_button4',
            interpreter.createNativeFunction(wrapper));
        wrapper = function() {
            return interpreter.createPrimitive(
                controls.button5);
        };
        interpreter.setProperty(scope, 'metabot_button5',
            interpreter.createNativeFunction(wrapper));
        wrapper = function() {
            return interpreter.createPrimitive(
                controls.button6);
        };
        interpreter.setProperty(scope, 'metabot_button6',
            interpreter.createNativeFunction(wrapper));
        
        wrapper = function() {
            return interpreter.createPrimitive(
                controls.slider1);
        };
        interpreter.setProperty(scope, 'metabot_slider1',
            interpreter.createNativeFunction(wrapper));
        wrapper = function() {
            return interpreter.createPrimitive(
                controls.slider2);
        };
        interpreter.setProperty(scope, 'metabot_slider2',
            interpreter.createNativeFunction(wrapper));
        wrapper = function() {
            return interpreter.createPrimitive(
                controls.slider3);
        };
        interpreter.setProperty(scope, 'metabot_slider3',
            interpreter.createNativeFunction(wrapper));
        wrapper = function() {
            return interpreter.createPrimitive(
                controls.slider4);
        };
        interpreter.setProperty(scope, 'metabot_slider4',
            interpreter.createNativeFunction(wrapper));
        
        wrapper = function() {
            return interpreter.createPrimitive(
                controls.joypad1X);
        };
        interpreter.setProperty(scope, 'metabot_joystick1X',
            interpreter.createNativeFunction(wrapper));
        wrapper = function() {
            return interpreter.createPrimitive(
                controls.joypad1Y);
        };
        interpreter.setProperty(scope, 'metabot_joystick1Y',
            interpreter.createNativeFunction(wrapper));
        wrapper = function() {
            return interpreter.createPrimitive(
                controls.joypad2X);
        };
        interpreter.setProperty(scope, 'metabot_joystick2X',
            interpreter.createNativeFunction(wrapper));
        wrapper = function() {
            return interpreter.createPrimitive(
                controls.joypad2Y);
        };
        interpreter.setProperty(scope, 'metabot_joystick2Y',
            interpreter.createNativeFunction(wrapper));
      
        wrapper = function(id) {
            id = id ? id.toString() : '';
            return interpreter.createPrimitive(highlightBlock(id));
        };
        interpreter.setProperty(scope, 'highlightBlock',
                interpreter.createNativeFunction(wrapper));
    }
    
    function highlightBlock(id) {
        Blockly.mainWorkspace.highlightBlock(id);
    }

    function doStop() {
        if (self.timer != null) {
            clearTimeout(self.timer);
            self.timer = null;
        }
        robot.command('stop');
        robot.command('dx=0');
        robot.command('dy=0');
        robot.command('turn=0');
        self.realRobot = false;
        simulator.stop();
        terminal.print('Stop', 'red');
    }

    this.enableRobot = function() {
        if (!this.realRobot) {
            robot.command('start');
        }
        this.realRobot = true;
    };

    this.step = function() {
        if (self.isStop) {
            return;
        }
        if (self.realRobot && !robot.isRobotConnected) {
            self.stop();
            return;
        }
        if (self.wait > 0.001) {
            self.wait -= 0.2;
            lastRedraw = 0;
            simulator.step();
            self.timer = window.setTimeout(self.step, 200);
        } else {
            var notFinish = interpreter.step();
            if (notFinish) {
                lastRedraw++; 
                if (lastRedraw > 50) {
                    lastRedraw = 0;
                    self.timer = window.setTimeout(self.step, 0);
                } else {
                    self.step();
                }
            } else {
                doStop();
            }
        }
    };

    this.run = function(debug) {
        Blockly.JavaScript.INFINITE_LOOP_TRAP = null;
        if (debug) {
            Blockly.JavaScript.STATEMENT_PREFIX = 'highlightBlock(%1);\n';
            Blockly.JavaScript.addReservedWords('highlightBlock');
            Blockly.mainWorkspace.traceOn(true);
            Blockly.mainWorkspace.highlightBlock(null);
        } 
        terminal.print('Start', 'red');
        var code = Blockly.JavaScript.workspaceToCode();
        interpreter = new Interpreter(code, initApi);
        self.isStop = false;
        lastRedraw = 0;
        self.step();
    };

    this.stop = function() {
        doStop();
        self.isStop = true;
        Blockly.mainWorkspace.traceOn(false);
        Blockly.mainWorkspace.highlightBlock(null);
    };
}

var exec = new SpideyExec();

