'''
The MIT License (MIT)
Copyright (c) 2013 Dave P.
https://github.com/dpallot/simple-websocket-server
modified by A.Hazan
'''

import signal
import sys
from SimpleWebSocketServer import WebSocket, SimpleWebSocketServer
from optparse import OptionParser

class SimpleEcho(WebSocket):

   def handleMessage(self):
      print self.data 
      self.sendMessage(self.data)
      if(self.data=="led1"):
		  print "call script toggle led1" 		

   def handleConnected(self):
      print 'connected'

   def handleClose(self):
      pass



if __name__ == "__main__":

   parser = OptionParser(usage="usage: %prog [options]", version="%prog 1.0")
   parser.add_option("--host", default='', type='string', action="store", dest="host", help="hostname (localhost)")
   parser.add_option("--port", default=8000, type='int', action="store", dest="port", help="port (8000)")

   (options, args) = parser.parse_args()

   cls = SimpleEcho
   server = SimpleWebSocketServer(options.host, options.port, cls)

   def close_sig_handler(signal, frame):
      server.close()
      sys.exit()

   signal.signal(signal.SIGINT, close_sig_handler)

   server.serveforever()
