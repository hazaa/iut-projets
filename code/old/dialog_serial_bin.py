# -*- coding: utf-8

# dialog_serial_bin/dialog_serial_bin.py
# Côté Python

import serial
import struct
import time

# j'ouvre ma connexion série
PORT="/dev/ttyACM0"
BAUD="9600"
ser = serial.Serial(PORT, BAUD)
# j'attends un peu
time.sleep(0.5)

# on prépare un message codé en binaire
x=100
message = struct.pack('!B',x)

while True:
	# on affiche le message pour vérifier que tout va bien
	print "send:", struct.unpack('!B',message)[0]
    # on envoie l'octet sur l'interface série
	ser.write(message)
    # on lit la réponse
	reponse_bin=ser.read(1)
	# on décode la réponse
	reponse_decimal = struct.unpack('!B',reponse_bin)[0]
	# on affiche la réponse
	print "received:",reponse_decimal
	time.sleep(0.001)
