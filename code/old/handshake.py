# -*- coding: utf-8
import serial
import struct
import time
import numpy as np

"""
TODO: 
 * increase speed 
 * random bytes
"""

PORT="/dev/ttyACM0"
BAUD="115200"#"9600"
ser = serial.Serial(PORT, BAUD)

# reset the arduino
ser.setDTR(level=False)
time.sleep(0.5)
# ensure there is no stale data in the buffer
ser.flushInput()
ser.setDTR()
time.sleep(0.5)


print "waiting for arduino..."

# initial handshake w/ arduino
print 'handshake: ' + str(struct.unpack("B", ser.read())[0])
ser.flushInput()
ser.write('\x10')

print "connected to arduino..."

while True:
    #print sp.read()    
    # unpack binary data composed of 3 16-bit signed ints
	#print struct.unpack("hhh", ser.read(6))
	# generate random bits:  numpy.random.bytes(length)¶
	message=np.random.bytes(1)
	#message='\x10'
	print "send: ", struct.unpack("b", message)
	ser.write(message)	
	print "read:", struct.unpack("b", ser.read(1)), "\n\n"
	#print ser.read(1)
	time.sleep(0.001)	
