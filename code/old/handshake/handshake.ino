/*
handshake
http://josephmr.com/2014/08/31/talking-to-arduino-with-python.html

*/

int inByte = 0;         // incoming serial byte

void setup() {
    Serial.begin(115200);//9600
    delay(50);
    while (!Serial.available()) {
      Serial.write(0x01);
      delay(300);
    }
    // read the byte that Python will send over
    Serial.read();
}

void loop() {
	//Serial.write(0x41);
	//delay(1000);

   if (Serial.available() > 0) {
    // get incoming byte:
    inByte = Serial.read();
    // read first analog input, divide by 4 to make the range 0-255:
    //firstSensor = analogRead(A0) / 4;
    // delay 10ms to let the ADC recover:
    delay(10);
   
    // send sensor values:
    Serial.write(inByte);
    //Serial.write(firstSensor);
    //Serial.write(secondSensor);
    //Serial.write(thirdSensor);
  }
  
}
