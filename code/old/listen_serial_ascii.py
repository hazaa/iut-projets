# -*- coding: utf-8

"""
Ce programme Python écoute le port série et décode en ASCII.

En Python, on commente le code avec un dièse #

Exemples sur le net:
https://www.arduino.cc/en/Tutorial/BuiltInExamples#communication
http://www.elcojacobs.com/communicating-between-python-and-arduino-with-pyserial/
"""

import serial
 
# ouvrir un port série 
ser = serial.Serial('/dev/ttyACM0',9600,timeout=2)

# boucle 
while True:
	a= ser.readline()	
	print a			
