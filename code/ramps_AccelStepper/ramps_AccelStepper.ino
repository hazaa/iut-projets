/* ramps_AccelStepper.ino
 * 
 * Ici on utilise la librairie AccelStepper 
 * pour commander un driver de moteur pas-à-pas
 * situé sur le port X de la carte RAMPS 1.4
 * 
 * Pour les autres ports (Y,Z), voir ramps_test/ramps_test.ino
 * 
 * inspired by AccelStepper/ProportionalControl.pde and stepper/stepper_speedControl.ino
 *  http://www.airspayce.com/mikem/arduino/AccelStepper/ProportionalControl_8pde-example.html#a3
 * 
*/
#include <AccelStepper.h>


#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38
AccelStepper stepper(1, X_STEP_PIN, X_DIR_PIN); // 1 = Driver


int inbyte, TargetSpeed=0;
int maxspeed=3000;
void setup()
{  
  /* initialize stepper */  
  stepper.setEnablePin(X_ENABLE_PIN);
  stepper.setPinsInverted(false, false, true); //invert logic of enable pin
  stepper.enableOutputs();
 
   /* setSpeed() not reliable above 1000 steps per second
  http://www.airspayce.com/mikem/arduino/AccelStepper/classAccelStepper.html#ae79c49ad69d5ccc9da0ee691fa4ca235
  */
  stepper.setMaxSpeed(maxspeed);   

}


void loop()
{
  
  /*
  if (Serial.available()>0)
      {
       
          stepper.setSpeed(2000); 
      }*/
    
  stepper.setSpeed(2000);
  stepper.runSpeed();

}
