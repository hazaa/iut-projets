// Random.pde
// -*- mode: C++ -*-
//
// Make a single stepper perform random changes in speed, position and acceleration
//
// Copyright (C) 2009 Mike McCauley
// $Id: Random.pde,v 1.1 2011/01/05 01:51:01 mikem Exp mikem $
#include <AccelStepper.h>
// Define a stepper and the pins it will use

#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38

#define LED_PIN            13

AccelStepper stepper(1, X_STEP_PIN, X_DIR_PIN); // 1 = Driver

void setup()
{  
  pinMode(LED_PIN  , OUTPUT);
    
  stepper.setEnablePin(X_ENABLE_PIN);
  stepper.setPinsInverted(false, false, true); //invert logic of enable pin
  stepper.enableOutputs();
  
  stepper.setMaxSpeed(5000);
//  stepper.setSpeed(1000);
  //stepper.setAcceleration(1000);
  
  
}
void loop()
{
   if (millis() %1000 <500) 
    digitalWrite(LED_PIN, HIGH);
  else
   digitalWrite(LED_PIN, LOW);
  
  
  if (millis() %1000 <500){
     stepper.setSpeed(10000);
     stepper.runSpeed();}
   else{
    stepper.setSpeed(-10000);
        stepper.runSpeed();
   }
    
  /*
    if (stepper.distanceToGo() == 0)
    {
        // Random change to speed, position and acceleration
        // Make sure we dont get 0 speed or accelerations
        delay(1000);
        stepper.moveTo(rand() % 200);
        stepper.setMaxSpeed((rand() % 200) + 1);
        stepper.setAcceleration((rand() % 200) + 1);
    }
    stepper.run();*/
}

