#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38
#define X_MIN_PIN           3
#define X_MAX_PIN           2

#define Y_STEP_PIN         60
#define Y_DIR_PIN          61
#define Y_ENABLE_PIN       56
#define Y_MIN_PIN          14
#define Y_MAX_PIN          15

#define Z_STEP_PIN         46
#define Z_DIR_PIN          48
#define Z_ENABLE_PIN       62
#define Z_MIN_PIN          18
#define Z_MAX_PIN          19


#define SDPOWER            -1
#define SDSS               53
#define LED_PIN            13

#define FAN_PIN            9

#define PS_ON_PIN          12
#define KILL_PIN           -1


void setup() {
 
  pinMode(LED_PIN  , OUTPUT);
  
  pinMode(X_STEP_PIN  , OUTPUT);
  pinMode(X_DIR_PIN    , OUTPUT);
  pinMode(X_ENABLE_PIN    , OUTPUT);
  
 
  
   digitalWrite(X_ENABLE_PIN    , LOW);
  
}





void loop () {
  
  if (millis() %1000 <500) 
    digitalWrite(LED_PIN, HIGH);
  else
   digitalWrite(LED_PIN, LOW);
  

  
  if (millis() %10000 <5000) {
    digitalWrite(X_DIR_PIN    , HIGH);
  
  }
  else {
    digitalWrite(X_DIR_PIN    , LOW);
   
  }
  
  
    digitalWrite(X_STEP_PIN    , HIGH);
   
  //delay(1);
   delayMicroseconds(1000);
    
    digitalWrite(X_STEP_PIN    , LOW);
   
  
}
