/*************************************************************************
* File Name          : serial_packet.ino 
* Author             : 
* Updated            : AH
* Version            : 
* Date               : 2015 feb 15
* Description        : adapted from Plotter_serial.ino
                       Data transmission format (6 byte):
                       =======================================
                        [5]   [4]   [3]   [2]   [1]   [0]
                        End    Z    Y     X     0xFE  0xFF
                        
                       End:0xFD for Data,0xFC for Reset
                       =======================================
* License            : CC-BY-SA 3.0
* Copyright (C) 2011-2013 Hulu Robot Technology Co., Ltd. All right reserved.
* http://www.makeblock.cc/
**************************************************************************/



// Define a stepper and the pins it will use

// 
byte rxBuf;
unsigned int timer = 0;
int x=0,y=0,xLast=0,yLast=0;
int inByte;
unsigned char dataBuf[8] = {0};

char stateMachine = 0,t=0; 
int minIndex = 10,posIndex = 0;


void setup(){  
  
  Serial.begin(9600);

}

void loop() {
  
    while(Serial.available()){
        rxBuf = Serial.read();      
        if(stateMachine == 0)// check state machine
        {
          if(rxBuf == 0xff) stateMachine = 1;
          else stateMachine = 0;
        }
        else if(stateMachine == 1)
        {
          if(rxBuf == 0xfe) stateMachine = 2;
          else stateMachine = 0;
        }
        else if(stateMachine == 2)// receive data
        {
          dataBuf[t++] = rxBuf&0xff;
          if(t>3){// when receive all of data, reset stateMachine
            if(dataBuf[3]==0xfd){
              /// do something with dataBuf[0], dataBuf[1], dataBuf[2]                
              Serial.println(dataBuf[0]);              
              Serial.println(dataBuf[1]); 
              Serial.println(dataBuf[2]); 
            }
            else if(dataBuf[3]==0xfc){
              //when finish or reset
              // set all motors to zero              
              delay(1000);
            }
          t=0;// reset 
          stateMachine=0;// reset stateMachine
          }
        }
      }
    
  
  //************************************
  //      [2]    [1]    [0]
  //       Z      Y      X 
  //************************************
}
