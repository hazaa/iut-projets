

/* 

read potentiometer and set motor speed
(encoders are not used)

*/ 
int sensorPin = A0;    // select the input pin for the potentiometer
int sensorValue = 0;  // variable to store the value coming from the sensor

#define analogOutPin 9  // PWM
//#define encoder0PinA  2 // encoder on interrupt 0
//#define encoder0PinB  3 // encoder on interrupt 1

volatile unsigned int encoder0Pos = 0;
int mot_speed, mot_speed_max;

void setup() { 

/*
  pinMode(encoder0PinA, INPUT); 
  digitalWrite(encoder0PinA, HIGH);       // turn on pull-up resistor
  pinMode(encoder0PinB, INPUT); 
  digitalWrite(encoder0PinB, HIGH);       // turn on pull-up resistor
*/
  //attachInterrupt(0, doEncoder, CHANGE);  // encoder pin on interrupt 0 - pin 2
  Serial.begin (9600);
  Serial.println("start");                // a personal quirk

} 

void loop(){

  // read the value from the sensor:
  sensorValue = analogRead(sensorPin);
  // compute new speed
  // y=map(value, fromLow, fromHigh, toLow, toHigh) 
  // https://www.arduino.cc/en/Reference/Map
  mot_speed_max = 40; // PWM command, must be in 0,255
  mot_speed = map(sensorValue, 0, 1023, 0, mot_speed_max); 
  // write motor speed
  analogWrite(analogOutPin, mot_speed);
  // print
  //Serial.print (encoder0Pos);
  Serial.print (sensorValue);
  Serial.print ("/");
  delay(100);
}

/*
void doEncoder() {
  // If pinA and pinB are both high or both low, it is spinning
  // forward. If they're different, it's going backward.
  //
  // For more information on speeding up this process, see
   // [Reference/PortManipulation], specifically the PIND register.
  //
  if (digitalRead(encoder0PinA) == digitalRead(encoder0PinB)) {
    encoder0Pos++;
  } else {
    encoder0Pos--;
  }
}

*/
