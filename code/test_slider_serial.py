# -*- coding: utf-8

#http://robotic-controls.com/learn/python-guis/tkinter-serial
# wx http://auzieman.blogspot.fr/2010/08/working-wxpython-example-servo-sliders.html
# Queue: http://stackoverflow.com/questions/25631229/python-sending-constant-serial-data-from-scale-widget
# https://wiki.python.org/moin/TkInter
#https://pyserial.readthedocs.org/en/latest/

from Tkinter import *
import time
import serial

#cbvar = Tk.IntVar()
#cbvar.set(1)

def show_values():
    print (w1.get(), w2.get())

ser = serial.Serial('/dev/ttyACM0',115200)

def send(v):
	#ser.write(s.get())
	s=hex(int(v))
	print "v=",v, type(v), s
	ser.write(s)
	time.sleep(0.1)	

master = Tk()
s = Scale(master, from_=0, to=255, orient=HORIZONTAL, command=send)#, variable=cbvar)
s.pack()
#Button(master, text='Show', command=show_values).pack()

mainloop()
