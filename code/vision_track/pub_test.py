# -*- coding: utf-8

"""
simple publisher that sends a message
"""

import zmq
import time 

port = "5556"

# publisher
context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)
i=0
while True:
	try:
		msg = "%s %d"%(port, i)
		socket.send(msg)
		print 'sending: '+msg 
		i+=1
		time.sleep(1)
	except KeyboardInterrupt:
		print 'keyboard interrupt...'
		break
