# -*- coding: utf-8
# Raspbery Pi Color Tracking Project
# Code written by Oscar Liang
# 30 Jun 2013


import sys
import cv2
import zmq
import numpy as np
#import smbus

#bus = smbus.SMBus(1)
address = 0x04
port = "5556"

# publisher
context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)

def sendData(value):
	bus.write_byte(address, value)
	# bus.write_byte_data(address, 0, value)
	return -1

def readData():
	state = bus.read_byte(address)
	#number = bus.read_byte_data(address, 1)
	return state

def ColorProcess(img):
	""" returns thresholded image"""
	# converts BGR image to HSV
	#imgHSV = cv.CreateImage(cv.GetSize(img), 8, 3)
	hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
	#!!!!!!!!!!!cv.Smooth(frame, frame, cv.CV_BLUR, 3)	
	# converts the pixel values lying within the range to 255 and stores it in the destination	
	low = np.array([110,50,50])
	hi = np.array([130,255,255])
    # Threshold the HSV image to get only white colors
	mask = cv2.inRange(hsv, low, hi)
	return mask

def main():
	# captured image size, change to whatever you want
	w = 320
	h = 240
	capture = cv2.VideoCapture(0)
	capture.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, w)
	capture.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, h)

	while True:
		_, frame = capture.read()
	
		mask = ColorProcess(frame)		
		# Calculating the moments
		moments = cv2.moments(mask, 0)		
		area = moments['m00']
		# Finding a big enough blob
		if(area > 60000):
			# Calculating the center postition of the blob
			posX = int(moments['m10'] / area)
			posY = int(moments['m01']/ area)
			#print posX,posY
			cv2.circle(frame, (posX,posY),10, (0,255,0),10 )
			# check slave status and send coordinates
			"""
			state = readData()
			if state == 1:
				sendData(posX)
				sendData(posY)
			"""
			socket.send("%d %d" % (posX,posY))
			#socket.send("%d %d" % (10001, posX))

		# update video windows
		if (showimage==1):
			cv2.imshow('frame',frame)
		# catch keyboard			
		k = cv2.waitKey(5) & 0xFF
		if k == 27:
			break
	return;

if __name__ == "__main__":
	# read args
	showimage=1
	if len(sys.argv) > 1:
	   showimage =  sys.argv[1]
	   int(showimage)
	   print "showimage=",showimage	

	# publisher
	port = "5556"
	context = zmq.Context()
	socket = context.socket(zmq.PUB)
	socket.bind("tcp://*:%s" % port)
	# image track
	main()
