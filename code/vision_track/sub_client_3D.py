# -*- coding: utf-8
"""
Lecture sur le réseau (en se connectant à une socket de la raspberry) 
de la position du marqueur mobile, dans le reprère du marqueur fixe

Exemple:
$python sub_client_3D.py 10.14.72.68

"""

import zmq
import sys

# la camera est branchée sur le raspberry pi.
# il faut indiquer la bonne adresse IP de votre raspberry
if len(sys.argv)>1:
	ip= sys.argv[1]  #"10.14.72.68"
else:	  
	ip="localhost"
port = "5556"      # ne pas modifier
topicfilter=""
print "listening ip=",ip,":",port, "topic=",topicfilter

# connexion à la socket de la raspberry
context = zmq.Context()
socket = context.socket(zmq.SUB)

socket.connect ("tcp://%s:%s" %(ip, port))
socket.setsockopt(zmq.SUBSCRIBE, topicfilter)

# boucle
while True:
    string = socket.recv()
    try:
		posX, posY,posZ = string.split()
		posX=float(posX)
		posY=float(posY)
		posZ=float(posY)
		print "posX=", posX, " posY=",posY, " posZ=",posZ
    except ValueError as e:
		print "string=",string
