# -*- coding: utf-8 -*-
"""
client that listens and writes incoming messages

see:
https://docs.python.org/2/howto/sockets.html

if used with whycon, the messages will have the following form:
(see imageproc/CPositionServer.cpp)
 
   void CPositionServer::clearToSend(){
     (...)
     sprintf(buffer,"Detected %i of %i at %ld. \n",numFound,numObjects,updateTime);
     sprintf(buffer,"%sRobot %03i %.3f %.3f %.3f %ld \n",buffer,o.ID,o.x,o.y,o.yaw*180/M_PI,lastDetectionArray[i]);
     }


"""


import socket
import string

hote = "localhost"    # adresse IP du PC où tourne le programme whycon
port = 6666           # port sur lequel whycon publie ses données
n = 1000              # taille du buffer

# connection à la socket 
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.connect((hote, port))

while True:
   # reception de donnée	
   data = socket.recv(n)
   if len(data)>0: 
	   # découpage de la chaine de caractère grâce aux espaces.
	   # NB: s est une liste de chaînes de caractères  	   
	   s = string.split(data)
	   print s
	   
		
