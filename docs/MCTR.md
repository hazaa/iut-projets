MCTR
======

Ce module fait partie du Parcours [SIRO](https://eprel.u-pec.fr/eprel/claroline/course/index.php?cid=4529): Système Informatique embarqué et RObotique mobile.
Objectif: Concevoir, commander et évaluer un système embarqué.


Les systèmes embarqués sont présents partout au quotidien 
(robots, drones, box internet, bornes wifi, routeurs, smartphones, appareils médicaux, automates industriels, domotique, contrôle d'accès, IoT, ...)

Leurs caractéristiques communes: 

*  interface utilisateur limitée
*  puissance de calcul limitée
*  lien direct avec des appareils électriques qu'ils commandent.

Ils sont souvent composés des éléments suivants, comme dans la figure :

![schema](fig/mctr_schema_global.png)

*  un mini-pc basse consommation, avec un vrai système d’exploitation (par exemple linux).
*  un micro-contrôleur qui gère les capteurs et la commande de puissance, en temps réel. Il est souvent relié au PC par liaison série.
*  des capteurs, ou une interface utilisateur.
*  des actionneurs (leds, moteurs, ...).





Sujets 2021-2022:

* TP [Raspberry Pi](tp-raspi.md) 
* Révision: [TP Arduino](tp-arduino.md) 
* Révision: [TP Python](tp-python.md)
* Robot de téléprésence: [TP téléprésence](tp-telepresence-streaming.md).



Sujets 2018-2019:

* TP [Raspberry Pi](tp-raspi.md) , Révision: [TP Arduino](tp-arduino.md) , Révision: [TP Python](tp-python.md)
* Robot de téléprésence: depuis un navigateur, on veut piloter le robot, voir ce que filme sa caméra, visualiser la vitesse de rotation de roues (BONUS: et le courant qui traverse chaque moteur): serveur streaming video [TP téléprésence](tp-telepresence-streaming.md), (Pilotage de moteur à courant continu avec [encodeur à quadrature v2](docs/moteur_cc_encodeur_quadrature_2.md) )



Sujets 2017-2018:

*   [tracking visuel](docs/tracking_visuel.md) avec tourelle.
*   Interface [web](docs/interface-web.md) CGI (à remplacer par node.js)
*   IHM [android](docs/tp_UI_android.md)



TODO
------


*  migration python3
*  raspi pico au lieu de arduino
*  UI+node.js: https://github.com/firmata/firmata.js, http://www.openchapters.com/chapter_4.html
*  remplacer interface web CGI par UI+raspi+nodejs [forums.framboise314](http://forums.framboise314.fr/viewtopic.php?f=44&t=3033&hilit=nodejs), [adafruit](https://learn.adafruit.com/node-embedded-development/why-node-dot-js).[pigpio+node.js](https://github.com/fivdi/pigpio). Exemple pi + bldc [youtube](https://www.youtube.com/watch?v=KupkUZTNC1o), [instructables](https://www.instructables.com/id/Driving-an-ESCBrushless-Motor-Using-Raspberry-Pi/)
*  python + raspi 2: file output, (sgbd)
*  cross-compile
*  (gpio commande directe: vieux sujet TP MCTR ; [gertboard](https://fr.farnell.com/gertboard/gertboard/carte-gertboard-assemblee/dp/2250034))
*  mini projets:
*  robot: instrumentation: quadrature encoder, capteur de courant. Réseau: trames iRobot.
*  linux+uC: arduino Yun
*  IoT: esp8266
*  Android + ML (voice, yolo):  tensoflow lite+ bluetooth+ arduino:  [youtube](https://www.youtube.com/watch?v=FKoxqZ7R7Ec), [create.arduino.cc](https://create.arduino.cc/projecthub/joechen/jrobot-self-drive-powered-by-tensorflow-lite-94c82f)
*  vision: in-browser Yolo ; third-party yolo
*  vision, voir [tp-vision-advanced](tp-vision-advanced.md)


*  admin: installer/configurer raspbian.
*  mini-projets: self-balancing micropython [github](https://github.com/jeffmer/micropython-upybbot), [forum](https://forum.micropython.org/viewtopic.php?t=658). Avec vision au lieu d'acceleromètre; petit train+vision, line following, face-tracking: voir [tracking visuel](docs/tracking_visuel.md)

