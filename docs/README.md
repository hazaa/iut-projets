# Documents techniques

NOTE: les projets et TP sont réalisés avec des [logiciels libres](https://fr.wikipedia.org/wiki/Logiciel_libre)!


*   robotique:
    *    pince [openbionics](http://openbionics.org/)
    *    bras [MeArm](http://mime.co.uk)
*   logiciels:
    *   conception de circuits électriques: [fritzing](http://fritzing.org), kicad, proteus
*   moteurs:
    *   moteur à courant continu [digilent 6V](http://store.digilentinc.com/dc-motor-gearbox-1-53-gear-ratio-custom-6v-motor-designed-for-digilent-robot-kits/)
        *    encodeur magnétique [pdf](notices_techniques/magnetic-encoders.pdf, [shayangye](http://www.shayangye.com/en/product-inner.aspx?f=s&i=2198)
        *    connecteur JST PHR/ZHR 6 [RS](https://uk.rs-online.com/web/p/pcb-connector-housings/8201481/?searchTerm=jst+phr+6&relevancy-data=636F3D3126696E3D4931384E53656172636847656E65726963266C753D656E266D6D3D6D61746368616C6C7061727469616C26706D3D5E2E2A2426706F3D31333326736E3D592673743D43415443485F414C4C5F44454641554C542673633D592677633D4E4F4E45267573743D6A7374207068722036267374613D6A737420706872203626&cm_mmc=UK-PPC-DS3A-_-bing-_-3_UK_EN_BM_IP_and_E_Exact-_-jst|pcb_connector_housings-_-jst%20phr%206&gclid=CJLjocWh3dkCFQGFhQodgTULOg)
    *   servomoteur standard [HS-322](https://www.servocity.com/hs-322hd-servo)
    *   moteur pas-à-pas
*   composants:
    *   pont en H [PmodHB5](https://reference.digilentinc.com/reference/pmod/pmodhb5/start)
    *   carte breakout [L298N](https://github.com/yohendry/arduino_L298N)
*   programmation:
    *   arduino:    
        *    varspeed
        *    Accelstepper 
    *   raspberry pi:
        *    communication série [FTDI](https://wolfpaulus.com/embedded/raspberry_serial/), [sparkfun](https://www.sparkfun.com/products/9873)
        *    caméra: le [module](https://www.raspberrypi.org/products/camera-module/)
        *    librairie de traitement d'images [openCV](http://opencv.org)
