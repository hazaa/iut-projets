Air Hockey
======

*   Sujet: concevoir et réaliser un robot qui peut jouer à ce jeu. 
	On pourra recycler une vieille imprimante de récupération pour la partie
	actionneurs. Pour aller plus loin, on pourra utiliser un smartphone pour la vision. 
*   Organisation: par binômes.

Plan des mini-TP:

*   première séance: présentation du projet.
*   TP:
   *  prototypage rapide: 
      *  cheap robotics: voir liens ci-dessous.
      *  dessin 2D fablab: [TP fablab](tp-fablab.md)
   *  arduino: [TP Arduino](tp-arduino.md), sauf l'exercice Dialogue Série.
   *  commande d'un MCC avec hacheur série: [TP moteur](tp-motor.md)

![moving_printer_gantry](fig/air_hockey.gif)
Merci à Emeric HANG et Elijah MBAPPE pour ce gif.


Cahier des charges fonctionnel
------

| Nom fonction | Description fonction | Critère |  Valeurs limites  |
| -------- | -------- | -------- | -------- |
| Commande moteur | commander 1 moteur DC dans les deux sens  | |   |
| Estimation position | estimer la position absolue du charriot | taux d'erreur  | <10% |
| Asservissement en position | Positionner le charriot en réponse à une position de consigne donnée manuellement | taux d'erreur   | <10% |
| Sécurité | garantir la sécurité des étudiants et du matériel|   |  |

CDC technique:

*   micro-contrôleur : arduino uno ou mega.
*   moteurs: DC ou stepper parmi ceux disponibles.
*   carte de puissance: parmi celles disponibles (DC: MD22, motor shield, pmod, hacheur série; stepper: RAMPS 1.4).
  


Matériel disponible
------

Nous recyclerons des imprimantes à jet d'encre. 
Exemple d'ensemble charriot+moteur sur une des imprimantes démontées:

![inkjet_printer_gantry](fig/inkjet_printer_gantry.jpg)


Le moteur de translation de la tête des imprimantes à jet d'encre peut être à courant continu ou "pas-à-pas".
Concernant l'électronique de commande, nous disposons de :

*   un lot d'arduino Uno et Mega.
*   commande moteur à courant continu:
   *   deux cartes MD22 (voir [tp-arduino](tp-arduino.md) )
   *   une carte motor shield v2.
   *   en cas de besoin, nous devrons fabriquer un pont en H avec l'équipement de l'IUT (transistors MOSFET).  Voir: [Motor-Controllers-for-Cheap-Robots](http://www.instructables.com/id/Motor-Controllers-for-Cheap-Robots/)   
*   commande moteur "pas-à-pas":   
   *   4 cartes d'extension [RAMPS1.4](http://reprap.org/wiki/RAMPS1.4) pour arduino Mega. (la carte RAMPS sert de support à 3 contrôleur [Pololu](http://reprap.org/wiki/Pololu_stepper_driver_board) de moteurs pas-à-pas).


Commande manuelle du charriot
------

*  deux boutons (ou potentiomètres) pour aller dans un sens ou dans l'autre.
*  joystick carte arduino [esplora](https://www.arduino.cc/en/Reference/EsploraLibrary)
*  souris PS2 [instructables](http://www.instructables.com/id/Optical-Mouse-Odometer-for-Arduino-Robot/)
	Cette solution peut utiliser:
	*  soit le codeur optique de la molette.
	*  soit le caméra miniature pointée vers le bas.

Vous pourrez prévoir des capteurs de fin de course pour éviter les chocs.
Plusieurs types de capteurs:

*  interrupteurs mécaniques (type imprimante 3d)
*  capteur optique.


Asservissement de la position du charriot
------

Dans la section précédente, la position du charriot était fixée à la main.
Ici, elle se fait automatiquement. Mais il faut d'abord la connaître
pour pouvoir envoyer la bonne commande au moteur,
en fonction de la position du charriot.

Pour les moteur pas-à-pas, la position peut être connue facilement, car
le moteur est commandé en position (on envoie des +1 et de -1).

Mais pour un moteur DC, c'est plus compliqué. Plusieurs techniques sont 
possibles:

*   avec un codeur optique (comme pour la molette de la souris ci-dessous),
   couplé à une bandelette rayée, qui permet de calculer la distance parcourue dans un sens ou dans l'autre.
   voir [tp-optical-encoder](tp-optical-encoder.md)
*   avec une caméra située à distance du charriot et qui mesure sa position.
    voir [tp-vision-advanced](tp-vision-advanced.md)

Vous devrez proposer une technique permettant de connaître la position
du charriot.

Asservissement proprement dit: TODO

Liens utiles
------

*  Arduino:
   *  de nombreux exemples de robots  [arduino.cc](https://create.arduino.cc/projecthub?category=motors-robotics&part_id=&sort=trending)
   *  commander un moteur à courant continu [servo](https://www.arduino.cc/en/Tutorial/Sweep)
*  Fablab [Senartlab](https://www.senartlab.fr/)
*  Exemple de Air hockey automatisé avec un [smartphone](http://hackaday.com/2016/12/15/smartphone-will-destroy-you-at-air-hockey/)

# FAQ


| Question |  | Réponse | 
| -------- | -------- | -------- | 
| QUESTIONS GENERALES : | | |
|  | Quel sera le contenu de nos séances d'EREP ? | Les séances sont sous formes de TP, avec des mini-cours en cas de besoin (par exemple sur la programmation arduino). L'une des séances portera sur le prototypage rapide, au fablab. |
|  | Les séances d'EREP avant et après les vacances auront-elles le même contenu ? | Oui, les mini-cours seront adaptés en fonction des besoins. |
|  | Quelle partie du projet allons-nous réaliser au fablab de Sénart ? | Le prototypage pourra y être réalisé (par exemple découpe laser de pièces en carton ou bois). |
|  | Aurons-nous un dossier technique à rendre ? | Vous aurez un rapport par binôme à rendre à la fin. Un modèle se trouve sur gitlab dans le répertoire [iut-projets/docs](https://gitlab.com/hazaa/iut-projets/tree/master/docs) |
|  | Devrons-nous présenter une maquette ou un prototype de notre projet lors de la soutenance ? | Vous présenterez un prototype lors de la dernière séance avant la soutenance, pour que j'effectue une évaluation technique. Pour la soutenance, vous réaliserez une vidéo du prototype en fonctionnement.|
|  | Devrons-nous inclure une partie gestion de projet (planning, analyse de temps...) dans le PowerPoint de la soutenance ? | oui |
| QUESTIONS SUR LA COMPOSITION DU ROBOT : | | |
|  | Quels seront les éléments qui composeront notre système (nombre d'axes, capteurs, leds, squelette en bois...) ? | un seul axe linéaire, muni d'un moteur et de l'électronique de commande.|
|  | Parmis ces éléments, lesquels seront fournis et lesquels devrons-nous nous procurer par nous-mêmes ? |  voir ci-dessus |
| QUESTIONS TECHNIQUES : | | |
|  | Quelles sont les fonctions précises du robot  ? | le robot doit déplacer une "raquette" rectangulaire afin d'intercepter un palet. Un mode "commande manuelle" sera réalisé en priorité, pour déplacer la raquette à l'aide de boutons ou potentiomètres.  |
|  | Quel modèle d'Arduino allons-nous utiliser ? | cf ci-dessus. |
|  | Comment transformer un smartphone en capteur ? | c'est une option que nous examinerons dans un second temps |
|  | Comment le lier à l'Arduino ? | idem |
|  |La table de palet sera-t-elle fournie ?|nous utiliserons les tables présentes dans les salles de tp à cet effet.|
|  |Utiliserons-nous tous des moteurs pas-à-pas, ou tous des moteurs à courant continu pour notre projet ?|cela dépend de ce que nous trouverons dans les imprimantes qui ne sont pas encore démontées. vous vous mettrez d'accord pour vous répartir le matériel disponible. |
|  |Et sinon, comment la répartition des moteurs se fera-t-elle entre les binômes ? |vous vous répartirez le matériel en bonne intelligence. si ça ne marche pas, nous tirerons au hasard.|
|Sur les livrables : | | |
| | nous avons pas compris comment remplir le tableau de satisfaction et nous ne comprenons pas l'exemple de celui-ci. | En gros, si vous remplissez les contraintes imposées, et que la fonction est réalisée, c'est +. Sinon c'est -. Si vous validez largement, c'est ++, si vous êtes loin du compte, c'est --. |
| | j'ai vu que vous demandiez un tableau de test de validation détaillé mais pouvez vous m'expliquer en quoi consiste ce tableau exactement. | Pour savoir si la fonction marche, il faut faire 10, 20, 30 tests. Vous notez le résultat de chaque test. A la fin, s'il y a 1 erreur sur 10 tests, vous avez 90% de réussite.|
| | Devons nous vous envoyer tout les éléments inclus dans la catégorie livrable (rapport + code + dessins + présentation) ? | Tous ceux dont vous disposez. |
| | Dans le modèle de rapport sur Gitlab, il est précisé que le code doit être en Annexe, or nous devons également joindre le code au fichier zip (dépendamment de la première question). Dans lequel de ces deux endroits poster le code ? | Vous mettez tout les fichiers de code dans le zip. Dans le rapport, si vous le souhaitez, vous copiez-collez des bouts de code dont vous êtes contents, car vous avez apporté quelque chose d'important. |
| | Le fichier zip mentionne le nom « présentation pdf » , est ce que c’est celle que nous utiliserons pour notre soutenance de jeudi ?| C'est celle que vous avez sous la main le jour d'envoi limite. Ca n'est pas un problème si la présentation le jour de la soutenance est un peu différente.|
| | La partie « Soutenance » dans le doc d’aide au livrable et à la soutenance mentionne le terme de « diapositives ». Devons nous accompagner notre oral d’une présentation power point ou d’un fichier pdf ? | Le pdf est mieux, car plus stable. Vous pouvez venir avec un ppt, mais soyez sûr d'avoir le logiciel sur votre portable. |
| | Dans le rapport, on nous parle de CDC Technique. Où le trouver ?| Sur la page du projet. |
 

# Extensions

*  commande manuelle avec smartphone par bluetooth/wifi
*  asservissement visuel: cmu cam, smartphone.

