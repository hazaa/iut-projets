Récupération d'alimentation ATX
======



Exemples de cartes électroniques à connecter à une alimentation ATX
------

Nous devrons concevoir et réaliser une "breakout board", c'est à dire une carte
électronique qui simplifie l'accès à un composant (ici l'alimentation) pour l'utilisateur.
Notre carte devra être simple.


Exemples complexes de breakout boards:

*    http://b.truzzi.me/building-a-better-breakout-board-for-atx-psus-2/
*    dangerous prototype  [ATX Breakout Board](http://dangerousprototypes.com/docs/ATX_Breakout_Board)
     *   design: http://dangerousprototypes.com/docs/ATX_Brakout_Board_design_overview
     *   fournisseurs:  http://boutique.semageek.com/fr/163-atx-breakout.html, http://www.seeedstudio.com/depot/atx-breakout-board-bench-power-supply-p-1222.html?cPath=155
     *   fichiers v2: https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/dangerous-prototypes-open-hardware/ATX_PSU.v2_KiCAD_20120911.zip
     *   fichiers v1: https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/dangerous-prototypes-open-hardware/ATX.source.r1949.v1.1.zip
*    lextronic (+fichiers CAD): http://www.lextronic.fr/P19198-platine-bench-pour-alimentation-atx.html
*    ATX+voltage regulator: http://hackaday.com/2014/05/12/not-your-typical-atx-power-supply-hack/


Exemples de boitiers
------

Nous devons protéger notre carte et dissimuler tous les fils.
Ci dessous des exemples de boitiers.


*    http://makezine.com/projects/computer-power-supply-to-bench-power-supply-adapter/
*    boitier pour alimentation atx: http://dangerousprototypes.com/2012/08/29/diy-atx-case/
*    boitier pour breakout board: http://boutique.semageek.com/fr/164-boitier-de-protection-pour-atx-breakout.html

Attention: le meilleur boitier est celui avec le moins de matière. Sachant que les 
alimentation ATX sont déjà protégées par un boitier en métal, on peut en profiter 
pour réduire la quantité de matériau utilisé. 


Logiciels d'EAD (electronic assisted design)
------

Il en existe plusieurs libres (kicad, fritzing) et payants (proteus, eagle).
Pour vous permettre de travailler chez vous, nous choisissons une solution libre

*    [fritzing](http://fritzing.org), [FAQ geii](fritzing.md)
*    [tutoriels](http://fritzing.org/learning/translations/) fritzing en français
*    videos: en [anglais](https://www.youtube.com/watch?v=Hxhd4HKrWpg), en français ?

Logiciels de CAD (computer assisted design) 2D et 3D
------

Nous aurons besoin de ces logiciels pour le boitier.
Nous utliserons surtout le logiciel libre de dessin vectoriel 2D inkscape, 
qui est installé à l'IUT, et que vous pourrez installer chez vous.

*    [inkscape](http://inkscape.org)
*    appli en ligne pour générer des plans de boitier: [makercase](http://www.makercase.com)


Composants
------

*    specifications du connecteur: http://pinouts.ru/Power/atxpower_pinout.shtml
*    fiches techniques
     *     [24 pins droits](http://fr.rs-online.com/web/p/embases-de-circuit-imprime/5109809/, http://www.lextronic.fr/P30375-connecteur-atx-droit.html) c'est **celui dont nous disposons**.
     *     [20 pins coudé](http://fr.rs-online.com/web/p/embases-de-circuit-imprime/4475975/) pas dispo
     *     [20 pins coudé](http://www.mouser.fr/ProductDetail/Molex/39-29-1208/?qs=EaMIwIL37xyUmjao7yWvYg%3d%3d) pas dispo



Contraintes de conception pour la carte électronique
------

*   protéger les sorties de tension vers l'utilisateur par des diodes. 

Contraintes de fabrication pour la carte électronique
------

Voir la page [fraiseuse](pcb_drill.md)


Validation
------

Lors de la conception:

*    utiliser la fonction Fritzing: Routage > vérification des règles de conception

Après production:

*    tester votre carte avec le multimètre (en position "diode"), SANS alimentation:
    *    vérification des "bons contacts": pour chaque pin 3.3V du connecteur, vérifier qu'il est relié à la sortie 3.3. Recommencer avec 5,12V et la masse.
    *    vérification des faux contacts: vérifier s'il y a contact pour toutes les combinaisons possibles: (masse ; 3.3), (masse ; 5), (masse ; 12), (3.3 ; 5), etc... 
    *    vérification de la protection des diodes: recommencer le test "bon contact" en inversant la polarité du multimètre.
*    test de l'alimentation ATX (en laissant votre carte électronique de côté): 
    *    brancher l'alim ATX au secteur. Avec un fil, relier le pin "power on" à la masse.
    *    vérifier que le ventilateur tourne.
    *    vérifier la tension aux bornes des pins 3.3, 5, 12 V.
*    tester votre carte avec l'alimentation ATX, **uniquement si les test précédent sont OK**:
    *     brancher l'alimentation ATX au secteur, et à votre carte. Vérifier que le ventilateur tourne. Tester avec un multimètre les tensions aux bornes des sorties 3.3, 5, 12V.

Vous synthétiserez ces résultats dans un tableau dans votre rapport.
NB: Ce tableau est différent du tableau de satisfaction global des critères pour chaque fonction.


Extensions 
------

Uniquement pour celles et ceux qui auront tout fini.

*    voltage regulator (LM317-based)


Questions/réponses
------


| Question | Réponse | 
| -------- | -------- | 
| Nous aurions par exemple aimé savoir si une carte électronique doit être réalisé par exemple pour redresser le type de tension fournis.  | la carte a réaliser n'effectuera pas de redressement. |
| D'après ce que nous avons compris cette alimentation permet d'obtenir du 3.3/5/12V à partir d'un câble et nous avons alors conclu que notre projet consistait à séparer chaques câble et les relier à un bornier pour la transformer en alimentation d'établie et permettre de brancher différents type de machine qui nécessitent chacune différentes tensions. Nous aurions donc aimé savoir si tel était bien notre projet ? | Les alimentations ATX sont standard et présentes dans la plupart des PC de bureau. Vous en avez surement chez vous. En sortie de ces alimentations, on a de nombreux cables et un connecteur standard, décrit sur le site gitlab. Vous devez concevoir et produire une carte électronique sur laquelle poser ce connecteur, et ajouter des fiches banane femelle. De plus, des diodes seront ajoutées pour éviter un mauvais usage de ces fiches. |
| De plus, concernant la boîte en carton à réaliser au FabLab, nous voulions savoir si les dimensions sont définis ou si nous devons prendre des initiatives quant à cette boîte car nous avons aucune séance TP avant la pré-étude.  |  Vous devez prendre des initiatives et bien lire ce que j'ai écrit sur le site gitlab. |
| Si je veux mettre une LED qui indique que l'alimentation est sous tension, sur quel pin du connecteur ATX ? ||
| Est-ce que je peux mettre les fiches BNC directement sur la carte ? | Comme on est en simple face, et que le connecteur ATX sort du même côté, on ne pourra pas poser la carte en facade.  |
| Comment router le connecteur ATX ?  | dans fritzing, en cliquant sur chaque pin, vous saurez quel est le voltage.  |
| Schéma électrique: faut-il ajouter la masse ?  | oui  |
| Peut-on faire passer une piste entre deux pastilles du connecteur ATX ?  | Non, voir prototype de carte de l'an dernier.  |
