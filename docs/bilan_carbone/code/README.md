Analyse emploi du temps ADE sous Python 3
======


Avec [ADE](https://ade.u-pec.fr/direct/) on peut faire des extractions de l'emploi du temps.

Ci-dessous on présente plusieurs manières d'estimer le nombre de jour de présence à l'IUT.
L'approche **la plus simple** est l'analyse de l'emploi du temps d'un étudiant.
Les autres sont données à titre de comparaison.

Pré-requis:

* avoir installé [Thonny](https://thonny.org/)
* avoir téléchargé ```ade_read.py```
* avoir installé le module icalendar: cf [wiki Thonny](https://github.com/thonny/thonny/wiki/InstallingPackages)
* dans Thonny, dans la partie Python, avoir importé les fonctions de ```ade_read.py```:
   ```from ade_read import *```


[[_TOC_]]

Dans ade_read.py vous trouverez plusieurs autres fonctions utiles.

* ```simple_read()```: lit le fichier ics et affiche une partie des champs.
* ```ics2csv()```: conversion d'un fichier ics vers csv


Nombre d'heures théorique
------

Sur la page web de chaque département, vous trouverez une fiche descriptive avec le nombre d'heures total.
Par exemple pour le [GEII](https://www.iutsf.u-pec.fr/nos-formations/but-genie-electrique-et-informatique-industrielle-geii), cliquez sur "Télécharger la fiche BUT GEII".

Dans la fiche on peut lire:

```
Formation initiale classique
1 800 heures sur 6 semestres de septembre à fin juin
```

Cela fait 900h par an. Si on divise ces 900h par 8h par jour, on obtient une 
première approximation (imprécise) du nombre de jours de présence.
On n'utilise pas ADE, pas besoin de programmer.


Emploi du temps ADE d'un étudiant
------

Ici, pour **un** étudiant, on télécharge son emploi du temps complet à l'année.
A partir de ce fichier ics, on calcule son nombre de jours de présence unique avec la fonction ci-dessous.

* ```get_nb_days_student_calendar()```: 

Pour tester cette fonction, faites comme dans la fonction ```test_get_nb_days_student_calendar()```:
dans votre intepréteur python, taper une par une les lignes contenues dans ```test_get_nb_days_student_calendar()```.

Pour un département donné à l'IUT, on peut donc calculer le nombre de jours de présence pour chaque groupe d'étudiants.


Emploi du temps ADE des groupes
------

Ici on récupère, pour chaque jour, la présence de tous les groupes.

ATTENTION: à cause de limitations du serveur, il semble qu'on ne puisse pas extraire plus qu'**un mois** à la fois. A vous de tester.


* ```make_dict_by_date()``` à partir du fichier ics, crée un dictionnaire dont les keys sont les jours de l'année. Exemple de sortie:

```
dict_by_date = 
{ '2018-10-01': {'1A': {'PROMO': {'nb': 90, 'present': 1},
   'TD A': {'nb': 30, 'present': 1},
   'TD B': {'nb': 30, 'present': 1},
   'TD C': {'nb': 30, 'present': 1},
   'TP A1': {'nb': 15, 'present': 1},
   'TP A2': {'nb': 15, 'present': 1},
   'TP B1': {'nb': 15, 'present': 1},
   'TP B2': {'nb': 15, 'present': 1},
   'TP C1': {'nb': 15, 'present': 1},
   'TP C2': {'nb': 15, 'present': 1}},
  '2A': {'PROMO FA': {'nb': 30, 'present': 1},
   'PROMO S3': {'nb': 90, 'present': 1},
   'PROMO S4': {'nb': 90, 'present': 0},
   'TD A': {'nb': 30, 'present': 1},
   'TD B': {'nb': 30, 'present': 1},
   'TD FA': {'nb': 30, 'present': 0},
   'TD FA1': {'nb': 15, 'present': 0},
   'TD FA2': {'nb': 15, 'present': 0},
   'TP A1': {'nb': 15, 'present': 0},
   'TP A2': {'nb': 15, 'present': 0},
   'TP B1': {'nb': 15, 'present': 0},
   'TP B2': {'nb': 15, 'present': 0}}}}
```

Pour tester cette fonction, faites comme dans la fonction ```test_make_dict_by_date()```, en changeant le chemin du fichier en fonction de votre cas.

Pour savoir si le groupe TP B2 en 2A était présent le "2018-10-01", il faut alors taper:

```
dict_by_date["2018-10-01"]["2A"]["TP B2"]["present"]
```

Exemple de contenu d'un fichier ics
------

```
BEGIN:VCALENDAR
METHOD:PUBLISH
PRODID:-//ADE/version 6.0
VERSION:2.0
CALSCALE:GREGORIAN
BEGIN:VEVENT
DTSTAMP:20201204T073614Z
DTSTART:20201202T073000Z
DTEND:20201202T113000Z
SUMMARY:OL
LOCATION:Teams
DESCRIPTION:\nGR TP C2 S1\nSORANZO GABRIEL\n(Modifié le:26/11/2020 12:11)
 
UID:ADE6050524f4a455455504543323032302d323032312d373038342d322d30
CREATED:19700101T000000Z
LAST-MODIFIED:20201126T111100Z
SEQUENCE:2033143860
END:VEVENT
```

Sans Python (ne pas utiliser)
------


* conversion ics/csv: le format csv peut être lu par libreoffice/excel.

  * avec le code du prof: [ade_read.py](docs/bilan_carbone/code/ade_read.py), fonction ```test_ics2csv()```
  * autres: 
    * outlook (calendrier, import ics), google
    * https://github.com/prometheus42/libreoffice-ical-importer (nécessite libreoffice+python)
    * site en ligne https://www.projectwizards.net/fr/support/ics2csv-converter
* sous libreoffice:
  * ouvrir le fichier .csv obtenu à partir du fichier .ics
  * utiliser les fonctions libreoffice appropriées: fonctions [classeur](https://help.libreoffice.org/7.0/fr/text/scalc/01/04060109.html?DbPAR=CALC), [base de données](https://help.libreoffice.org/latest/fr/text/scalc/01/04060101.html?DbPAR=CALC). Exemple: RECHERCHEV, SOUS.TOTAL,  [filtres](https://help.libreoffice.org/7.0/fr/text/scalc/guide/specialfilter.html?DbPAR=CALC)... 

