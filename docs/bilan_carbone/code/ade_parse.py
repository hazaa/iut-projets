#################################
def count_from_dict(dict_by_date):
    """
    compter le nombre de présents pour chaque jour de dict_by_date
    """
    d_new = {}
    def func(it,dic,y,d,label):
        if it[y]['TD '+label]['present']:    
            dic[d] +=  it[y]['TD '+label]['nb']
        else:
            for grp in ['1','2']:
                if it[y]['TP '+label+grp]['present']:    
                    dic[d] +=  it[y]['TP '+label+grp]['nb']                        
    
    for day,item in dict_by_date.items():        
        d_new[day] = 0
        for year in ['1A','2A']:
            if item[year]['PROMO']['present']: 
                d_new[day] +=  item[year]['PROMO']['nb']                
            else:    
                for lab in ['A','B','C','FA']: func(item,d_new,year,day,lab)            
    return d_new

#################################
# 1st try    -- DEPRECATED

def test_make_dict_by_date():
    """
    """
    # read ics file
    #filename = "data/ADECal_GEII.ics"
    filename = "/home/aurelien/local/git/iut-projets/docs/bilan_carbone/data/ADECal_GEII.ics"
    fname_in= filename           
    f = open(fname_in,'r')
    gcal = ical.Calendar.from_ical(f.read())
    # create dictionary from ics
    myparser= parser_simple
    #myparser= parser_count
    dict_by_date = make_dict_by_date(gcal,parser=myparser)
    # analyze dict
    f.close()   


def parser_simple(component):
    """
    analyse le contenu
    
    #list_grp = ["TD B", "GR TP C2", "GR TP A1 S1", "DS", "PROMO FA2", "PROMO S3","LP SARII TP1"]
    """
    output_str = ''
    list_semester = ['S1','S2','S3','S4','FA', 'LP']
    # which semester
    descr_vtext = component['DESCRIPTION']
    for x in list_semester:
        if descr_vtext.find(x)>0 :
            output_str += x
            break
    # HERE: YOUR CODE        
    return output_str 

def make_dict_by_date(gcal,parser=parser_simple):
    """
    make a dictionary. Keys are dates found in .ics file.
    
    parameters:
    ----------
    gcal: icalendar.gcal object
    
    outputs:
    ---------
    dict_by_date: dict
    """
    dict_by_date = {}
    # make dict
    for component in gcal.walk(name='VEVENT'):        
        # get iso date
        vdate_iso=component['DTSTART'].dt.isoformat()[0:10]
        # add parsed content
        if dict_by_date.get( vdate_iso ) is None:
            # if nothing in the dict at that date, add an empty list
            dict_by_date[ vdate_iso ] = []
        else:    
            # if not empty, append to the list
            dict_by_date[ vdate_iso ].append(parser(component))              
    return dict_by_date
     
    




# -------------------------------------------------
# DEPRECATED
def parser_GEII(component):
    """
    analyse le contenu
    
    #list_grp = ["TD B", "GR TP C2", "GR TP A1 S1", "DS", "PROMO FA2", "PROMO S3"]
    # LP ?LP GRENEL "LP SARII TP1"
    """
    output_str = ''
    list_semester = ['S1','S2','S3','S4', 'FA', 'LP']
    # which semester
    descr_vtext = component['DESCRIPTION']
    for x in list_semester:
        if descr_vtext.find(x)>0 :
            output_str += x
    # is promo/TD/TP ?
    if descr_vtext.find("PROMO")>0 :
        output_str += ' PROMO'
    elif descr_vtext.find("TD")>0:
        output_str += ' TD'
    elif descr_vtext.find("TP")>0:    
        output_str += ' TP'
    return output_str  


def update_dict_by_date(dict_by_date, date_iso, component):
    """
    analyse le contenu
    renvoie un dictionnaire de dictionnaires:
    """

    # 'PROMO' was found in description 
    descr_vtext = component['DESCRIPTION']
    if descr_vtext.find('PROMO')>0 :
        if descr_vtext.find('S1')>0 or descr_vtext.find('S2')>0:
            #dict_by_date[date_iso]['1A']['PROMO'].append(1)            
            dict_by_date[date_iso]['1A']['PROMO'] += 1
        elif descr_vtext.find('S3')>0 or descr_vtext.find('S4')>0: 
            #dict_by_date[date_iso]['2A']['PROMO'].append(1)
            dict_by_date[date_iso]['2A']['PROMO'] += 1
    else:     
    # 'TD' was found in description
    # 'TP' was found in description
        pass

def make_dict_by_date2(gcal):
    """
    make a dictionary. Keys are dates found in .ics file.
    
    parameters:
    ----------
    gcal: icalendar.gcal object
    
    outputs:
    ---------
    dict_by_date: dict
    """
    dict_by_date = {}
    #d_temp = {'PROMO':[],'TDA':[],'TDB':[],'TDC':[], 'TPA1':[], 'TPA2':[], 'TPB1':[], 'TPB2':[], 'TPC1':[], 'TPC2':[]}
    d_temp = {'PROMO':0,'TDA':0,'TDB':0,'TDC':0, 'TPA1':0, 'TPA2':0, 'TPB1':0, 'TPB2':0, 'TPC1':0, 'TPC2':0}
    d_1day =  {'1A': d_temp.copy(),  '2A': d_temp.copy()}
    # make dict
    for component in gcal.walk(name='VEVENT'):        
        # get iso date
        vdate_iso=component['DTSTART'].dt.isoformat()[0:10]
        # add parsed content
        if dict_by_date.get( vdate_iso ) is None:
            # if nothing in the dict at that date, add an empty list
            dict_by_date[ vdate_iso ] = d_1day.copy()          
        # update
        update_dict_by_date(dict_by_date, vdate_iso, component)
    return dict_by_date
