# -*- coding: utf-8 -*-
"""
Author: Aurélien Hazan

REFS:
* icalendar: https://pypi.org/project/icalendar/
            https://icalendar.readthedocs.io/en/latest/

"""
import icalendar as ical
import csv

###################################################################
# LECTURE SIMPLE 


def test_simple_read():
    """
    """
    fname_in=  "../data/ADECal_GEII_septembre_2018.ics"
    simple_read(fname_in)

def simple_read(fname):
    """
    lit le fichier ics et affiche une partie des champs.
    """
    f = open(fname,'r')
    gcal = ical.Calendar.from_ical(f.read())
    for component in gcal.walk(name='VEVENT'):
        #print(component.name)
        print('-------------------------')
        print(component['SUMMARY'])
        print(component['DESCRIPTION'])
        print('-------------------------')
        # component content
        vT = component['DESCRIPTION'] 
        vT.find('TP') 
        vdate=component['DTSTART'].dt
        break
    f.close()

###################################################################
# CONVERSION .ICS -> .CSV
# (POUR FAIRE L'ANALYSE SOUS LIBREOFFICE)

def test_ics2csv():
    """
    fonction qui permet de tester la fonction ics2csv()
    """
    filename = "data/ADECal_GEII.ics"
    #filename = "/home/aurelien/local/git/iut-projets/docs/bilan_carbone/data/ADECal_GEII.ics"

    fname_in= filename
    fname_out="/home/aurelien/local/git/iut-projets/docs/bilan_carbone/data/ADECal_GEII.csv"
    ics2csv(fname_in,fname_out)

def ics2csv(fname_in,fname_out):
    """
    lit un fichier ics en entrée, copie les valeurs de certains champs
    (SUMMARY, DTSTART, DESCRIPTION) dans le fichier csv en sortie.
    
    parameters:
    ----------
    fname_in: str
    nom du fichier ics en entrée
    
    fname_out: str
    nom du fichier csv en sortie
    """
    # open files
    f_in = open(fname_in,'r')
    f_out = open(fname_out, 'w', newline='\n') 
    # create csv writer
    writer = csv.writer(f_out, delimiter=';',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
    # create ics reader
    gcal = ical.Calendar.from_ical(f_in.read())
    # loop on vevents in gcal object
    for component in gcal.walk(name='VEVENT'):
        # get date of the event, transform it to iso format '2020-12-11'
        vdate_iso=component['DTSTART'].dt.isoformat()[0:10]
        # write to csv file
        writer.writerow([ component['SUMMARY'], component['DESCRIPTION'], 
                          vdate_iso  ])    
    # close all files                      
    f_in.close()        
    f_out.close()        
 
###################################################################
# ANALYSE PYTHON - NOMBRE DE JOURS DE PRESENCE D'UN ETUDIANT
 
def test_read_1_student_calendar():
    """
    
    ATTENTION: les noms de fichiers sous Thonny avec windows:
     "C:\\Users\\aurel\\Desktop\\projet\\test.ics"
    """
    fname_in=  "/home/aurelien/local/git/iut-projets/docs/bilan_carbone/data/ADECal_etudiant_GEII_S3S4_2018_2019.ics"
    n = get_nb_days_student_calendar(fname_in)
    print('nombre de jours de présence distincts: {}'.format( n ))
 
def get_nb_days_student_calendar(fname_in):
    """
    Read ics file with calendar of 1 student.
    Compute number of unique dates.
    
    Inputs:
    -------
    fname_in: str
    full path of ics file
    
    Outputs:
    ---------
    n: int
    number of unique dates
    """
    # open file, create object with type: icalendar.cal.Calendar
    f = open(fname_in,'r')
    gcal = ical.Calendar.from_ical(f.read())
    # create an empty list to store the dates
    l_date = []
    # loop through events in calendar
    for component in gcal.walk(name='VEVENT'):        
        # get iso date of current event
        vdate_iso=component['DTSTART'].dt.isoformat()[0:10]
        #  check if date already in list
        if not (vdate_iso in l_date): 
            # append to list
            l_date.append(vdate_iso)
    # get list length        
    n = len(l_date)
    return n
    
###################################################################
# ANALYSE PYTHON - EMPLOI DU TEMPS DES GROUPES D ETUDIANTS


def test_make_dict_by_date():
    """
    """
    # read ics file
    #filename = "data/ADECal_GEII.ics"
    #filename = "/home/aurelien/local/git/iut-projets/docs/bilan_carbone/data/ADECal_GEII.ics"
    fname_in=  "/home/aurelien/local/git/iut-projets/docs/bilan_carbone/data/ADECal_GEII_septembre_2018.ics"
    f = open(fname_in,'r')
    gcal = ical.Calendar.from_ical(f.read())
    dict_by_date = make_dict_by_date(gcal)
    f.close() 


def update_dict_by_date(dict_by_date, date_iso, component):
    """
    analyse le contenu
    modifie le dictionnaire dict_by_date
    """
    descr_vtext = component['DESCRIPTION']
    # year:  'S*' was found
    year = 0
    group = 0
    if descr_vtext.find('S1')>0 or descr_vtext.find('S2')>0:
        year = '1A'
    elif descr_vtext.find('S3')>0 or descr_vtext.find('S4')>0 or descr_vtext.find('FA')>0:    
        year = '2A'        
    # 'PROMO' was found in description 
    if year in ['1A','2A']:
        for group in list(dict_by_date[date_iso][year].keys()) :
            if descr_vtext.find(group)>0 : 
                dict_by_date[date_iso][year][group]['present'] = 1


def make_dict_by_date(gcal):
    """
    make a dictionary. Keys are dates found in .ics file.
    
    parameters:
    ----------
    gcal: icalendar.gcal object
    
    outputs:
    ---------
    dict_by_date: dict
    """
    dict_by_date = {}
    #d_temp = {'PROMO':[],'TDA':[],'TDB':[],'TDC':[], 'TPA1':[], 'TPA2':[], 'TPB1':[], 'TPB2':[], 'TPC1':[], 'TPC2':[]}
    #d_temp = {'PROMO':0,'TDA':0,'TDB':0,'TDC':0, 'TPA1':0, 'TPA2':0, 'TPB1':0, 'TPB2':0, 'TPC1':0, 'TPC2':0}
    d1A = {'PROMO': {'present': 0, 'nb': 90},
          'TD A' : {'present': 0, 'nb': 30},
          'TP A1' : {'present': 0, 'nb': 15},
          'TP A2' : {'present': 0, 'nb': 15},
          'TD B' : {'present': 0, 'nb': 30},
          'TP B1' : {'present': 0, 'nb': 15},
          'TP B2' : {'present': 0, 'nb': 15},
          'TD C' : {'present': 0, 'nb': 30},
          'TP C1' : {'present': 0, 'nb': 15},
          'TP C2' : {'present': 0, 'nb': 15},
           }
    d2A = {'PROMO S3': {'present': 0, 'nb': 90},
         'PROMO S4': {'present': 0, 'nb': 90},
         'PROMO FA': {'present': 0, 'nb': 30},
          'TD A' : {'present': 0, 'nb': 30},
          'TP A1' : {'present': 0, 'nb': 15},
          'TP A2' : {'present': 0, 'nb': 15},
          'TD B' : {'present': 0, 'nb': 30},
          'TP B1' : {'present': 0, 'nb': 15},
          'TP B2' : {'present': 0, 'nb': 15},
          'TD FA' : {'present': 0, 'nb': 30},
          'TD FA1' : {'present': 0, 'nb': 15},
          'TD FA2' : {'present': 0, 'nb': 15}
           } 
    d_1day =  {'1A': d1A.copy(),  '2A': d2A.copy()}
    # make dict
    for component in gcal.walk(name='VEVENT'):        
        # get iso date
        vdate_iso=component['DTSTART'].dt.isoformat()[0:10]
        # init dict_by_date
        if dict_by_date.get( vdate_iso ) is None:
            # if nothing in the dict at that date, add an empty list
            dict_by_date[ vdate_iso ] = d_1day.copy()          
        # add parsed content, update dict_by_date
        update_dict_by_date(dict_by_date, vdate_iso, component)
    return dict_by_date

