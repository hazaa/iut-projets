# TP vision/réseau

*    [sujet](tp-vision-nn-reseau.md)
*    deps:
    *    developpement: [node](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions), [yarn](https://yarnpkg.com/lang/en/docs/install/), [deeplearnjs](http://deeplearnjs.org)
    *    websocket: [client py](https://github.com/websocket-client/websocket-client)
*    script utiles:
    *    ./script/watch-demo demos/imagenet    
*   deploiement en salle TP:  
    *    d'abord l'un des scripts (?) build-demo ou make-website   
    *    puis servir à partir du répertoire deeplearjs: 
        >    python -m SimpleHTTPserver
   
