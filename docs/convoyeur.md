Convoyeur et tri d'objets
======


*    [Introduction](#introduction)
*    [TP](#TP) préparatoires
*    [Cahier des charges](#cahier-des-charges) 
*    [Livrables](#livrables)
*    [FAQ](#FAQ)


# Introduction

Votre objectif dans ce projet est de concevoir et réaliser un dispositif de tri d'objets.
Il utilise un dispositif optique pour détecter les objets intéressants, et
une partie mécanique simple pour convoyer les objets, et en mettre certains de côté.

![convoyeur](fig/convoyeur.png)

Le schéma de principe des différents sous-systèmes est le suivant:

![schema](fig/convoyeur_schema_principe.png)


Le calendrier :

| Groupe | Semaine 51 |  2 | 3 | 4 |
| -------- | -------- | -------- | -------- |-------- |
| B1 | début |  fablab(8/01)| soutenance | |
| A1 |  |  |  début,fablab(16/01)| soutenance |

# TP

Les connaissances nécessaires pour ce projet seront rappelées au cours de plusieurs
mini TP (programmation python, programmation arduino, électronique de base).
Un livrable sera demandé pour vérifier l'acquisition des connaissances.

*   [TP Python](tp-python.md) 
*   [TP Arduino](tp-arduino.md)
*   [TP fablab](tp-fablab.md) 
*   [TP vision/réseau](tp-vision-nn-reseau.md)


# Cahier des charges 

Le cahier des charges fonctionnel :

| Nom fonction | Description fonction | Critère |  Valeurs limites  |
| -------- | -------- | -------- | -------- |
| Convoyeur | faire avancer des objets  | vitesse | >0.5cm/s  |
| Détection | détecter des objets intéressants | taux d'erreur  | <10% |
| Tri | mettre de côté des objets | taux d'erreur   | <10% |

Le cahier des charges techniques (voir les [notices techniques](https://gitlab.com/hazaa/iut-projets/docs/) correspondantes):

*    Partie électro-mécanique:
   *   servomoteur analogique de type HS-322.
   *   moteur à courant continu digilent PmodHB5.
   *   micro-contrôleur Arduino Uno ou Mega
*    Programmation:
   *   Python 2  
   *   Arduino IDE      
*    Vision:
   *   webcam USB
   

# Livrables

On vous demande de mener à bien les taches suivantes

*    Conception et Réalisation (découpe, assemblage de la partie mécanique, cablage électrique).
*    Validation:
   *   Validation des performances du tri.
   

Les livrables du projet sont les suivants:

*   un prototype à rendre à la fin du projet.
*   soutenance: 10 minutes, avec une partie en anglais de 2 minutes par étudiant. 15 minutes de questions du jury. Présentation de la satisfaction du cahier des charges (++,+,-,--) pour chaque critère.
*   une vidéo vivante (exemple [popfab](https://vimeo.com/45911972)), pour être ajoutée sur le [blog GEII](http://iutsfgeii.blogspot.fr/). ATTENTION: ne pas mettre la vidéo dans le zip, mais utiliser un site d'échange de fichiers lourds, comme [framadrop](https://framadrop.org) (<100M) et ajouter le lien vers cette vidéo dans le zip.
*   un fichier zip contenant le rapport à partir du modèle fourni (avec un rapport des tests de validation, et le GRAFCET/algorithme pour le tri), tous vos programmes (arduino, python, inkscape), un "post" décrivant votre projet.

Lisez IMPERATIVEMENT la [description détaillée des livrables ici](livrable_soutenance.md).


# FAQ

La FAQ sera alimentée en cours de projet avec vos retours.

| Question | Réponse | 
| -------- | -------- | 


## Commandes linux de base

*   ls
*   mkdir DIR
*   cp F1 DIR/
*   mv F1 F2
*   ps -aux
*   kill -9 PID
