FAQ fritzing geii
======

Comment l'[installer](http://fritzing.org/download/?donation=0#install)
 

Voir en priorité les tuto fritzing en [français](http://fritzing.org/learning/translations)
 
Attention:

*    se mettre absolument en **simple face**, sinon votre carte sera **inutilisable**.
*    utiliser la fonction Routage > vérification des règles de conception
 
 
| Question | Réponse | 
| -------- | -------- | 
| Comment régler la largeur des pistes ?  |  Menu routage, paramètres d'autoroutage. Cliquer sur "personnalisé", vous accédez à un nouveau menu.|
| Quel type de carte choisir ?  | rectangulaire  | 
| Simple face ou double face ?  |  simple face |
| Quel type de diode choisir ?  | celles qui ont la même empreinte que les résistances   |
| Comment ajouter un trou ?  | un grand trou: fiche "banana" ; un petit trou: "via", dont vous pouvez régler le diamètre. |
| Aimantation de la grille: comment la régler ?  |  Menu vue > définir la taille de la grille. |
| Quels fichiers pour la machine-outil ?  |  Fichiers > exporter > pour la production > gerber |
| pourquoi le composant "ATX" dans fritzing a un dessin qui n'est pas superposé avec les pins ? |  car le composant fritzing correspond à un connecteur coudé, différent du notre qui est droit. |




