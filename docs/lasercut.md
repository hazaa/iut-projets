# DECOUPE LASER


##   Liens utiles

*     cours découpe laser [web](http://hazan.xyz/lasercut)
*     les plugin inkscape de la [cité des sciences](http://carrefour-numerique.cite-sciences.fr/fablab/wiki/doku.php?id=logiciels:inkscape)
     *    voir le plugin "export de fichiers .tsf"

##   Sujet de TP

*    intro séance 1: découpe laser
*    6 séances + 1 soutenance

*    intro séance 1: découpe laser
     *    pourquoi ? car on a besoin de pièces  pour notre projet : boitier, ou bras robot.
     *    On le fait au début pour que vous ayez le temps de revenir.
     *    livrable: selon le prohjet : un boitier ; un bras.  Design et la technique de fabrication libres. 

*    prise en main inkscape
    *    nouveau; Fichier>Propriétés du doc>unités par défaut, unité -> mm
    *    Affichage	> Grilles, Aimanter (icône)
    *    Editer les couleurs: fond (pas de remplissage), contour, épaisseur 0.05mm
	*    outils : Alignement

*    exos inkscape: 
    *    tracé de découpe simple (rond, carré) avec la bonne couleur pour la découpe		
    *    carré dont un des côtés est un demi-cercle (avec union)				
    *    tracé pour assemblage perpendiculaire (finger); tslot


*    prise en main découpe:
    *    génération du fichier tsf: 
        *    avec plugin inkscape ou sur la machine de la salle.
        *    Extensions>Fablab>découpe laser trotec

*    exo découpe: 
    *    reprendre exemples ci-dessus
	*    ajouter boite à encoches plugin Fablab

*    sujets :
    *    MeArmV2
         *    doc MeArmV2, github, fichier final dxf
         *    pipeline dxf -> inkscape + plugin -> trotec
         *    exercice: modifier le dxf pour mettre gros servo à la place du petit
    *    ATX : ?
                   
*    assemblage:
    *    présentation des pièces disponibles
    *    commencer le modèle en acrylic déjà dispo

*    TODO etudiants:
    *    revenir si nécessaire au fablab (cf horaires d'ouverture mercredi 18-20, jeudi 17-20)
    *    finir assemblage avec servos.
    *    Obtenir un objet fonctionnel
