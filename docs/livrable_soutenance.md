# Livrables et soutenances

Livrables
------

Vous remettrez sur eprel, à la date indiquée par l'enseignant, un fichier au format ZIP uniquement:

*    nom du fichier : 2018_ER2_BINOME1_BINOME2.zip
*    contenu du zip:
   *   votre rapport (au format odt ou pdf, **pas en doc ou docx**), en suivant le modèle fourni sur [gitlab](https://gitlab.com/hazaa/iut-projets/tree/master/docs)
   *   vos fichiers: programmes informatiques (arduino,python,...), dessins et plans (inkscape, fritzing, proteus,...), présentation pdf.

*    contenu du rapport:
   *  tous les sujets: tableau de test de validation détaillé, tableau de satisfaction synthétique (voir ci-dessous).  
   *  sujet alim ATX:		
      *  ajouter un tableau précisant le pourcentage de connecteur routés pour chaque tension de sortie (3.3, 5, 12)
      *    coût: mentionner la surface totale de votre carte électronique
      *    coût: mentionner la surface totale de matériau utilisé pour le boitier
   *  sujet MeArm: 
		    


L'écart à ces consignes pourra être pénalisé.


## Tableau de satisfaction: comment le remplir

Pour chacun des critères du Cahier des charges fonctionnel (CDCf), nous devons calculer une ou plusieurs grandeurs et vérifier si elles remplissent les conditions imposées.

*    Si le critère est largement satisfait, la satisfaction est ++
*    Si le critère est à peine satisfait, la satisfaction est +
*    Si le critère est juste sous la barre, la satisfaction est -
*    Si le critère est largement sous la barre, la satisfaction est --

Exemple :

J'ai mesuré l'erreur de position lors du centrage. Je trouve 0.05m.
Le tableau de satisfaction sera donc :


| Fonction |  |  Libellé  | Critère | Seuil  | Satisfaction |
| -------- | -------- | -------- | -------- |-------- | -------- |
| F1 | F13 | centrer | précision | 0.01m| - |



## Soutenances


*    10 minutes par binôme. Tout dépassement sera **fortement pénalisé**. Vous devez répéter au moins 2 fois avant pour vérifier le timing. 
*    2 minutes en anglais par étudiant.
*    Les deux premières pages de votre présentation doivent faire apparaitre clairement le cahier des charges et le résultat obtenu (satisfaction des fonctions F0,F1...)
*    Vous devez au maximum **prouver** ce que vous dites au moyen de mesures expérimentales.
*    Vous devez indiquer le planning prévisionnel et final.
*    Vos diapositives de présentation seront numérotées.
*    Munissez vous d'un PC portable.



Exceptions:

*    si vous êtes un trinôme, 12 minutes au total, 2 min en anglais chacun.


Un planning des soutenances vous sera communiqué.


FAQ
------

| Question | Réponse | 
| -------- | -------- | 
|  | | 
