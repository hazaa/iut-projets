Bras robot 
======

Design bras
------

*    [MeArm v2](https://github.com/mimeindustries/MeArm/tree/v2)
    *   télécharger le fichier dxf
    *   l'ouvrir avec inkscape. NE PAS utiliser le redimensionnement automatique à la page A4.
    *   vérifier que les unités sont "mm" (voir les informations sur inkscape données ci-dessous)
    *   dimension des pièces: sélectionner la pièce, vous voyez s'afficher ses dimensions en mm dans la barre du haut (L et H)
*    [bibliothèque de formes](notices_techniques/MeArm/library-pattern-cutouts.svg) pour découpe laser, avec servos (normal et mini), arduino, etc...    
*    assemblage: [instructions](https://hackaday.io/project/16537/instructions), [video](https://youtu.be/az8JMvvetyU), apercu [3D](https://www.tinkercad.com/things/2yiEi523PiA-mearm-3d)

Fournitures
------

*    matériau de l'armature 
    *    dimension: 3mm x 212 mm x 373 mm    
    *    pour le prototype: carton (dur type calendrier) de récupération
    *    pour le produit fini: [PMMA](https://en.wikipedia.org/wiki/Poly%28methyl_methacrylate%29) __coulé__ 3mm (nom commercial: perspex,plexiglass). Attention, le PMMA __extrudé__ risque de se casser; ou MDF 3mm.
*    visserie: disponible à l'IUT
*    élastiques x 4
*    carte PCB [servo](https://github.com/mimeindustries/mearm-base-pcb) au format [kicad](http://kicad-pcb.org). A remplacer par l'empreinte [ServoPCB](notices_techniques/MeArm/ServoPCB-Edge.Cuts.svg) 
    *    extension: réaliser la carte avec la fraiseuse de table CIF de l'IUT ou du fablab.
*    micro-servos [MG90s](http://www.towerpro.com.tw/product/mg90s-3/):
    *    dimension: 22.8×12.2×28.5mm  
    *    générique: https://www.sparkfun.com/products/9065

Nous disposons d'un nombre limité de servos, et ils ne sont pas du même type que ceux employés par MeArm. 
Il faudra donc adapter le design.

*    nombre de micro-servos SG90s :  7 (NC)
*    nombre de servos               3 (NC)  + 6
*    nombre de moteurs PmodHB5      6

 
Résumé des fournitures nécessaires ([hackaday.io](https://hackaday.io/project/16537-mearm-raspberry-pi-edition) ): 
 
 
| Matériel nécessaire| Quantité nécessaire| Quantité disponible  | 
| -------- | -------- | -------- |
|7 	|	M3x6 Screws  | |
|4 	| 	M3x10 Screws |  |
|1  |  Arduino uno |   |
|4  |  Elastique  |    |
|1   |   plaque MDF 3mm |     |
|3   |   servo MG90s |       |
|3   |   servo horn |       |
 



Logiciels de dessin CAD 2D et 3D
------

CAD = computer assisted design

Nous utiliserons surtout le logiciel libre de dessin vectoriel 2D inkscape, 
qui est installé à l'IUT, et que vous pourrez installer chez vous.

*    2D: [inkscape](http://inkscape.org), [installation windows](https://fr.flossmanuals.net/inkscape/installation-sous-windows/), [video prise en main](https://youtu.be/7iujdCtF8cE), [guide](http://fr.flossmanuals.net/inkscape/) en français.
    *    toute mettre dans l'unité "mm" dans Fichiers>Propriétés du document>Unités par défaut , et Unités
    *    vérifier que tous les contours sont fermés (c'est nécessaire pour la découpe laser). Voir la vidéo pour savoir comment faire.
*    3D: [freecad](http://www.freecadweb.org/) 3D, permet de faire l'assemblage avec le module Assembly2. 


Si vous avez besoin de composants supplémentaires non disponibles, vous pouvez les fabriquer (impression 3D ou découpe laser):

*     adaptateur micro servo/ standard: [plate](https://www.thingiverse.com/thing:2144138), [box](https://www.thingiverse.com/thing:2087608)
*     servo horn: [9g](https://www.thingiverse.com/thing:195207)



Electronique, programmation, calibration des servos
------

Il faut piloter les servos. Nous utiliserons des cartes arduino, disponibles à l'IUT.
 

*    arduino uno: connexion, programmation, pilotage de servos: [arduino](http://eprel.u-pec.fr/eprel//nzltnwj/7603/document/2016_2017/co/activ_appren_arduino_motor.html) 
*    calibration des servos: 
*    commande en vitesse des servos (pour limiter leur vitesse de rotation):
*    esplora:

Validation
------

*    test électrique:
    *    controler indépendamment les 3 servos en position.
*    test mécanique:
	*    faire tourner manuellement les pièces reliées aux 3 servos. La rotation s'effectue.
	*    rigidité de l'ensemble.	
*    test complet:
    *    controler indépendamment les 3 axes du robot en position.


Bonus
------
A faire en dernier si vous avez tout fini.

*     limitation de la vitesse des servos (e.g. varspeed)
*     Commande de servos depuis python via l'interface série: voir ce [TP](tracking_visuel.md).
*     idem avec IHM python tk
*     idem avec LeapMotion



 
Questions/Réponses
------



| Q | R | 
| -------- | -------- | 
| "Comment modifier le design du robot sans connaître ses dimensions ? Concernant les dimensions de l'armature du robot, nous avons trouvé une trame avec toutes les figures géométriques qui le composent mais sans les dimensions." | vous disposez du fichier dxf de l'armature du robot. Pour avoir les dimensions, regardez la rubrique "Design bras" ci-dessus. |
| Comment modifier le design du robot sans connaître les dimensions du servomoteur et du MCC ?  | le [dessin technique du servo HS322HD](notices_techniques/HS322HD.pdf). Le [dessin technique du MCC](notices_techniques/moteur_DC_ig220053x00085r_ds.pdf) se trouve p.5 de la notice technique. | 
| Serait-il possible de pouvoir emprunter un servomoteur HS-322 ainsi qu'un MCC pour pouvoir réaliser le prototype ? | oui, contactez-moi pour que nous prenions rendez-vous. |
| Pouvez vous nous confirmer que l’on puisse se mettre à 3 ou faut il que quelqu'un reste tous seul  ? | s'il y a un seul groupe de 3, c'est bon. |
| Pour les modifications du bras robotique, doit on découper toutes les pièces du bras ou juste les pièces que l’on a besoin de modifier ? | vous aurez besoin de toutes les pièces pour assembler votre robot. |
| Est ce à nous de choisir le matériau et de le ramener au Fablab pour la découpe ? | lisez la rubrique Fournitures ci-dessus. Pour le prototypage vous pourrez travailler avec du carton dense 3mm que vous récupérerez. Une fois que nous serons sûrs du résultat, nous passerons à un autre matériau (contreplaqué ou plastique). Si vous disposez de chutes de ces matériaux, vous pouvez les tester. |
|En identifiant les pièces du robot sur le fichier MeArm.dxf avec inkscape, je ne parviens pas à trouver cette pièce : Dans le montage elle sert à monter le micro-servo pour faire tourner le bras. Faut-il créer cette pièce ? ![MeArmParts](notices_techniques/MeArm/MeArm2_parts.png) | Cette pièce est une carte électronique. Pour le prototypage nous pouvons la remplacer par une pièce d'acrylique de même forme: [ServoPCB](notices_techniques/MeArm/ServoPCB-Edge.Cuts.svg) . Attention toutfois à l'épaisseur. |
|Faut-il aussi modifier le bras pour les dimensions de l’Arduino UNO ? | Ajoutez 3 trous pour l'arduino, en utlisant la bibliothèque de formes mentionnée dans la partie "Design". |
|Avec les micro-servos originaux, les embouts ont une certaine forme: Alors que dans la fiche technique des HS-322HD ils sont ronds : Faut-il prendre en compte la forme des embouts ou ils seront les mêmes que les originaux ? | Je n'ai pas encore de réponse. Je fais l'inventaire des embouts disponibles, et vous réponds ensuite.|


Liens
------

*    [kickstarter](https://www.kickstarter.com/projects/mime/mearm-pi-build-your-own-raspberry-pi-powered-robot)
