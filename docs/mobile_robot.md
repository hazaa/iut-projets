Robot mobile
======

*   Sujet: concevoir et réaliser un petit robot mobile avec deux moteurs uniquement (un moteur à courant continu pour la propulsion et un servomoteur pour la direction). Inspiration: les robot aspirateur roomba entrée de gamme. 
*   Organisation: par binômes.

Plan des mini-TP:

*   première séance: présentation du projet.
*   TP:
   *  prototypage rapide: 
      *  cheap robotics: voir liens ci-dessous.
      *  dessin 2D fablab: [TP fablab](tp-fablab.md)
   *  arduino: [TP Arduino](tp-arduino.md), sauf l'exercice Dialogue Série.
   *  commande d'un MCC avec hacheur série: [TP moteur](tp-motor.md)
  
  
Cahier des charges fonctionnel et technique
------

| Nom fonction | Description fonction | Critère |  Valeurs limites  |
| -------- | -------- | -------- | -------- |
| Commande moteurs | commander 2 moteur DC (ou 1 servo+1moteur DC) dans un seul sens  | |   |
| Conception robot | concevoir et réaliser le robot |  | |
| Commande position robot | déplacer le robot en réponse à une consigne manuelle  |    |  |
| Sécurité | garantir la sécurité des étudiants et du matériel|   |  |
 
CDC technique:

*   micro-contrôleur : arduino uno ou mega.
*   moteurs: DC, parmi ceux disponibles.
*   carte de puissance: parmi celles disponibles (MD22, motor shield, pmod, hacheur série).
  

Liens utiles
------

*  Arduino:
   *  de nombreux exemples de robots  [arduino.cc](https://create.arduino.cc/projecthub?category=motors-robotics&part_id=&sort=trending)
   *  commander un [servo](https://www.arduino.cc/en/Tutorial/Sweep)
*  Exemples de robots cheap:
   *   collection [making cheap robots](http://www.instructables.com/id/For-Cheap-Robots/), [Poor man's robot base](http://www.instructables.com/id/Poor-Mans-Robot-Base/)
   *   [Motors-and-Wheels-for-Cheap-Robots](http://www.instructables.com/id/Motors-and-Wheels-for-Cheap-Robots/)
   *   [Motor-Controllers-for-Cheap-Robots](http://www.instructables.com/id/Motor-Controllers-for-Cheap-Robots/)
*  Exemples de robots mobiles simples: 
   *  [hackaday.io](https://hackaday.io/projects?tag=ROBOT)
   *   robot en carton [boxbot](https://hackaday.io/project/94473-boxbotics)
   *   avec roulette à l'avant: [carduino](https://www.thingiverse.com/thing:366065/#files)
   *   [cing](https://hackaday.io/project/93532-robot-cing-educational-robot-for-everyone), [turtle](http://www.instructables.com/id/Electric-Turtle-Robot/), [versalino](https://www.thingiverse.com/thing:59216/)
*  Fablab [Senartlab](https://www.senartlab.fr/)
*  Composants utiles:
   *  transistor [mosfet](https://oscarliang.com/how-to-use-mosfet-beginner-tutorial/), faire un [pont en H](http://bristolwatch.com/ele/h_bridge.htm)
   *  yellow [motors](http://averagemaker.com/2015/12/yellow-motors.html)

# FAQ

| Question |  | Réponse | 
| -------- | -------- | -------- | 
| Sur le robot: | | |
| | A quoi sert-il ? | il doit pouvoir se déplacer sur une surface plane. On peut imaginer un robot aspirateur de type Roomba|
| | Que doit être sa taille, forme et la vitesse max ou réglables ?| une taille d'environ 10cm est suffisants |
| | Se déplace-t-il seul à l’aide de capteurs pour détecter des obstacles ou avec une télécommande et quoi comme commandes ?| Les moteurs seront commandés par un arduino. Dans un premier temps, le robot se déplacera en répondant à des ordres directs. Pour ceux qui ont réussi, on pourra étudier la possibilité d'une navigation autonome. |
| |Mettre un bouton marche/arrêt ? | Oui |
| |Quelle est son moyen de déplacement (roues/roulettes) et pivote-t-il ?| un seul moteur à l'arrière, fixé à des roues, pour assurer la propulsion. Un seul servo à l'avant qui commande l'orientation d'une roulette servant à fixer la direction. |
| Sur le moteur à courant continu | | |
| | Comment fonctionne le moteur avec une pile ou une batterie ? | Dans un premier temps, avec un fil relié à une alimentation de TP. L'alimentation par batterie est une option à voir uniquement à la fin.|
| | Si c’est une batterie, que doit être son temps de charge et son autonomie ? | |
| | Faut-t-il afficher le niveau de l’autonomie restante ?| |
| |Les moteurs sont-ils liés et si oui, comme le sont-ils ? | il y a un seul moteur à courant continu pour la propulsion. Le servo (voir ci-dessous) est alimenté indépendamment.|
||Au niveau des moteurs qu'elle est la tension à prendre pour chacun d'eux?|cela dépend des moteurs, en général entre 6 et 12V, chaque groupe s'adaptera au matériel qu'il aura à sa disposition|
| |Faut-il une batterie par moteur ou une batterie pour les deux moteurs ? | |
| |A-t-on besoin de quatre quadrants ou juste besoin de deux quadrants ? |deux quadrants, car on ne fera pas de marche arrière. On peut même utiliser uniquement un transistor |
| | Les prises en salle TP sont-elles alimentées en courant alternatif ou continu? Sont-elles sous la même tension ? Si oui, laquelle ?| nous disposerons de quelques alimentation à courant continu, comme dans les TP.|
| | Si les prises sont en alternatif, doit-il y avoir un transformateur sur le robot?| pas de transformateur sur le robot|
| | La puce est-elle intégrée dans la carte Arduino? Sinon, est-elle imposée (L293D)?| quelle puce ? il n'y a pas de pont en H présent d'office sur les cartes arduino. Pour connaître précisément de quoi est composée une carte arduino, allez sur le site arduino.cc et cherchez les documents techniques.|
| |Quel est le bouton de commande? (est-ce une manette, un joystick, un slider, un téléphone?) Laconsole de commande sera-t-elle reliée par un câble (et/ou greffée au robot) ou par wifi?|vous proposerez et testerez les solutions les plus simples possibles (interrupteurs, potentiomètres) . |
| |Y a-t-il d'autres composants à part la carte Arduino (avec potentiellement la puce), les deux moteurs, les câbles et les roues?|c'est à vous de répondre à cette question|
| |Le robot agit-il par commande directe ou va-t-il d'un point A à un point B définis avant de lancer le robot?|il agit par commande directe. La deuxième option s'appelle planification/navigation, et est beaucoup plus complexe.|
| Sur le servomoteur:  | | |
| |  comment les commander ?| à l'aide d'un micro-controlleur arduino. voir le lien sur cette page.  |
| Sur les matériaux: | | |
| |Qu’est ce qui enveloppe les moteurs et doit être solide ? | Le robot sera le plus simple possible. Il sera constitué d'un simple carré ou d'un cercle de matériau récupéré (carton épais, plastique). Les moteurs et l'électronique de commande seront fixés sur cette base de manière simple mais robuste (par exemple avec des colliers de serrage en plastique).  La présence d'une enveloppe de protection est optionnelle.|
| |Ferons-nous l’enveloppe du robot nous-même ? | Vous concevez et réalisez le robot vous-mêmes.|
| |Est-ce que le pont diode est égale au pont diode ? | ?|
||La rotation du servomoteur est-elle prédéfinie (exemple : angle incrémenté de 10 en 10) ou y a-t-il l'intégralité de la plage de 0 à 180°?|la réponse précise se trouve dans les documents techniques des servomoteurs. en première approximation, 0 à 180°, mais vous devez vérifier|
|Sur l’organisation : | | |
| |Combien de cours pratique et théorique avons-nous ? | Nous aurons 6 TP et une soutenance.  |
| |Quel est le coût s’il y en a un ? | le plus faible possible. Vous aurez accès au matériel de l'IUT, mais vous n'aurez pas de budget alloué. |
| |Comment utiliser le temps des vacances ? | Pour la documentation et la conception |
| |Quand est-ce que l’on va à FaBLab, faut-t-il une inscription payante ou non ? | Nous irons ensemble au FabLab lors d'une séance de TP dont la date vous sera précisée. Pour les étudiants de l'IUT, l'accès est libre dans le cadre des projets pédagogiques.|
|Sur les livrables : | | |
| | nous avons pas compris comment remplir le tableau de satisfaction et nous ne comprenons pas l'exemple de celui-ci. | En gros, si vous remplissez les contraintes imposées, et que la fonction est réalisée, c'est +. Sinon c'est -. Si vous validez largement, c'est ++, si vous êtes loin du compte, c'est --. |
| | j'ai vu que vous demandiez un tableau de test de validation détaillé mais pouvez vous m'expliquer en quoi consiste ce tableau exactement. | Pour savoir si la fonction marche, il faut faire 10, 20, 30 tests. Vous notez le résultat de chaque test. A la fin, s'il y a 1 erreur sur 10 tests, vous avez 90% de réussite.|
| | Devons nous vous envoyer tout les éléments inclus dans la catégorie livrable (rapport + code + dessins + présentation) ? | Tous ceux dont vous disposez. |
| | Dans le modèle de rapport sur Gitlab, il est précisé que le code doit être en Annexe, or nous devons également joindre le code au fichier zip (dépendamment de la première question). Dans lequel de ces deux endroits poster le code ? | Vous mettez tout les fichiers de code dans le zip. Dans le rapport, si vous le souhaitez, vous copiez-collez des bouts de code dont vous êtes contents, car vous avez apporté quelque chose d'important. |
| | Le fichier zip mentionne le nom « présentation pdf » , est ce que c’est celle que nous utiliserons pour notre soutenance de jeudi ?| C'est celle que vous avez sous la main le jour d'envoi limite. Ca n'est pas un problème si la présentation le jour de la soutenance est un peu différente.|
| | La partie « Soutenance » dans le doc d’aide au livrable et à la soutenance mentionne le terme de « diapositives ». Devons nous accompagner notre oral d’une présentation power point ou d’un fichier pdf ? | Le pdf est mieux, car plus stable. Vous pouvez venir avec un ppt, mais soyez sûr d'avoir le logiciel sur votre portable. |
| | Dans le rapport, on nous parle de CDC Technique. Où le trouver ?| Sur la page du projet. |

    



    


    


# Bonus

*  alimentation par batterie 
*  commande avec un joystick
*  évitement d'obstacle: 
   *   avec interrupteur "bumper" 
   *   capteur d'ultrason
   *   photocoupleur OPB704 (voir [tp-optical-encoder](tp-optical-encoder.md))
*  drone navigation [nanomap](https://hackaday.com/2018/02/12/nanomap-mits-uncertain-solution-to-autonomous-navigation/ , https://github.com/peteflorence/nanomap_ros)
*  challenge [iron-car](http://www.makery.info/2018/02/13/iron-car-trois-petits-tours-de-piste-avec-un-raspberry-pi-sous-le-capot/ https://github.com/xbraininc/ironcar)
