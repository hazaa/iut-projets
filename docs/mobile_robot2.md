Robot mobile
======

Sujet: 
  
*   Concevoir, réaliser, tester un robot mobile avec deux moteurs à courant continu pilotés par transistors MOSFET et arduino. 
*   Concevoir, réaliser, tester la navigation du robot reposant sur la vision. Le robot doit pouvoir se déplacer pour atteindre une cible visuelle prédéfinie.
*   Organisation: par binômes.

Le schéma de principe des différents sous-systèmes est le suivant:

![schema](fig/robot_mobile_schema_principe.png)


Le calendrier :

| Groupe | Semaine 2 |  3 |  4 |
| -------- | -------- | -------- | -------- |
| A1 | début,fablab(10/01) |  3TP | soutenance |



TP
------

*  Prototypage rapide:  Dessin 2D fablab [TP fablab](tp-fablab.md). Au cours de cette séance au Fablab [Senartlab](https://www.senartlab.fr/) on réalisera une copie de la base du robot (voir ici le [scan](fig/robot_mobile_scan.pdf)), et on ajoutera un étage supplémentaire pour accueillir la Rasperry Pi.
*  Arduino: [TP Arduino](tp-arduino.md)
*  Commande d'un MCC avec hacheur série: [TP moteur](tp-motor.md)
*  [TP Python](tp-python.md) pour gérer à la fois la commande moteur et la partie vision.
*  Navigation visuelle [TP vision](tp-vision-whycon.md)
*  [Livrables et soutenance](docs/livrable_soutenance.md)
*  (si nécessaire: Prise en main de la [Raspberry pi](tp-raspi.md) )
  
Cahier des charges fonctionnel et technique
------

| Nom fonction | Description fonction | Critère |  Valeurs limites  |
| -------- | -------- | -------- | -------- |
| Conception robot | concevoir et réaliser le robot |  | |
| Commande moteurs | commander 2 moteur DC dans un seul sens  | |   |
| Commande position robot manuelle | déplacer le robot en réponse à une consigne manuelle  |    |  |
| Navigation autonome vers une cible | déplacer le robot automatiquement vers une cible visuelle  | erreur en pixels    |  erreur<10% de la taille de l'image |
| Navigation autonome en suivant un chemin | déplacer le robot automatiquement: suivre un chemin constitué de marqueurs visuels  |    |  |
| Sécurité | garantir la sécurité des étudiants et du matériel|   |  |
 
CDC technique: utiliser le matériel décrit ci-dessous.

*    Partie électro-mécanique:
   *   moteur à courant continu.
   *   micro-contrôleur Arduino Uno ou Mega
*    Programmation:
   *   Python 2  
   *   Arduino IDE      
*    Vision:
   *   webcam USB


Matériel
------

*  Chassis [robot](http://www.limpulsion.fr/art/ROB12866V02/Chassis_robot_a_monter_avec_moteur_inclu) x7 
*  Arduino Uno 
*  Shield Arduino [prototypage](http://www.limpulsion.fr/art/DFR0019/DFROBOT__SHIELD_PROTOTYPE_POUR_ARDUINO) x7 
*  Transistor [mosfet](https://oscarliang.com/how-to-use-mosfet-beginner-tutorial/), faire un [pont en H](http://bristolwatch.com/ele/h_bridge.htm)
*  Raspberry

Liens utiles
------

# Livrables

Les livrables du projet sont les suivants:

*   un prototype fonctionnel.
*   soutenance: 10 minutes, avec une partie en anglais de 2 minutes par étudiant. 15 minutes de questions du jury. Présentation de la satisfaction du cahier des charges (++,+,-,--) pour chaque critère.
*   une vidéo vivante (exemple [popfab](https://vimeo.com/45911972)), pour être ajoutée sur le [blog GEII](http://iutsfgeii.blogspot.fr/). ATTENTION: ne pas mettre la vidéo dans le zip, mais utiliser un site d'échange de fichiers lourds, comme [framadrop](https://framadrop.org) (<100M) et ajouter le lien vers cette vidéo dans le zip.
*   un fichier zip contenant le rapport à partir du modèle fourni (avec un rapport des tests de validation, et le GRAFCET/algorithme pour le tri), tous vos programmes (arduino, python, inkscape), un "post" décrivant votre projet.

Lisez IMPERATIVEMENT la [description détaillée des livrables ici](livrable_soutenance.md).


## Test et qualité

Vous devrez tester systématiquement le fonctionnement de chaque sous-ensemble.
La démarche de test fait partie de la note.

| Sous-ensemble|  | Test | 
| -------- | -------- | -------- | 
| commande moteurs arduino+transistor | | commander la rotations de deux moteurs avec l'arduino et le transistor|
| dialogue série arduino/python | | allumer une led depuis python |
| commande moteurs série | | commander les deux moteurs depuis python (pas de vision)|
| vision  | | calculer si la cible est au milieu, à droite, à gauche du champ de vision |
| Navigation autonome ligne droite  | | le robot se dirige vers une cible placée à distance au centre de son champ de vision|
| Navigation autonome sur chemin | |le robot se dirige vers une cible placée à distance en périphérie de son champ de vision (gauche puis droite) |


# FAQ

| Question |  | Réponse | 
| -------- | -------- | -------- | 
| Sur le robot: | | |
| | A quoi sert-il ? | il doit pouvoir se déplacer sur une surface plane. On peut imaginer un robot aspirateur de type Roomba|
| | Se déplace-t-il seul à l’aide de capteurs pour détecter des obstacles ou avec une télécommande et quoi comme commandes ?| Les moteurs seront commandés par un arduino. Dans un premier temps, le robot se déplacera en répondant à des ordres directs. Pour ceux qui ont réussi, on pourra étudier la possibilité d'une navigation autonome. |
| |Mettre un bouton marche/arrêt ? | Oui |
| |Quelle est son moyen de déplacement (roues/roulettes) et pivote-t-il ?| deux moteurs à l'arrière, fixés à des roues. |
| Sur le moteur à courant continu | | |
| | Comment fonctionne le moteur avec une pile ou une batterie ? | Dans un premier temps, avec un fil relié à une alimentation de TP. L'alimentation par batterie est une option à voir uniquement à la fin.|
| | Si c’est une batterie, que doit être son temps de charge et son autonomie ? | |
| | Faut-t-il afficher le niveau de l’autonomie restante ?| |
| |Les moteurs sont-ils liés et si oui, comme le sont-ils ? | chaque moteur a un circuit de commande indépendant.|
||Au niveau des moteurs qu'elle est la tension à prendre pour chacun d'eux?|cela dépend des moteurs, en général entre 6 et 12V, chaque groupe s'adaptera au matériel qu'il aura à sa disposition|
| |Faut-il une batterie par moteur ou une batterie pour les deux moteurs ? | |
| |A-t-on besoin de quatre quadrants ou juste besoin de deux quadrants ? |deux quadrants, car on ne fera pas de marche arrière. On peut même utiliser uniquement un transistor |
| | Les prises en salle TP sont-elles alimentées en courant alternatif ou continu? Sont-elles sous la même tension ? Si oui, laquelle ?| nous disposerons de quelques alimentation à courant continu, comme dans les TP.|
| | Si les prises sont en alternatif, doit-il y avoir un transformateur sur le robot?| pas de transformateur sur le robot|
| | La puce est-elle intégrée dans la carte Arduino? Sinon, est-elle imposée (L293D)?| quelle puce ? il n'y a pas de pont en H présent d'office sur les cartes arduino. Pour connaître précisément de quoi est composée une carte arduino, allez sur le site arduino.cc et cherchez les documents techniques.|
| |Quel est le bouton de commande? (est-ce une manette, un joystick, un slider, un téléphone?) Laconsole de commande sera-t-elle reliée par un câble (et/ou greffée au robot) ou par wifi?|vous proposerez et testerez les solutions les plus simples possibles (interrupteurs, potentiomètres) . |
| |Y a-t-il d'autres composants à part la carte Arduino (avec potentiellement la puce), les deux moteurs, les câbles et les roues?|c'est à vous de répondre à cette question|
| |Le robot agit-il par commande directe ou va-t-il d'un point A à un point B définis avant de lancer le robot?|il agit par commande directe. La deuxième option s'appelle planification/navigation, et est beaucoup plus complexe.|
| Sur les matériaux: | | |
| |Qu’est ce qui enveloppe les moteurs et doit être solide ? | Le robot sera le plus simple possible. Les moteurs et l'électronique de commande seront fixés sur cette base de manière simple mais robuste (par exemple avec des colliers de serrage en plastique).  La présence d'une enveloppe de protection est optionnelle.|
| |Ferons-nous l’enveloppe du robot nous-même ? | Vous concevez et réalisez le robot vous-mêmes.|
|Sur l’organisation : | | |
| |Combien de cours pratique et théorique avons-nous ? | Nous aurons 6 TP et une soutenance.  |
| |Quel est le coût s’il y en a un ? | le plus faible possible. Vous aurez accès au matériel de l'IUT, mais vous n'aurez pas de budget alloué. |
| |Comment utiliser le temps des vacances ? | Pour la documentation et la conception |
| |Quand est-ce que l’on va à FabLab, faut-t-il une inscription payante ou non ? | Nous irons ensemble au FabLab lors d'une séance de TP dont la date vous sera précisée. Pour les étudiants de l'IUT, l'accès est libre dans le cadre des projets pédagogiques.|
|Sur les livrables : | | |
| | nous avons pas compris comment remplir le tableau de satisfaction et nous ne comprenons pas l'exemple de celui-ci. | En gros, si vous remplissez les contraintes imposées, et que la fonction est réalisée, c'est +. Sinon c'est -. Si vous validez largement, c'est ++, si vous êtes loin du compte, c'est --. |
| | j'ai vu que vous demandiez un tableau de test de validation détaillé mais pouvez vous m'expliquer en quoi consiste ce tableau exactement. | Pour savoir si la fonction marche, il faut faire 10, 20, 30 tests. Vous notez le résultat de chaque test. A la fin, s'il y a 1 erreur sur 10 tests, vous avez 90% de réussite.|
| | Devons nous vous envoyer tout les éléments inclus dans la catégorie livrable (rapport + code + dessins + présentation) ? | Tous ceux dont vous disposez. |
| | Dans le modèle de rapport sur Gitlab, il est précisé que le code doit être en Annexe, or nous devons également joindre le code au fichier zip (dépendamment de la première question). Dans lequel de ces deux endroits poster le code ? | Vous mettez tout les fichiers de code dans le zip. Dans le rapport, si vous le souhaitez, vous copiez-collez des bouts de code dont vous êtes contents, car vous avez apporté quelque chose d'important. |
| | Le fichier zip mentionne le nom « présentation pdf » , est ce que c’est celle que nous utiliserons pour notre soutenance de jeudi ?| C'est celle que vous avez sous la main le jour d'envoi limite. Ca n'est pas un problème si la présentation le jour de la soutenance est un peu différente.|
| | La partie « Soutenance » dans le doc d’aide au livrable et à la soutenance mentionne le terme de « diapositives ». Devons nous accompagner notre oral d’une présentation power point ou d’un fichier pdf ? | Le pdf est mieux, car plus stable. Vous pouvez venir avec un ppt, mais soyez sûr d'avoir le logiciel sur votre portable. |
| | Dans le rapport, on nous parle de CDC Technique. Où le trouver ?| Sur la page du projet. |

    


# Bonus

*  line-following
*  alimentation par batterie 
*  commande avec un joystick
*  évitement d'obstacle: 
   *   avec interrupteur "bumper" 
   *   capteur d'ultrason
   *   photocoupleur OPB704 (voir [tp-optical-encoder](tp-optical-encoder.md))
*  drone navigation [nanomap hd18](https://hackaday.com/2018/02/12/nanomap-mits-uncertain-solution-to-autonomous-navigation/) , [github](https://github.com/peteflorence/nanomap_ros)
*  challenge [iron-car](http://www.makery.info/2018/02/13/iron-car-trois-petits-tours-de-piste-avec-un-raspberry-pi-sous-le-capot/) [github](https://github.com/xbraininc/ironcar)


# TODO

*   vision: compiler whycon en statique pour x86; script python regexp; Raspy: compilation, statique.
