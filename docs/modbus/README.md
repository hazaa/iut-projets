Projet Energimètre et tableau de bord
======

Dans ce projet on va câbler, et mettre en oeuvre un energimètre communiquant SDM120M sur une prise secteur 220V sur laquelle on branchera une charge, puis on communiquera avec l'energimètre par protocole Modbus série, et on programmera une interface de visualisation comme ci-desous.

![](fig/dashboard.jpg)



**Schéma global**

![](fig/schema_cablage_ensemble.svg)



L'évaluation du projet tiendra compte des éléments suivants:
* 1 CR par binôme.
* CTP.
* Soutenance.
* Quiz en option.



Sommaire:

 [[_TOC_]]


1 Théorie
======


Le SDM120M utilise une **couche physique RS-485**. Sur celle-ci, il emploie un protocole de communication Modbus "série" (ou RTU) pour échanger avec les autres module.

Ci-dessous on explique les fonctionnement de ces différentes couches.



A) Couche physique RS-485
------

La couche physique définit **comment sont codés les 0 et les 1**.

C'est une norme, destinée à la communication sur "longue distance" (quelques centaines de mètres), dans un environnement bruité. 



Elle emploie 3 fils. Les données sont transmises sur "A" et "B". "C" est une référence de masse.

3 états 0/1 ou "idle" (au repos) sont possibles: 

* Si le potentiel sur la ligne A est négatif par rapport au potentiel de la ligneB, l'état binaire est 1.  (on parle de  "differential signaling").
* L'inverse  (A +, B-) correspond au 0 binaire.
* si les potentiels des lignes A et B sont proches, l'état est "idle".



La figure ci-dessous représente les potentiels de la ligne A (bleu), et de la ligne B (rouge) lors de la transmission du signal correspondant à 0xD3 = 11010011, le bit de moindre poids en premier  ("least significant bit first").



![](fig/RS-485_waveform.svg)

A noter également:

* Les informations peuvent circuler dans les deux sens entre deux dispositifs qui communiquent (on parle de « *half duplex* » sur 2 fils), grâce au mode "idle".  (il existe aussi un mode « *full duplex* », sur quatre fils, mais on ne l'utilisera pas ici).
* la topologie du réseau est de préférence linéaire. 
* Il peut y avoir plusieurs émetteurs (32), plusieurs récepteurs (32).

Pour l'instant on n'a pas parlé du protocole de communication, juste de la manière de coder les 0 et les 1. 



B) Conversion UART/RS-485
-----

Les micro-contrôleur communiquent via un bus [UART](https://en.wikipedia.org/wiki/Universal_asynchronous_receiver-transmitter) pour la communication série, qui utilise 3 fils, mais différemment de ce qu'on a vu ci-dessus:

* la ligne TX ne fait que transmettre.
* la ligne RX ne fait qu'écouter.
* GND est la référence de masse.

<img src="fig/UART.svg" style="zoom:33%;" />



Les 0 et les 1 sont codés par le potentiel sur la ligne TX. Ci dessous, le transmetteur envoie une suite de bits sur la ligne TX. Le message est composé de :  1 bit "start", 8 bits de données utiles, 1 bit "stop". L'octet transmis est 01001011 s'écrit 0x4b en hexadécimal, et correspond au caractère ASCII "K".

![](fig/Rs232_oscilloscope_trace.svg)





**Conclusion**: on voit bien que la manière de coder les 0 et les 1 n'est pas la même avec le RS485 (utlisé par le SDM120M) et le UART (utilisé par le micro-contrôleur).

Il est donc nécessaire d'utiliser un convertisseur pour que le SDM120M et l'arduino puissent communiquer: c'est ce que fait le module MAX485.

![](fig/MAX485.png)



Source: https://en.wikipedia.org/wiki/RS-232

C) Protocole de communication Modbus
-----

Le protocle Modbus existe en deux versions: 

* Modbus série "RTU" (sur un bus RS-485) comme on utilisera dans ce projet.
* Modbus TCP, sur un réseau ethernet, que vous avez **déja vu en ARS3**. 

En Modbus RTU, il y a 1 maître et plusieurs esclaves. 

Le maître fait requete en envoyant une trame de requête (par exemple de lecture, avec la fonction 4).

L'esclave répond en envoyant une trame de réponse



La composition d'une **trame de requête** pour le SDM120M, est indiquée dans le  [manuel](Eastron_SDM120Modbus_protocol_V2.1.pdf) du fabricant (page 1):

| First Byte | Slave Address         | 8-bit value representing the slave being addressed (1 to 247), 0 is reserved for the broadcast address. The Eastron Digital meters do not support the broadcast address. |
| ---------- | --------------------- | ------------------------------------------------------------ |
|            | Function Code         | 8-bit value telling the addressed slave what action is to be performed. (3, 4, 8 or 16 are valid for Eastron Digital meter) |
|            | Start Address (Hi)    | The top (most significant) eight bits of a 16-bit number specifying the start address of the data being requested. |
|            | Start Address (Lo)    | The bottom (least significant) eight bits of a 16-bit number specifying the start address of the data being requested. As registers are used in pairs and start at zero, then this must be an even number. |
|            | Number of Points (Hi) | The top (most significant) eight bits of a 16-bit number specifying the number of registers being requested. |
|            | Number of Points (Lo) | The bottom (least significant) eight bits of a 16-bit number specifying the number of registers being requested. As registers are used in pairs, then this must be an even number. |
|            | Error Check (Lo)      | The bottom (least significant) eight bits of a 16-bit number representing the error check value. |
| Last Byte  | Error Check (Hi)      | The top (most significant) eight bits of a 16-bit number representing the error check value. |


On voit qu'on peut choisir entre différentes **fonctions**, mais dans la pratique on n'utilisera dans ce projet que la fonction 0x04 pour la lecture.


La composition d'une **trame de réponse** (page 2):

| First Byte | Slave Address         | 8-bit value representing the address of slave that is responding. |
| ---------- | --------------------- | ------------------------------------------------------------ |
|  | Function Code|  8-bit value which, when a copy of the function code in the query, indicates that the slave recognised the query and has responded. (See also Exception Response). |
|  | Byte count | 8-bit value indicating the number of data bytes contained within this response |
|  | First Register (Hi)|The top (most significant) eight bits of a 16-bit number representing the first register requested in the query.  |
|  |First Register (Lo) | The bottom (least significant) eight bits ... |
|  | Second Register (Hi) |  The top (most significant) eight bits of a 16-bit number representing the second register requested in the query. |
|  |Second Register (Lo) | The bottom (least significant) eight bits |
|  | Error Check (Lo) |  The bottom (least significant) eight bits of a 16-bit number representing the error check value. |
| Last Byte |Error Check (Hi) | The top (most significant)... |



Les données utilisées par le compteur SDM120 sont au format 32 bits IEEE 754 à virgule flottante. Ainsi, chaque grandeur mesurée par l'instrument est contenue dans **deux registres** de 16 bits, adjacents dans la trame réponse Modbus.

Conversion:

* wikipedia: [float32](https://en.wikipedia.org/wiki/Single-precision_floating-point_format), [IEEE754](https://en.wikipedia.org/wiki/IEEE_754)
* livre "Programmation en Python pour les mathématiques" Casamayou-Boucau, Chauvin, Connan, [scholarvox](univ.scholarvox.com.ezproxy.u-pec.fr/reader/docid/88926811/page/144), 2.4 Réels et ﬂottants : un très rapide survol de la norme IEEE754.
* code didactique IEEE754 Python [github](https://github.com/geusebi/float-ieee754-didactic/)



Les différents registres du SDM120M contiennent les différentes grandeurs mesurées (tension, intensité, puissance, etc...). Leur liste se trouve dans le manuel, page 5.

Plusieurs **exemples** de trames requête/réponse sont données dans le manuel.


https://en.wikipedia.org/wiki/Modbus



2 Assemblage et câblage
======



Matériel:

* 2 modules énergimètre « SDM120-Modbus Single-Phase Energy Meters ».
* 2 module de conversion Max485.
* 2 boîtier de protection de l’ensemble « prise 220V+energimètre ».
* 2 prise femelle 220V à monter sur le couvercle de la boîte.
* 2 câbles 220V avec prise mâle.
* pour chaque groupe TP: Arduino.



A) Câblage du SDM120M sur prise secteur
------

Le schéma ci-dessous est tiré du [manuel](SDM120Modbus_user_manualV2.0.pdf) de l'énergimètre. "L" est le symbole de la phase en anglais ("live").

La phase traverse l'energimètre et va alimenter une charge. 

![](fig/SDM120M_cablage.png)



La prise mâle 220V sera connectée au SDM120M en entrée comme indiqué dans le tableau:

| Prise 220V mâle | SDM120M entrées secteur |
| ------- | ------ |
| phase       | 1 (L="live"=phase)     |
| neutre      | 4 (N=neutre)    |



En sortie le SDM120M sera connecté à la prise 220V femelle comme indiqué dans le tableau:

| SDM120M entrées secteur | Prise 220V femelle |
| ------- | ------ |
|  2 (L="live"=phase)      | phase      |
| 4 (N=neutre)     | neutre     |



Le cablage de la prise femelle doit respecter le positionnement de la phase et du neutre:
![](fig/european_plug.jpg)
source [wikimedia](https://commons.wikimedia.org/wiki/File:European_plug_and_sockets,_UE_standard,_EU_plug_and_socket_wiring_diagram,_schuko,_french_socket,_cee_7-7.jpg)



**Travail demandé:** 

* assembler et câbler. 
* tester : le SDM120 est-il alimenté ? Quelle est la valeur de tension mesurée ? Quel est  l'identifiant du module ? Quelle est la fréquence de communication ?
* tester: si disponible, utiliser le convertisseur USB/RS485, le brancher sur le PC et chercher un logiciel type [scanner modbus](https://www.modbus.org/tech.php). ([qmodbus](http://qmodbus.sourceforge.net/), [qmodmaster](https://sourceforge.net/projects/qmodmaster/files/))



B) Câblage SDM120M / Arduino
-----

Le schema de cablage de l'Arduino est le suivant:

![](fig/schema_cablage_arduino.png)

Code couleur du domino connecté au SDM120:

| borne SDM120M | couleur fil |
| ------------- | ----------- |
| 8             | jaune/vert  |
| 9             |             |
| 10            |             |





Câblage Arduino/Max485:

| Arduino | Max485 |
| ------- | ------ |
| 10      | RO     |
| 11      | DI     |
| 8       | DE/RE  |
| VCC (5V)       | VCC  |
| GND       | GND  |



**Travail demandé:** assembler et câbler. 




3 Communication Modbus série
======


**Materiel nécessaire**: ce qui a été assemblé dans la partie 2.



Une fois l'assemblage et le câblage réalisés, on peut passer à l'étape suivante. Pour lire la valeur des registres du compteur, il faut envoyer des trames Modbus sur l'interface série du micro-contrôleur. 

Le [manuel]( Eastron_SDM120Modbus_protocol_V2.1.pdf ) donne (p.3) un **exemple de trame requête**:

| Field Name | Example(Hex) |
| ---------- | ------------ |
|Slave Address | 01|
|Function| 04|
|Starting Address High |00 |
|Starting Address Low |00|
|Number of Points High| 00|
|Number of Points Low| 02|
|Error Check Low |71|
|Error Check High |CB|



La trame **réponse** à la requête ci-dessus es donnée dans le manuel.



**Question 1:**

1. A quoi sert cette requête ? Quelle fonction est demandée ? Quelle grandeur est concernée ? A quel compteur la requête est envoyée?



**Question 2:** 

1. Sur le PC de la salle de TP, coder sur Arduino IDE l'envoi de la trame Modbus ci-dessus afin de lire la tension mesurée par le compteur. Vous pourrez vous aider du fichier [energymeter_SDM120_simple.ino](energymeter_SDM120_simple.ino), en complétant les informations manquantes (points d'interrogation ?????).  NB: l'arduino renvoie la valeur lue vers la console série, il faut l'ouvrir en cliquant Outiles>Moniteur Série.
2. Vérifier que la requête réponse correspond à la réponse attendue (voir manuel).
3. Pourquoi les octets reçus correspondent à 230.2V ?
4. Puis lisez les 6 premières grandeurs contenues dans le registre. On utilisera maintenant le fichier [energymeter_SDM120.ino](energymeter_SDM120.ino), qui gère automatiquement le calcul des codes correcteurs d'erreur.

A la fin, écrivez le résultat dans votre compte-rendu.



Liens:

* (Autre exemple modbusRTU: [arduino.cc](https://create.arduino.cc/projecthub/123325/monitor-your-energy-bill-via-modbus-mkr-wifi-1010-and-rs485-814e5e?ref=similar&ref_id=127999&offset=0), bit operations)



4 Base de données
=====

A) Arduino+Ethernet vers InfluxDB
-----

**Materiel nécessaire**: un Arduino avec module [Arduino Ethernet](https://store.arduino.cc/products/arduino-ethernet-shield-2).



Installation et configuration de Influxdb:

* Téléchargement: https://portal.influxdata.com/downloads/
  * version: influxDB v2.1.1
  * Platform: Linux Binaries (64bit)

* Execution du programme du serveur: ouvrir un terminal (Ctrl+Alt+T) puis:  ```cd mydir; ./influxdb```
* Connection au serveur de base de données: ouvrir un navigateur puis taper l'url: ```http://localhost:8086/```
* Pour la première utilisation, il faut configurer la base de données:
  * login:geii ; password:geiigeii ; bucket:test; organization:org



Envoyer des mesures depuis un Arduino avec module Ethernet vers le serveur de base de données:

*   Téléchargez le fichier Arduino [ethernet_influxdb.ino](ethernet_influxdb.ino) sur votre poste.
*   Modifiez ce fichier en changeant la configuration pour l'adapter à votre cas:
     *    eth_server: l'adresse ip du serveur de base de données.  (Comment la récupérer sous linux: ouvrir un terminal avec Ctrl+Alt+T, taper ifconfig, chercher une ligne du type ```inet 10.14.72.120```).
     *    eth_default_ip: l'adresse IP du module Ethernet de l'Arduino.  A vous de proposer une adresse qui soit sur le même réseau que le serveur, mais qui n'est pas déjà occupée par les autres PC de la salle.
     *    eth_mac: l'adresse mac (au dos du module Ethernet. Il faut ouvrir délicatement le boîtier et séparer l'Arduino de son module). 
     *    Authorization token: il est donné par le serveur InfluxDB. Cliquer sur "Data" (panneau de gauche), puis "Api Tokens" (barre du haut); puis cliquer sur "geii's Tokens". Une fenêtre s'ouvre, avec votre token. Il faut le copier-coller dans le fichier ethernet_influxdb.ino
*   Compilez et téléversez le programme sur l'Arduino.

NB: ce fichier envoie au serveur HTTP une requête [POST](https://en.wikipedia.org/wiki/POST_(HTTP)) qui respecte l'[API ](https://docs.influxdata.com/influxdb/v2.0/write-data/developer-tools/api/)de InfluxDB. 



Pour vérifier que cela fonctionne:

* sur la console série de l'Arduino IDE: cliquer sur Outils>Moniteur série. Le message reçu par l'interface série (via le port USB) est il correct ? Quelle est la réponse du serveur ? ("Reply from InfluxDB"). Le serveur a-t-il renvoyé une erreur ?
* sur InfluxDB: cliquer sur "Explore" (barre de gauche). Dans la boîte "FROM" (en bas à gauche) choisir votre bucket ("test"), puis dans la boîte Filter (en bas à gauche) cliquer sur "airSensors".  Enfin cliquer sur le bouton bleu "Submit" à droite de l'écran. Voyez-vous une courbe s'afficher ?


**Question 1:  chaque binôme sur son poste avec son Arduino**

1. Faire fonctionner ce qui est demandé ci-dessus.
2. Idem avec plusieurs valeurs fictives.

A la fin **appelez l'enseignant pour qu'il vérifie** et écrivez le résultat dans votre compte-rendu.




B) Arduino+Ethernet+energimètre SDM120M vers InfluxDB
-----

**Materiel nécessaire**: ce qui a été assemblé dans la partie 2, ainsi que l'Arduino et son module Ethernet.


Dans cette partie on assemble les différentes parties déjà vuez ci-dessus. On a maintenant besoin de brancher **en même temps** sur l'Arduino le module RS485 et le SDM120M. On va d'abord vérifier que c'est possible.


**Question 1:**

1. Dans la documentation du module [Arduino Ethernet](https://store.arduino.cc/products/arduino-ethernet-shield-2), trouver quels sont les pins utilisés par le module.
2. Quels pins de l'ensemble Arduino+module Ethernet sont disponibles pour brancher le module RS485 ?  Connecter le module RS485. Modifiez le code de la partie 3 (communication modbus) pour tenir compte de ce nouveau branchement. Vérifier que le code marche toujours. 
3. Vérifier que le code Influxdb (partie 4 Question 1) marche toujours.
4. Fusionner le code modbus avec le code InfluxDB, afin que les mesures reçues de la part de l'energimètre soient envoyées sur la base de données InfluxDB. 
4. Le message envoyé à InfluxDB doit permettre de distinguer les binômes. Mettez le nom des membres du binôme en tête du message envoyé. Dans leprogramme ethernet_influxdb.ino, remplacer ```String str = "airSensors,sensor_id=TLM0201 temperature=";``` par ``` String str = "Nom1_Nom2,sensor_id=1 tension=";``` où sensor_id permet prendra la valeur 1 ou 2 selon l'energimètre avec lequel vous travaillez.
5. Vérifier que les mesures arrivent dans InfluxDB. Les visualiser (barre de gauche, "Explore)". Vous pouvez cliquer sur "view raw data" (au milieu à côté du bouton "CSV") pour voir le tableau de données. Vérifier que l'heure exacte de réception correspond bien.


Copier la configuration d'InfluxDB (si on doit changer de machine):

* copier l'integralité du répertoire ```.influxdbv2```


Liens supplémentaires:

* Solutions de rechange pour communiquer avec InfluxDB
    *    avec la librairie InfluxDB-Client: ESP32 [github](https://github.com/tobiasschuerg/InfluxDB-Client-for-Arduino) (NB: pas testé avec Arduino+module wifi).
    *    Avec un script python qui lit les messages envoyés par l'arduino et les écrit sur la base de données: https://github.com/ppetr/arduino-influxdb. Problème; peut-on l'installer sur le PC de la salle TP ?
    *    avec curl (cf [API](https://docs.influxdata.com/influxdb/v2.0/write-data/developer-tools/api/)), mais celui-ci n'est pas installé en salle TP.
    *    solution peu élégante mais simple: stocker les messages envoyés par l'arduino dans un fichier au format influxdb "line protocol "[doc](https://docs.influxdata.com/influxdb/v2.1/reference/syntax/line-protocol/#elements-of-line-protocol)
    *    avec [influxdb 1](http://embedonix.com/articles/embedded-projects/setting-up-influxdb-and-grafana-for-storing-and-viewing-arduino-measured-sensor-values).




5 Visualisation sous InfluxDB
=====

**Question 1:**

1. Créer un dashboard (barre de gauche "Boards", cliquer sur "cell" pour ajouter un graphique) avec: 
    *  sur la première ligne: l'historique de tension, le courant, la puissance, le facteur de puissance, la puissance réactive (pour cela on choisira le graphique de type "graph"), la fréquence. Donnez un titre à chaque graphique.
    *  sur la deuxième ligne: les valeurs instantanées de ces grandeurs (graphique de type "Single stat" ou "Gauge").
2. Créer des alarmes en cas de dépassement. Dans la barre "Alerts" à gauche:
    *  créer un "check" (au dessus d'un certain niveau, l'état est "warn" ou "crit"). 
    *  créer un "notification endpoint" avec la destination "HTTP" et la méthode "POST".
    *  créer une "notification rule" et pointez-la vers le "notification endpoint" que vous venez de créer.
    *  tester: dans le menu "notification rule" cliquer sur l'icone bleue (en forme d'engrenage) et cliquer sur "History". Cliquer sur "statuses" en haut à gauche et faites "Refresh" après avoir effacé le contenu de la barre. Idem mais en cliquant sur "Notifications" en haut à gauche.
    *  tester: avec un serveur HTTP en Python sur une autre machine (NB: il faut un serveur qui écoute sur le port 443 et répond aux requêtes POST (cela ne marchera pas avec [http.server](https://docs.python.org/3/library/http.server.html?highlight=http) en lançant un serveur: ```python -m http.server 8000```). Voit-on les notifications arriver ?

A la fin, écrivez le résultat dans votre compte-rendu.




**Question 2 :**

1. Vérifez que le dashboard est accessible depuis un autre poste.
2. Branchez plusieurs types d'appareils électriques  (avec ou sans puissance réactive). Notez les caractéristiques théoriques (tension, puissance) sur l'étiquette. Les valeurs observées et théoriques sont-elles cohérentes ?  Comparez les mesures des différents appareils 2 à 2. Les valeurs mesurées sont-elles cohérentes avec le type d'appareil (moteur, PC, écran,...). Analyser et expliquer 
3. Créer une alarme sur la fréquence du secteur ($f \in [50-\epsilon;50+\epsilon]$).
4. Peut-on agréger plusieurs mesures de consommation ? Utiliser le langage de programmation Flux inclus dans InfluxDB comme indiqué dans la documentation: [mathematic-operation](https://docs.influxdata.com/influxdb/v2.1/query-data/flux/mathematic-operations/), [percentage](https://docs.influxdata.com/influxdb/v2.1/query-data/flux/calculate-percentages/).

A la fin, écrivez le résultat dans votre compte-rendu.




6 CTP, compte-rendu, soutenance
=====

A ce stade du projet, vous allez préparer la fin du projet: CTP, compte-rendu final, et soutenance du 24 janvier à 13h30.

CTP:
* individuel.
* date: lors de la dernière séance, le 18 janvier à 13h30.
* chaque étudiant aura 10 min pour préparer et 10 min pour montrer le bon fonctionnement. 



Compte-rendu:
* en binôme.
* date: vendredi 21 janvier 23:59
* où: eprel
* format: odt ou pdf, **pas en doc ou docx**  (sinon: pénalité).
* longueur: 15 pages max.
* contenu: répondre aux questions posées dans le sujet. Vous pouvez aussi vous inspirer du fichier RAPPORT_ANON.pdf, voir dans les [fichiers](https://gitlab.com/hazaa/iut-projets/-/tree/master/docs/modbus) du projet.
* nom du fichier : 2022_ER3_BINOME1_BINOME2.zip
* contenu du zip:
   *   votre rapport 
   *   vos fichiers: programmes informatiques (Arduino,...).



Soutenance: 

* **individuelle**.
* date: 24 janvier 13h30.
* planning: vous sera envoyé par mail.
* 10 min chacun, plus questions.
* 2 min en anglais.
* format: présentation powerpoint converti en format **pdf** uniquement. 5 à 7 diapositives **numerotées**. Pas d'animations.




7 Bonus
=====



A) Test du module avec convertisseur USB/RS485
-----

**Materiel**: un convertisseur USB/RS485, un energimètre SDM120M.

* Connectez le convertisseur USB/RS485 sur votre PC. Des drivers sont accessibles sur la clef.

* Connectez le convertisseur USB/RS485 à l'energimètre.

* Travail à faire: lire les registres contenant les valeurs mesurées.

  


B) Création d'une passerelle Modbus RTU/Modbus IP
-----

* Objectif: créer une passerelle Modbus Série - Modbus IP, afin que l'energimètre soit accessible par l'interface Ethernet. (plusieurs maîtres pourront alors envoyer des requêtes à l'appareil).
* Example: avec un Arduino [github](https://github.com/budulinek/arduino-modbus-rtu-tcp-gateway) ou avec un mini-ordinateur linux (par exemple raspberry)  [github](https://github.com/3cky/mbusd).



C) Visualisation avec Grafana
------

* example: lora-node: node-red + influxdb + grafana [arduino.cc](https://create.arduino.cc/projecthub/naresh-krish/visualizing-lora-node-data-with-node-red-and-grafana-8960d3)
* [Grafana](https://en.wikipedia.org/wiki/Grafana): with influxdb [doc](https://grafana.com/docs/grafana/latest/datasources/influxdb/). Téléchargement: édition "OSS"; puis choisir "standalone linux binaries". Ouvrir un navigateur et saisir l'url ```http://localhost:3000/```


Autres solutions de visualisation:

*  jupyterdash: https://medium.com/analytics-vidhya/live-streaming-visualization-with-jupyterdash-dded3b9c93c5
*  jupyter+influxdb: https://www.influxdata.com/blog/streaming-time-series-with-jupyter-and-influxdb/
*  jupyter+matplotlib: https://www.youtube.com/watch?v=vsaH2vFJrnc
*  dash+streaming+SQL: [dash](https://dash.gallery/Portal/?search=[Streaming])
*  [node-RED](https://nodered.org) : [dashboard](https://github.com/node-red/node-red-dashboard), examples: [arduino](https://create.arduino.cc/projecthub/gokulpsamy/interfacing-arduino-with-node-red-f07882)



D) Autres bonus
------

* plusieurs modules sur 1 ligne RS485. Cela nécessite de reconfigurer les adresses des modules. Cf manuel "2.3 Write Holding Registers".
* fablab: fabriquer boîtier.
* debug: 
   * max485
   * RS485 wireshark [fieldbus](https://gitlab.com/wireshark/wireshark/-/wikis/FieldbusProtocolFamily)



Liens
=====

* http://www.projetsgeii.iutmulhouse.uha.fr/mallette-qualite-de-lenergie/
* https://www.industrialshields.com/
* https://www.framboise314.fr/raspberry-pi-et-arduino-dans-le-monde-industriel/

* SDM120 1 phase, modbus, power meter:
    * https://github.com/reaper7/SDM_Energy_Meter (https://github.com/reaper7/SDM_Energy_Meter)
    * https://www.youtube.com/watch?v=yBtqKSWDn1Q
    * https://www.youtube.com/watch?v=2N5RLPIme0A (espagnol)
    * https://fr.aliexpress.com/item/32503313371.html?   

* serial modbus:
  * to usb: 
  * arduino: https://www.arduino.cc/en/ArduinoModbus/ArduinoModbus
    https://create.arduino.cc/projecthub/hwhardsoft/how-to-use-modbus-with-arduino-6f434b
    https://github.com/MichaelJonker/Arduino_HardwareSerial_RS485
  * arduino as modbus master: https://circuitdigest.com/microcontroller-projects/rs-485-modbus-serial-communication-with-arduino-as-master
     lib modbus master: [doc](https://github.com/4-20ma/ModbusMaster), [example](https://4-20ma.io/ModbusMaster/examples_2_r_s485__half_duplex_2_r_s485__half_duplex_8ino-example.html)
     needs: 5V MAX485 TTL to RS485 which is based on Maxim MAX485 IC

* arduino PLC:

   *  www.industruino.com
   *  https://www.industrialshields.com
   *  [arduibox](https://www.hwhardsoft.de/english/projects/arduibox/),  with RS485 shield [instructable](https://www.instructables.com/Arduino-RS485-Din-Rail-Mount/)
   *  arduino/raspi DIN rail bracket [adafruit](https://www.adafruit.com/product/4557)

* cours réseau de terrain (RS485, modbus)

   * https://fr.wikipedia.org/wiki/IEEE_754
   * RS485 [wikipedia](https://en.wikipedia.org/wiki/RS-485)
   * https://silanus.fr/sin/formationISN/Reseau/Niveau1/ISN-ReseauN1.pdf
   * (techniques de l'ingénieur [www](https://ezproxy.u-pec.fr/login?url=http://www.techniques-ingenieur.fr/))
   * (ressource "connect" de la BU [url](https://ezproxy.u-pec.fr/login?url=https://connect.ed-diamond.com/user/login))
   * rs232 vs TTL: [seedstudio](https://www.seeedstudio.com/blog/2019/12/11/rs232-vs-ttl-beginner-guide-to-serial-communication/)

Fournitures
=====

* tableau-electrique-nu 6 modules [leroymerlin](https://www.leroymerlin.fr/produits/electricite-domotique/tableau-electrique-et-disjoncteur/tableau-electrique/tableau-electrique-nu/tableau-electrique-nu-lexman-1-rangee-6-modules-69839651.html)

  
   * prise 220V femelle sur rail DIN [RS](https://fr.rs-online.com/web/p/boitiers-rail-din/2559661/)
   * debug: USB/TTL converter: https://ftdichip.com/wp-content/uploads/2021/02/DS_TTL-232R_CABLES.pdf, https://datasheet.octopart.com/TTL-232R-DLP-datasheet-83642.pdf



TODO
=====

* casing: arduino + Max485 + rail Din
* ajouter décodage float32
* vérifier trame donnée en exemple dans le manuel. Pourquoi le SDM ne répond pas ? Quelle est la valeur codée en réponse ?
* ajouter analyse détaillée puissance active/réactive/facteur de puissance
* db: requeter la base en python ?
* (ajouter harmoniques et facteur de distorsion ?)
* IP: centraliser gestion de IP, passerelle salle TP/réseau des automates.
* améliorer orga ctp
