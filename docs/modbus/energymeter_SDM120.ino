
#include <SoftwareSerial.h>                                   //import SoftwareSerial library
     
//https://www.arduino.cc/en/Reference/SoftwareSerialBegin
SoftwareSerial swSerSDM( ???, ???? );                              //config SoftwareSerial. Arduino pins connected to pins R0 and DI on MAX485
//config SDM

uint16_t reg= ??????;  // register we want to read
uint8_t node= ??????;    // id of SDM unit
int dere_pin = 8;      //Arduino pins connected to pins DE and RE on MAX485. 

#define SDM_B_01                           0x01                      //  BYTE 1 -> slave address (default value 1 read from node 1)
#define SDM_B_02                           0x04                      //  BYTE 2 -> function code (default value 0x04 read from 3X input registers)
#define SDM_B_05                           0x00                      //  BYTE 5
#define SDM_B_06                           0x02                      //  BYTE 6
                                                                    
#define FRAMESIZE                          9                         //  size of out/in array
#define SDM_REPLY_BYTE_COUNT               0x04                      //  number of bytes with data


void flush() {
  unsigned long flushtime = millis() ;
  while (swSerSDM.available() || flushtime >= millis()) {
    if (swSerSDM.available())                             //read serial if any old data is available
      swSerSDM.read();
    delay(1);
  }
}


uint16_t calculateCRC(uint8_t *array, uint8_t len) {
  uint16_t _crc, _flag;
  _crc = 0xFFFF;
  for (uint8_t i = 0; i < len; i++) {
    _crc ^= (uint16_t)array[i];
    for (uint8_t j = 8; j; j--) {
      _flag = _crc & 0x0001;
      _crc >>= 1;
      if (_flag)
        _crc ^= 0xA001;
    }
  }
  return _crc;
}


void dereSet(bool _state) {

    digitalWrite(dere_pin, _state);   //receive from SDM -> DE Disable, /RE Enable (for control MAX485)
}

void setup() {

  pinMode(dere_pin, OUTPUT);    
  Serial.begin(115200);  // hardware serial: from Arduino to PC (serial monitor)     
  swSerSDM.begin(2400);  // software serial on pins 10/11: from Arduino to MAX485 transceiver.
}

void loop() {


  uint8_t sdmarr[FRAMESIZE] = { ?? , ?? , ??, ??, ??, ??, ??, ??, ??};
  
  float res = NAN;
    uint16_t temp;
  /*
   * Extracts the high-order (leftmost) byte of a word 
  https://www.arduino.cc/reference/en/language/functions/bits-and-bytes/highbyte/
  Extracts the low-order (rightmost) byte of a variable
  https://www.arduino.cc/reference/en/language/functions/bits-and-bytes/lowbyte/
  */
  sdmarr[2] = highByte(reg);
  sdmarr[3] = lowByte(reg);
  temp = calculateCRC(sdmarr, FRAMESIZE - 3); 
  sdmarr[6] = lowByte(temp);
  sdmarr[7] = highByte(temp);

  //swSerSDM.listen();  
  //flush();                                     //read serial if any old data is available

  dereSet(HIGH);                                //transmit to SDM  -> DE Enable, /RE Disable (for control MAX485)

  delay(2);                                    

  swSerSDM.write(sdmarr, FRAMESIZE - 1);        //send 8 bytes

  swSerSDM.flush();                            //clear out tx buffer

  dereSet(LOW);                                //receive from SDM -> DE Disable, /RE Enable (for control MAX485)

     if (swSerSDM.available() >= FRAMESIZE) {

      for(int n=0; n<FRAMESIZE; n++) {
        sdmarr[n] = swSerSDM.read();
      }

        if (sdmarr[0] == node && sdmarr[1] == SDM_B_02 && sdmarr[2] == SDM_REPLY_BYTE_COUNT) {

        if ((calculateCRC(sdmarr, FRAMESIZE - 2)) == ((sdmarr[8] << 8) | sdmarr[7])) {  //calculate crc from first 7 bytes and compare with received crc (bytes 7 & 8)
          ((uint8_t*)&res)[3]= sdmarr[3];
          ((uint8_t*)&res)[2]= sdmarr[4];
          ((uint8_t*)&res)[1]= sdmarr[5];
          ((uint8_t*)&res)[0]= sdmarr[6];
        } 
        }
    }

  Serial.print("Voltage:   ");
  Serial.print(res);                            //display voltage
  Serial.println("V");
    
    delay(1000); 
}
