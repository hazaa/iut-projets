
#include <SoftwareSerial.h>                                   //import SoftwareSerial library
     
//https://www.arduino.cc/en/Reference/SoftwareSerialBegin
SoftwareSerial swSerSDM( ???, ???? );                              //config SoftwareSerial. Arduino pins connected to pins R0 and DI on MAX485

//config SDM
uint16_t reg= ??????;  // register we want to read
uint8_t node= ??????;    // id of SDM unit
int dere_pin = 8;      //Arduino pins connected to pins DE and RE on MAX485. 

#define SDM_B_01                           0x01                      //  BYTE 1 -> slave address (default value 1 read from node 1)
#define SDM_B_02                           0x04                      //  BYTE 2 -> function code (default value 0x04 read from 3X input registers)
#define SDM_B_05                           0x00                      //  BYTE 5
#define SDM_B_06                           0x02                      //  BYTE 6
                                                                    
#define FRAMESIZE                          9                         //  size of out/in array
#define SDM_REPLY_BYTE_COUNT               0x04                      //  number of bytes with data



void dereSet(bool _state) {

    digitalWrite(dere_pin, _state);   //receive from SDM -> DE Disable, /RE Enable (for control MAX485)
}

void setup() {

  pinMode(dere_pin, OUTPUT);    
  Serial.begin(115200);  // hardware serial: from Arduino to PC (serial monitor)     
  swSerSDM.begin(2400);  // software serial on pins 10/11: from Arduino to MAX485 transceiver.
}

void loop() {


  uint8_t sdmarr[FRAMESIZE] = { ?? , ?? , ??, ??, ??, ??, ??, ??, ??};
  
  float res = NAN;
    uint16_t temp;
  /*
   * Extracts the high-order (leftmost) byte of a word 
  https://www.arduino.cc/reference/en/language/functions/bits-and-bytes/highbyte/
  Extracts the low-order (rightmost) byte of a variable
  https://www.arduino.cc/reference/en/language/functions/bits-and-bytes/lowbyte/
  */
  sdmarr[2] = highByte(reg);
  sdmarr[3] = lowByte(reg);
  temp = calculateCRC(sdmarr, FRAMESIZE - 3); 
  sdmarr[6] = lowByte(temp);
  sdmarr[7] = highByte(temp);

  //swSerSDM.listen();  
  //flush();                                     //read serial if any old data is available

  dereSet(HIGH);                                //transmit to SDM  -> DE Enable, /RE Disable (for control MAX485)

  delay(2);                                    

  swSerSDM.write(sdmarr, FRAMESIZE - 1);        //send 8 bytes

  swSerSDM.flush();                            //clear out tx buffer

  dereSet(LOW);                                //receive from SDM -> DE Disable, /RE Enable (for control MAX485)

     if (swSerSDM.available() >= FRAMESIZE) {

      for(int n=0; n<FRAMESIZE; n++) {
        sdmarr[n] = swSerSDM.read();            // the answer to our request is here
      }

    }

  Serial.print("Message:   ");
  Serial.print( sdmarr[0]);   
    
    delay(1000); 
}
