/*
Code adapted from http://embedonix.com/articles/embedded-projects/setting-up-influxdb-and-grafana-for-storing-and-viewing-arduino-measured-sensor-values authored by Saeid Yazdani
Adaptation by Aurélien Hazan, Université Paris-Est Créteil
*/

#include <Ethernet.h>
 
/** Ethernet client instance */
EthernetClient client;
 
/** Default local IP address, in case of DHCP IP assignement fails */
const IPAddress eth_default_ip( 192,  168,  2,  250);
 
/** MAC Address of the ethernet shield */ 
byte eth_mac[] = { 0xA8, 0x61, 0x0A, 0xAE, 0x85, 0x38 };
 
/** Server address wich runs InfluxDB */
const byte eth_server[] = {192, 168, 2, 249};
 
/** InfluxDB HTTP port */
const int eth_port = 8086;
 
int i;

/**
 * Starts the ethernet shield as client
 */
bool eth_start(){
   Ethernet.begin(eth_mac, eth_default_ip);
   delay(2000); //delay to allow connection to be done
 
   //do a fast test if we can connect to server
   int conState = client.connect(eth_server, eth_port);
 
   if(conState > 0) {
    Serial.println("Connected to InfluxDB server");
    client.stop();
    return true;
   }
 
  //print the error number and return false
  Serial.print("Could not connect to InfluxDB Server, Error #");
  Serial.println(conState);
  return false;
}
 
/**
 * Send  HTTP data to InfluxDB
 */
void eth_send_data(float x) {

  // ----------------------
  //connect to HTTP server   
  int conState = client.connect(eth_server, eth_port);

  //check if connection to HTTP server is established
  if(conState <= 0) { 
    Serial.print("Could not connect to InfluxDB Server, Error #");
    Serial.println(conState);
    return;
  }

  // ----------------------
  // prepare message header
  String header = "POST /api/v2/write?org=org&bucket=test&precision=ns HTTP/1.1\n"
                    "Authorization: Token jgO1NIy2SxAXhYR-sOCZv7Rb8ZrbHl7ctfALkQTPKmcyHhhIPcMLdPmK3TsOnQ51Kp2UZ1LEZkMh4T-PpjRJZg==\n" 
                    "Host: arduino\n" 
                    "Content-Type: text/plain; charset=utf-8\n"
                    "Accept: application/json\n"
                    "Content-Length:";  
                  
  // prepare measurements we want to add in message
  String str = "airSensors,sensor_id=TLM0201 temperature=";
  // Concatenate numeric value we want to send
  // cd doc: https://www.arduino.cc/reference/en/language/variables/data-types/string/functions/concat/
  str.concat(x);
  int dataSize = str.length();

  // ----------------------
  //Send HTTP header and buffer
  client.print(header);
  client.println(dataSize);
  client.println(); // don't remove that (for some reason ?)/
  client.println(str);
  client.println();

  // ----------------------
  //Send to serial for monitoring/debugging
  //This can be removed.
  Serial.print(header);                  
  Serial.println(dataSize);
  Serial.println(); // don't remove that (for some reason ?)/
  Serial.println(str);
  Serial.println();

  //------------------------
  // check HTTP server answer
  delay(50); //wait for server to process data
  
  //Now we read what server has replied and then we close the connection
  Serial.println("Reply from InfluxDB");
  while(client.available()) { //receive char
    Serial.print((char)client.read());
  }  
  Serial.println(); //empty line
 
  client.stop();  
}
 
/**
 * Setups the peripherals
 */
void setup() {
  
  //set up Serial interface for debugging purposes
  Serial.begin(115200);
  delay(1000);

  //set up ethernet interface
  eth_start(); 

  //set up a counter
  i=0;
}
 
 
 
/**
 * Main loop
 */
void loop() {
 
  //send to HTTP server (InfluxDB)
  eth_send_data(i);

  // incrementing i, to simulate some measurement
  i++;  
  if(i==10){ i=0; }
  
  //some small delay!
  delay(10000);
  
}
