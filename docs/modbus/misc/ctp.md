#  CTP 



En utilisant le sujet de TP et les squelettes de programmes donnés avec le sujet:

* Q1: cabler et programmer la communication Modbus entre le SDM120M et un Arduino, avec une grandeur mesurée (la puissance).  La sortie se fait avec la console série.
* Q2: cabler et programmer la communication entre un Arduino+module Ethernet vers le PC de bureau. (envoyer la valeur d'un compteur qui sera remis à zéro au bout de 20 itérations).
* Q3: cabler et programmer la communication Arduino+module Ethernet+SDM120 vers la base influxdb.



Interdit: amener un autre programme que ceux présents sur le gitlab, quel que soit le support (usb,...).