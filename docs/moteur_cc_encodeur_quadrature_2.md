Pilotage de moteur à courant continu avec encodeur à quadrature v2
======

Pré-requis: TP arduino et TP Python.




Nous souhaitons:

*  commander un moteur à courant continu branché à un arduino depuis un PC avec un script Python.
*  mesurer la vitesse de rotation d'un MCC sur l'arduino.
*  récupérer sur le PC sous Python la valeur de la vitesse de rotation.


Pour cela nous utiliserons:
*    un moteur à courant continu [IG220053](notices_techniques/moteur_DC_ig220053x00085r_ds.pdf) de tension d'alimentation 6V et de courant max 200mA
*    un Transistor mosfet "logic level" STP55NF06L
*    un encodeur à quadrature composé de deux détecteurs à effet Hall, et d'un aimant.
*    nous programmerons la partie C sur arduino uno.
*    la partie Python sera codée sur les PC de bureau. Elle sera copiée sur la raspberry dans un second temps.




Puis lisez entièrement les documents techniques avant de passer à la suite.

>   **Question**: cabler le transistor sur l'arduino comme dans [TP hacheur série](tp-motor.md) et le commander.

>   **Question**: cabler les capteurs à effet hall sur les pins 2 et 3. Demander à l'enseignant avant d'alimenter.


Attention: 

*    l'alimentation du moteur doit etre séparée du reste. Utiliser une alimentation CC que vous brancherez.


## Compte-tour simple
Le moteur n'est pas alimenté dans cette partie.
Vous avez cablé les capteurs magnétiques sur les pins 2 et 3 de l'arduino. Nous allons lire l'état de ces pins.

>   **Question**: utilisez le programme [position_control_encoder_pmodhb5_simple.ino](../code/position_control_encoder_pmodhb5_simple/position_control_encoder_pmodhb5_simple.ino) pour lire sur le console série les informations qui viennent de l'arduino. Que se passe-t-il quand vous faites tourner à la main l'aimant situé sur l'axe du moteur ?

Lisez [vhdl], surtout la partie "codeurs incrémentaux" pour comprendre ce qui se passe.
Comptez le nombre d'incréments par tour de l'arbre moteur. Comme le réducteur a un rapport 1/53 (voir la doc du moteur), on obtient 318 incréments par tour en sortie du réducteur.


## Compte-tour simple avec interruptions
Le moteur n'est pas alimenté dans cette partie.

Nous allons utiliser le mécanisme des [interruptions](https://fr.wikipedia.org/wiki/Interruption_%28informatique%29) dans cette partie.

>   **Question**: utilisez le programme [position_control_encoder_pmodhb5_interrupt.ino](../code/position_control_encoder_pmodhb5_simple/position_control_encoder_pmodhb5_interrupt.ino), modifiez-le pour voir apparaître sur la console série l'état actuel du compteur, par exemple toutes les 100ms 

>   **Question**: Comptez le nombre d'incréments par tour de l'arbre moteur. Retrouve-t-on le même chiffre que ci-dessus ?


Attention: ne rien mettre de superflu dans les interruptions. Par exemple, ne pas y mettre de Serial.print()


## Compte-tour double 
Le moteur n'est pas alimenté dans cette partie.

Répeter ce que vous avez fait ci-dessus, mais cette fois en utilisant deux interruptions, une pour chaque encodeur, comme dans cet [exemple](http://playground.arduino.cc/Main/RotaryEncoders#Example3)

## Commande du moteur à courant continu depuis le PC de bureau

>   **Question**: utiliser la partie dialogue série dans [TP Arduino](tp-arduino.ino) pour commander le moteur à la vitesse souhaitée.


## BONUS: intensité

A l'aide d'une résistance de valeur connue, et des ports analogiques, calculer le courant traversant le moteur et l'envoyer côté Python.

