NILM: non-intrusive load monitoring
======

*  mooc "Socle en Electricité" arts et métiers https://www.fun-mooc.fr/courses/course-v1:ensam+022005+session03/about
*  hi-freq mains sampling: [hackaday.io](https://hackaday.io/project/79848-grid-2-audio)
*  zac pajol [tpline.eu](http://www.tpline.eu/fr/cours/livres/ZAC%20Pajol/Bulletin_01_2019.php)

* def elec: puissance: http://www.epsic.ch/cours/electrotechnique/theorie/puialt/340.html, https://fr.wikipedia.org/wiki/Puissance_r%C3%A9active
* smart meter online: hd18 grid-2-audio
   * NILM,NIALM (non intrusive load monitoring): http://nilmworkshop.org/2016/proceedings/Poster_ID17.pdf, https://www.researchgate.net/publication/261082558_Smart_meter_systems_measurements_for_the_verification_of_the_detection_classification_algorithms,
   * https://www.ijcaonline.org/archives/volume180/number6/zemene-2017-ijca-916052.pdf, https://rd.springer.com/article/10.1007%2Fs10044-015-0487-x
   * openenergymonitor.org
   * hardware: Yomo researchgate, Yay https://www.researchgate.net/publication/282539462_YoMo_the_Arduino-based_smart_metering_board https://www.researchgate.net/publication/318664197_Yay_-_an_open-hardware_energy_measurement_system_for_feedback_and_appliance_detection_based_on_the_arduino_platform https://www.researchgate.net/publication/324459175_Design_and_implementation_of_a_low-cost_arduino-based_high-frequency_AC_waveform_meter_board_for_the_raspberry_pi
   * quintanas https://www.researchgate.net/publication/324459175_Design_and_implementation_of_a_low-cost_arduino-based_high-frequency_AC_waveform_meter_board_for_the_raspberry_pi
   * Makonin: http://makonin.com/, researchgate piNILM, https://github.com/smakonin/APMR
   * HI-FREQ: low cost hardware [[medal|https://www.researchgate.net/publication/317051078_MEDAL_A_Cost-Effective_High-Frequency_Energy_Data_Acquisition_System_for_Electrical_Appliances]] usb sound card + pince ampermétrique ; clustering [[RG|https://www.researchgate.net/publication/324609389_Appliance_classification_across_multiple_high_frequency_energy_datasets]]
* NILM [wikipedia](https://en.wikipedia.org/wiki/Nonintrusive_load_monitoring), [[nilmtk|  https://github.com/nilmtk/nilmtk/tree/master/docs/manual/user_guide]], https://scipy.in/2017/downloads/ankitmahato/attachment/ABSTRACT/Smart_Meter_Data_Analytics_using_Orange_ankitmahato..pdf, https://github.com/ConvergenceDA/visdom-web,https://github.com/Logimethods/smart-meter 
        
      
