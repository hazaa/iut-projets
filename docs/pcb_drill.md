Fraiseuse (PCB drill)
------


*   la machine de l'IUT impose les contraintes suivantes pour la fabrication:
    *    simple face bottom.
    *    pad pleins, 3mm diamètre mini
    *    packaging des diodes : comme les résistances, horizontal, pas vertical
    *    largeur pistes : 40th
    *    écart entre deux pistes : ?

*   ATTENTION: pour des raisons de vitesse de production, le diamètre du trou sera unique, et fixé au minimum.
    *    cela va poser problème pour le connecteur ATX. Il faudra repasser les trous à la perceuse avec le forêt de 1.6mm
    *    le même problème se posera avec les autres trous de diamètre supérieur à 1mm (par exemple les sorties 3.3, 5 et 12V). On prendra un forêt encore plus large.

*   Pour des raisons de fiabilité, il faut éviter certaines géométries pour les pistes:

![dessin_pistes](fig/pistes_ok.png)


*   Avant d'aller voir l'enseignant qui opère la fraiseuse munissez-vous de:
    *    les dimensions exactes de votre carte, en mm
    *    tous les fichiers .gb* produits par le logiciel de EAD (par exemple [fritzing](fritzing.md) ).
 
 
 ## FAQ
 

| Question | Réponse | 
| -------- | -------- | 
| Comment ajouter des trous dans ma carte ? | Pour des grands trous, choisissez le composant "banana" dans fritzing. Pour des petits trous, choisissez le composant "via", dont vous pouvez régler le diamètre.|
| | |
