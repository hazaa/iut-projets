Seed Watch
======

Ce sujet s'inspire d'un stage effectué en 2018 par un étudiant du DUT GEII.

Sujet:

*   Concevoir, réaliser, tester un robot qui se positionne à intervalle de temps régulier au dessus d'un ensemble de graines de plantes en cours de germination disposés sur une grille. 
*   Organisation: par quadrinomes.


Le schéma de principe des différents sous-systèmes est le suivant:

![seed_watch](fig/seed.png)



Le calendrier :

| Groupe | Semaine 2 |  3 |  4 |
| -------- | -------- | -------- | -------- |
| B1 | début,fablab(10/01) |  3TP | soutenance |


TP
------

*   Première séance: présentation du projet.
*   Prototypage rapide: dessin 2D fablab [TP fablab](tp-fablab.md). Au cours de cette séance au Fablab [Senartlab](https://www.senartlab.fr/) on réalisera un [réceptacle pour graines](fig/seed_plaque.svg), et deux boitiers pour accueillir l'électronique: [côté Shapeoko](fig/DB25.svg), [côté Arduino](fig/boitier_Arduino_Mega_DB25.svg). Ces designs sont modifiables en fonction de votre besoin.
*   Arduino: [TP Arduino](tp-arduino.md)
*   Commande d'un moteur pas-à-pas: [TP moteur](tp-motor-stepper.md)
*   [TP Python](tp-python.md) pour gérer à la fois la commande moteur et la partie vision.
*   Asservissement visuel:  [TP vision simple](tp-vision-openloop.md). BONUS: [TP vision whycon](tp-vision-whycon.md)
*   [Livrables et soutenance](docs/livrable_soutenance.md)
  

Cahier des charges fonctionnel et technique
------

| Nom fonction | Description fonction | Critère |  Valeurs limites  |
| -------- | -------- | -------- | -------- |
| Conception robot | concevoir et réaliser le robot |  | |
| Commande moteurs | commander 2 moteur stepper | |   |
| Commande position robot manuelle| déplacer le robot en réponse à une consigne manuelle  |    |  |
| Mode autonome | se déplacer automatiquement au dessus de chacune des cases de la matrice de graines, et prendre une photo. |  | |
| Sécurité | garantir la sécurité des étudiants et du matériel|   |  |
 
CDC technique: utiliser le matériel décrit ci-dessous.


Matériel
------

*    Arduino + carte de commande [RAMPS1.4](http://reprap.org/wiki/RAMPS1.4) pour arduino Mega. (la carte RAMPS sert de support à 3 contrôleur [Pololu](http://reprap.org/wiki/Pololu_stepper_driver_board) de moteurs pas-à-pas).
   *   Plans Arduino Mega [svg](docs/fig/Arduino_Mega.svg), [dxf](https://store.arduino.cc/arduino-mega-2560-rev3)
*    boitier arduino+ramps [svg](docs/fig/boitier_Arduino_Mega_DB25.svg)
*    Shapeoko 2 [wiki](http://shapeoko.github.io/Docs/index.html)
*    Connectique:
   *   [DB25 breakout](https://www.aliexpress.com/item/DB25-D-SUB-Female-25Pin-Plug-Breakout-PCB-Board-2-Row-Terminals-Connectors/32801804396.html)
   *   molex 2510 4 pin [alibaba](https://www.alibaba.com/showroom/2510-4-pin-connector_2.html)


# Livrables

Les livrables du projet sont les suivants:

*   un prototype fonctionnel.
*   soutenance: 10 minutes, avec une partie en anglais de 2 minutes par étudiant. 15 minutes de questions du jury. Présentation de la satisfaction du cahier des charges (++,+,-,--) pour chaque critère.
*   une vidéo vivante (exemple [popfab](https://vimeo.com/45911972)), pour être ajoutée sur le [blog GEII](http://iutsfgeii.blogspot.fr/). ATTENTION: ne pas mettre la vidéo dans le zip, mais utiliser un site d'échange de fichiers lourds, comme [framadrop](https://framadrop.org) (<100M) et ajouter le lien vers cette vidéo dans le zip.
*   un fichier zip contenant le rapport à partir du modèle fourni (avec un rapport des tests de validation, et le GRAFCET/algorithme pour le tri), tous vos programmes (arduino, python, inkscape), un "post" décrivant votre projet.

Lisez IMPERATIVEMENT la [description détaillée des livrables ici](livrable_soutenance.md).


## Test et qualité

Vous devrez tester systématiquement le fonctionnement de chaque sous-ensemble.
La démarche de test fait partie de la note.


| Sous-ensemble|  | Test | 
| -------- | -------- | -------- | 
| commande moteurs arduino+RAMPS | | commander la rotations de deux moteurs avec l'arduino et la carte RAMPS|
| dialogue série arduino/python | | allumer une led depuis python |
| commande moteurs série | | commander les deux moteurs depuis python|
| vision  | | capture image |
| Commande robot et capture image manuelle via interface utilisateur  | | |
| Commande robot et capture image automatique  | | |



# Commandes linux de base

*   ls
*   mkdir DIR
*   cp F1 DIR/
*   mv F1 F2
*   ps -aux
*   kill -9 PID

# FAQ

| Question |  | Réponse | 
| -------- | -------- | -------- | 
| Sur le robot: | | |

# Bonus

*   photo individuelle de chacune des plantes.

# Crédits

*    images [http://openclipart.org](http://openclipart.org), contributeurs: ?

