Apprendre le C 
=====

Ressource pour apprendre le C seul
-------

* en anglais, en ligne:

    *   https://www.codecademy.com/learn/learn-c
    *   https://www.educative.io/courses/learn-c-from-scratch
    *   https://www.codingame.com


* en installant un compilateur sur votre machine:


* en empruntant ou en achetant un arduino:

    *   [store.arduino.cc](https://store.arduino.cc/collections/boards/products/arduino-uno-rev3)

* avec un livre en français:
   
    * Arduino - Apprivoisez l'électronique et le codage pour donner vie à vos projets (2e édition).  Disponible en ligne [BU UPEC](https://athena.u-pec.fr/permalink/33BUCRET_INST/g3ov5m/alma997959704104611)



A voir aussi
------

* apprendre le Python [tp-python](tp-python.md)
