

## TP mesure de consommation électrique des batiments

* consommation par salle ? Phase 1,2,3
* quelle est la part des différents postes à l'intérieur de l'IUT ?



## Analyse données 



https://www.dataiku.com/learn/samples/energy-consumption/

https://gallery.dataiku.com/projects/DKU_ENERGY_CONSUMPTION/flow/

https://gallery.dataiku.com/project-list/

apache tool ? air ? (flows?)



Academic program:

https://pages.dataiku.com/dataiku-academic-program


Hackathon:

https://blog.dataiku.com/using-ai-to-solve-real-world-problems-meet-the-winners-of-the-smart-india-hackaton




### BTS

* facture: Etude de la consommation électrique du lycée en vue de la réduction de la facture  [eduscol](https://eduscol.education.fr/sti/ressources_pedagogiques/etude-de-la-consommation-electrique-dun-lycee-en-vue-de-reduire-la-facture)

* monitoring: Gestion de la consommation électrique d'un etablissement scolaire [eduscol](https://eduscol.education.fr/sti/ressources_pedagogiques/gestion-de-la-consommation-electrique-dun-etablissement-scolaire)

### pymodbus

* wago + python
  * google: "pymodbus" "wago"
  * pymodbus https://pymodbus.readthedocs.io  exemple [scraper](https://pymodbus.readthedocs.io/en/dev/source/example/modbus_scraper.html)
  * Pi4IoT [youtube](https://www.youtube.com/watch?v=4s56FcxEOWA) [github](https://github.com/Pi4IoT/SPS/blob/master/WAGO/Python/wago_1_support.py)
  * wago+python https://github.com/M-o-a-T/asyncwago/
  * www.edom-plc.pl,
    * dashboard javascript + wago https://www.youtube.com/watch?v=PpuZB9EAr7k https://www.edom-plc.pl/index.php/en/more-about-plc/steering/192-panel-administracyjny-wszystko-w-jednym
    *  https://www.edom-plc.pl/index.php/en/more-about-plc/functions/187-jeszcze-jeden-sposob-komunikacji-z-plc
    * modbus function / wago addresses https://www.youtube.com/watch?v=PpuZB9EAr7k

### GTB Sénart : rapports

* Gestion Technique du Batiment. supervision topkapi

* Q: 1er étage ? (salles tp electrotech)

* Laurent 23/10: 
    * le lien PLC/topkapi a été fait par des LPsari. (Q: Véronique)
    * mesure instantanées l'instant t sur supervision: courant et puissance instantanée par phase par étage, ouverture des vannes. 
    * db: enregistrements elec stockés depuis 2017.
    * vannes: débit/ouverture: seule l'ouverture
    * réseau: les automates et la supervision sont sur le même réseau. On peut brancher un autre sur le switch.
    * audit énergétique: aucun à sa connaissance.
    * control layer: 5 PLC, one per floor. “WAGO-I/O-SYSTEM” PLC: a family of ETHERNET
    * agent layer: power measurement
    * TopKapi server supervision software (scada software) https://www.topkapi-scada.com/fr
  
* PhD Roozbeh: 
  
  * [Sadeghian Broujeny et al. 17] "A multi-layer system for smart-buildings' functional and  energy-efficiency awareness: Implementation on a real five-floors  building"  [ieee](https://ieeexplore.ieee.org/abstract/document/8256529)
  * [Sadeghian Broujeny et al. 20] "Data-driven living spaces' heating dynamics modeling in smart buildings using machine learning-based identification" [mdpi](https://www.mdpi.com/1424-8220/20/4/1071)
  * WAGO Kontakttechnik GmbH and Co. KG, “WAGO-I/O-SYSTEM 750 Manueal”, 2016. [wago](http://www.wago.ca/download.esm?file=\download\00368374_0.pdf&name=m07500880_xxxxxxxx_0_en.pdf) https://github.com/WAGO
  * topkapi server
* Building Controls Virtual Test Bed https://simulationresearch.lbl.gov/bcvtb
  
* Rapport Boeuf: 3eme et 4eme étages. gestion lumère température

  * WAGO 750-880.
  * IP: 192.168.0.204    (p.14)
  * pas d'adresse de registre

* Rapport Boeuf/Lebouc: GTB 4 eme étage. dali

  * fichier csv config.

* Soutenance Agenor:

  * diapo 15: **dashboard courant/tension/Papparente/Energie**
  * 2.2.2 Acquisition des automates et tableau d'échange (diapo 22). IP, code station, **zones mémoires**
  * **tableau adressage capteurs** diapo 29 fichier???? 
    * "c:\Users\conception_ci\Desktop\CSV 23-07-2014\3e étage rc7_materielle_23_7_2014.csv"

* Rapport Agenor: 

  * objectifs (p.8)
    * "base de données MySQL et le faire communiquer avec Topkapi." 
    * "Calculer la consommation d’énergie du bâtiment à partir du serveur de base de données." 
    * "tableau d’échange sur Topkapi entre les logiciels Codesys et Topkapi." 
  * config codesys automate: carte SD avec fichier csv
  * 2.3.3 Tableau d'échange de Topkapi avec Codesys. "CT401 possède l’**adresse 1101 sur Codesys**, 1101 + 12288 = 13389 pour Topkapi" p.13
  * 2.3.6 Serveur MySQL  p.18
    * connexion topkapi/mysql: qui a fini le boulot ?  **pas documenté**

* Rapport Hernandez:

  * Module de mesure de puissance à trois phases (750-493)  (p.18)
  * IP: 192.168.0.204    (p.27)
  * modbus UDP/TCP: port 502
  * caméra thermiq p.43
  * recommandations dépertitions thermiq p.44

* Jean-Eric Lo

  * Q: comment sont calculées les **énergies**, puissance apparente, réactive ??

  * /stage_Jean-Eric/save2.sql 

    ```
    SELECT * FROM conso2_et2 where nom = "VOLTAGE1_ET2" INTO OUTFILE 'C:/Users/gtb/Desktop/STAGE Jean-eric/csv/csv/VOLTAGE1_ET2.csv'  fields terminated by ',' lines terminated by '\n';
    SELECT * FROM conso2_et2 where nom = "VOLTAGE2_ET2" INTO OUTFILE 'C:/Users/gtb/Desktop/STAGE Jean-eric/csv/csv/VOLTAGE2_ET2.csv'  fields terminated by ',' lines terminated by '\n';
    SELECT * FROM conso2_et2 where nom = "VOLTAGE3_ET2" INTO OUTFILE 'C:/Users/gtb/Desktop/STAGE Jean-eric/csv/csv/VOLTAGE3_ET2.csv'  fields terminated by ',' lines terminated by '\n';
    
    SELECT * FROM conso2_et2 where nom = "CURRENT1_ET2" INTO OUTFILE 'C:/Users/gtb/Desktop/STAGE Jean-eric/csv/csv/CURRENT1_ET2.csv'  fields terminated by ',' lines terminated by '\n';
    SELECT * FROM conso2_et2 where nom = "CURRENT2_ET2" INTO OUTFILE 'C:/Users/gtb/Desktop/STAGE Jean-eric/csv/csv/CURRENT2_ET2.csv'  fields terminated by ',' lines terminated by '\n';
    SELECT * FROM conso2_et2 where nom = "CURRENT3_ET2" INTO OUTFILE 'C:/Users/gtb/Desktop/STAGE Jean-eric/csv/csv/CURRENT3_ET2.csv'  fields terminated by ',' lines terminated by '\n';
    
    SELECT * FROM conso2_et2 where nom = "ENERGIE_CONSO_P1_ET2" INTO OUTFILE 'C:/Users/gtb/Desktop/STAGE Jean-eric/csv/csv/ENERGIE_CONSO_P1_ET2.csv'  fields terminated by ',' lines terminated by '\n';
    SELECT * FROM conso2_et2 where nom = "ENERGIE_CONSO_P2_ET2" INTO OUTFILE 'C:/Users/gtb/Desktop/STAGE Jean-eric/csv/csv/ENERGIE_CONSO_P2_ET2.csv'  fields terminated by ',' lines terminated by '\n';
    SELECT * FROM conso2_et2 where nom = "ENERGIE_CONSO_P3_ET2" INTO OUTFILE 'C:/Users/gtb/Desktop/STAGE Jean-eric/csv/csv/ENERGIE_CONSO_P3_ET2.csv'  fields terminated by ',' lines terminated by '\n';
    
    SELECT * FROM conso2_et2 where nom = "PUISS_APPARENTE_P1_ET2" INTO OUTFILE 'C:/Users/gtb/Desktop/STAGE Jean-eric/csv/csv/pap1_et2.csv'  fields terminated by ',' lines terminated by '\n';
    
    SELECT * FROM conso2_et4 where nom = "PUISS_EFFECTIVE_P3_ET4" INTO OUTFILE 'C:/Users/gtb/Desktop/STAGE Jean-eric/csv/csv/pep3_et4.csv'  fields terminated by ',' lines terminated by '\n';
    
    ```

  * /Stage_Jean-Eric/csv/csv: 

    * PUISS_EFFECTIVE_P3_ET2:  pep3_et2.csv
    
  * config topkapi/SQL: Topkapi vision>Mode configuration>Edition des bases de données
  
    * GTB_SENART/GTB BATIMENT A/PHOTOS et DESSINS/PROGRAMME/TOPKAPI/Connexions OK/Connexion SGBD Topkapi.png

### GTB Sénart: Codesys

* Déclaration de variable: GTB_SENART/GTB BATIMENT A/PHOTOS et DESSINS/PROGRAMME/CODESYS/Déclaration de variable partie 4, étage 4.png"
* execution codesys: GTB_SENART/GTB BATIMENT A/CODESYS/0 RDC/GEII_Etage0.pro

### Wago

* Mesure de puissance à 3 phases https://www.wago.com/fr/syst%C3%A8mes-i-o/mesure-de-puissance-%C3%A0-3-phases/p/750-494

* Application Note for the 750-494 3-Phase Power Measurement Module https://www.wago.com/fr/d/7071



### linky+Enedis

* https://enedisgateway.tech/



### linky+télérelevé TIC

*  https://pad.lamyne.org/s/BkvMdU_If#
* lora: compatible IC2Q
  * kne watteco lora: https://www.nke-watteco.fr/produit/capteur-tic-harvesting-lorawan/ 280€  https://www.tecsol-one.com/fr/boitier/nouveau-tecsol-tic-lora
  * ixel.fr loratic (mais nécessite alim)
* wifi: 
  * esmart D2L  http://eesmart.fr/modulesd2l/erl-wifi-compteur-linky/ [amazon](https://www.amazon.fr/Seifel-D2L-WiFi/) >50€ mais propriétaire. cloudless possible [jeedom](https://community.jeedom.com/t/eesmart-d2l/3470)
  * pitinfo https://www.tindie.com/stores/hallard/  http://hallard.me/teleinfo-emoncms/
*  mysensors
   *  gateway: [serial](https://www.mysensors.org/build/serial_gateway) or esp8266 see [fumee bleur](https://www.openhardware.io/view/717/Usb-serial-Mysensors-Gateway),  
   *  sensor: https://fumeebleue.fr/boutique/ 
