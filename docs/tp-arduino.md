TP Arduino
======

**Matériel**: Arduino + cable, une diode, bouton poussoir.

**Logiciel**: Arduino IDE, librairie python pyserial



Une vidéo [youtube](https://www.youtube.com/watch?v=xu_wsGJ9RQ0) tirée du MOOC MinesTelecom.



Initiation
------

A faire en premier: démonstration de la programmation arduino avec l'enseignant.

**Exercice 1:**

1.   Lancer le programme Arduino IDE sur votre PC. Ouvrir le programme Fichier>Exemples>Basics>Blink. Le compiler. Connecter votre Arduino. Choisir le bon type de carte et le bon port. Téléverser le programme que vous venez de compiler sur l'Arduino. La diode test du pin 13 doit s'allumer.
1.   Faire clignoter une LED connectée sur la broche numérique n°10. Elle doit rester allumée pendant 4 s et éteinte pendant 1 s.
1.   Allumer une LED connectée sur la broche numérique n°13 lorsqu’un bouton poussoir, connecté sur la broche numérique n°12 est appuyé.
1.   Ouvrez et compiler le programme Outils>Traceur série. L'instruction Serial.print() envoie une valeur sur le port série de l'arduino, et votre PC peut la récupérer. Cliquer sur Outils>Moniteur série. Choisir la vitesse de communication (bauds) qui correspond. Que se passe-t-il ?

Pour plus de contenus, voir les liens ci-dessous.



Dialogue Serie
------

Le PC effectue une demande à l'Arduino, qui répond. Comment faire ? C'est simple :

*   côté Arduino : on écoute ce qui vient de l'interface série. Dès qu'il reçoit un octet, il prépare une réponse et la renvoie.
*   côté PC : on envoie régulièrement des octet à l'Arduino, et on écoute immédiatement la réponse.



**Exercice 2: **

1.   Sur le gitlab, allez dans le répertoire [code](https://gitlab.com/hazaa/iut-projets/tree/master/code)
2.   côté Arduino : compilez et téléversez SerialNumericParser/SerialNumericParser.ino qui se trouve dans le répertoire [code](https://gitlab.com/hazaa/iut-projets/tree/master/code)
3.   côté PC: dans un terminal, executez le script SerialNumericParser.py



# Resources

*   Le site de référence: [arduino.cc](https://www.arduino.cc/en/Tutorial/Foundations)
*   Apprendre Arduino en autonomie: [MOOC MinesTelecom](https://www.fun-mooc.fr/courses/MinesTelecom/04017S02/session02/about),[adafruit lessons](https://learn.adafruit.com/adafruit-arduino-lesson-1-blink/uploading-blink-to-the-board)
*   Arduino IDE, le logiciel qui permet de programmer l'arduino, est un logiciel libre. Vous pouvez le [télécharger](https://www.arduino.cc/). Si vous ne souhaitez pas l'installer sur votre machine, vous pouvez l'utiliser [en ligne](https://create.arduino.cc/editor)
*   Arduino [sans ses librairies](http://geii.iut-troyes.univ-reims.fr/wikigeii/index.php?title=Cours:TPs_2103)
*   (Ancien TP [scenari](http://eprel.u-pec.fr/eprel//nzltnwj/4513/document/2015_2016/co/activ_appren_arduino_motor.html): exercices: "Bases", option: "servomoteur analogique", "Moteur à courant continu").




FAQ
------

| Question | Réponse |
| -------- | -------- |
| sous python, il y a une erreur avec "import serial": le paquet pyserial n'est pas installé sur votre pc.  | Il faut télécharger [pyserial-2.6](https://pypi.python.org/pypi/pyserial/2.6), le décompacter dans un répertoire, par exemple Bureau/data_etudiant/python. Au début de chaque programme python qui utilise serial, ajouter: import sys; sys.path.append('/home/etudiant/Bureau/data_etudiant/python') |
| | |





# Liens

*   Sujet [upsti](https://www.upsti.fr/documents-pedagogiques/communication-serie-python-arduino)
*   fast uart: cf upsti
*   python+ftdi : hackaday18
*   materiel:  H-bridge/hacheur 4 quadrants [aliexpress](https://fr.aliexpress.com/item/15931-Free-Shipping-New-Dual-H-Bridge-DC-Stepper-Motor-Drive-Controller-Board-Module-L298N-MOTOR/32355666632.html)
