# Bilan carbone DUT GEII

[[_TOC_]]

* Projet S3: UE32 M3203 "Etudes et réalisation d'ensembles pluritechnologiques". (coeff: 3, UE32=coef 11, total S3=30)
* planning: TP: 25/11, 4/12, 11/12, 18/12, 08/01, 15/01; soutenance: 18/01.
* organisation du TP:
  * par binômes.
  * rendre un rapport par binôme sur eprel à la fin de chaque séance.
* pré-requis:
  * TP1: [libreoffice](https://www.libreoffice.org/) (ou excel).




## TP 1  bilan carbone IUT: premiers pas

* **Contexte**: réchauffement climatique accéléré.
  * objectif du [GIEC](https://fr.wikipedia.org/wiki/Groupe_d%27experts_intergouvernemental_sur_l%27%C3%A9volution_du_climat#R%C3%A9sum%C3%A9_%C3%A0_l'intention_des_d%C3%A9cideurs_politiques_(2018)): limiter l'augmentation de $T$ moyenne en dessous de +2°C en 2100.
  * chaque pays à son niveau prend des engagements de réduction des émissions.
  * comment ? d'abord en réalisant des **bilans d'émission**, puis en réalisant des plans d'**action**.
* Que dit la loi française concernant les bilan de gaz à effet de serre (BGES) ?
  * c'est une **obligation légale** [L.229-25](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000039369665?tab_selection=code&searchField=ALL&query=l229-25&page=1&init=true&nomCode=EEFtyw%3D%3D&) du code de l'environnement (cf aussi [Ademe](https://www.bilans-ges.ademe.fr/fr/accueil/contenu/index/page/art75/siGras/1), guide tertiaire non marchand p.10).
  * il est nécessaire de prendre en compte les émission directes mais aussi **indirectes** (par exemple liées à la consommation d'électricité) [Article R.229-47](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000031695480/2016-03-23)
* Dans ce projet S3 on va s'intéresser aux flux de matière et d'énergie qui rentrent et qui sortent de l'IUT sur une année.



Code de l'environnement, partie législative [L.229-25](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006074220/LEGISCTA000022476850/#LEGIARTI000039369665)

> « − Sont tenus d'établir un bilan de leurs émissions de gaz à effet de serre :
>  1° Les personnes morales de droit privé employant plus de cinq cents personnes ;
>  2° Dans les régions et départements d'outre-mer, les personnes morales  de droit privé employant plus de deux cent cinquante personnes exerçant  les activités définies au 1° ;
>  3° L'Etat, les régions, les départements, les métropoles, les  communautés urbaines, les communautés d'agglomération et les communes ou communautés de communes de plus de 50 000 habitants ainsi que les  autres personnes morales de droit public employant plus de deux cent  cinquante personnes.```

Code de l'environnement, partie règlementaire [R.229-47](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000031695480/2016-03-23) 

> Le bilan distingue : 
>
> 1° Les émissions directes, produites par les sources, fixes et mobiles, nécessaires aux activités de la personne morale ; 
>
> 2° Les émissions indirectes associées à la consommation d'électricité, de  chaleur ou de vapeur nécessaire aux activités de la personne morale. 

**QUESTION1** : à l'aide de la Fig.6 et du tableau 2 du [guide ADEME](docs/ADEME_guide_bges_tertiaire-non-marchand-2012.pdf) , passez en revue tous les postes d'émission, et donner à chaque fois un exemple pour l'IUT. 

| Catégorie d’émission                | N°   | Postes d’émissions | Exemple IUT |
| ----------------------------------- | ---- | ------------------ | ----------- |
| Émissions directes de GES (SCOPE 1) |      |                    |             |
| Indirecte (SCOPE 2)                 |      |                    |             |
| Indirectes (SCOPE 3)                |      |                    |             |





### Emissions directes (SCOPE1)

**QUESTION1**: quels sont les postes d'émissions directes à l'IUT ?

**QUESTION2**: quelles sont les factures correspondantes dans le répertoire [docs](/docs) ? Saisir les données mensuelles en kWh dans libreoffice, par mois et par site, créer le graphe correspondant (exemple ci-dessous). Analyser le montant total, les poids relatifs des différents sites, des différentes périodes de l'année. 

| Année |Mois    | Conso. site 1 (kWh) | Conso. site 2 (kWh) | Total (kWh) |
|- |------- | ------------------------- | ------------------------- |- |
| |Janvier | 100                       | 50                        |  |
| |...     | ...                       | ...                       |  |
| |TOTAL   | 1200                      | 600                       |  |

**QUESTION3**: retrouver la surface de l'IUT dans le document "UPEC_Tableau_de_bord_de_caractérisation...". Ajouter les 3 colonnes de consommation par $m^2$ dans le tableau précédent (site 1, site 2, total).

**Comparaison à d'autres batiments** on se propose maintenant de comparer la consommation de **gaz** de l'IUT par $m^2$ à d'autres bâtiments. On aimerait se comparer aux autres IUT de même taille, mais nous n'avons pas accès à ces informations protégées par le *secret statistique*. Mais on peut connaître la consommations d'autres établissements voisins et qui ont une activité similaire. Pour cela on va utiliser des portail "Open Data" (exemple [Open Data GRDF](https://opendata.grdf.fr/ )) et plusieurs des jeux de données proposés qui donnent:

* GRDF: [consommation-annuelle-de-gaz-par-epci-et-code-naf-annee-2018](https://opendata.grdf.fr/explore/dataset/consommation-annuelle-de-gaz-par-epci-et-code-naf-annee-2018)  qui donne la consommation annuelle de gaz de ville au niveau plus large de la Communauté d'Agglomération (CA), en séparant de manière fine le secteur d'activité (code NAF), mais uniquement pour les années 2018-2019.
* GRDF: [consommation-annuelle-gaz-agregee-maille-iris](https://opendata.grdf.fr/explore/dataset/consommation-annuelle-gaz-agregee-maille-iris/) qui donne la consommation annuelle de gaz de ville au niveau local de l'IRIS, c'est-à-dire une zone d'habitation d'environ 2000 habitants, depuis 2011, mais avec peu de détails sur le type d'activité (Residentiel, industrie, tertiaire).
* Agence ORE: [consommation-annuelle-delectricite-et-gaz-par-commune-et-par-code-naf/](https://www.data.gouv.fr/fr/datasets/consommation-annuelle-delectricite-et-gaz-par-commune-et-par-code-naf/). Attention ne pas télécharger: taille >200Mo

**QUESTION4**: avec le jeu de données [consommation-annuelle-de-gaz-par-epci-et-code-naf-annee-2018](https://opendata.grdf.fr/explore/dataset/consommation-annuelle-de-gaz-par-epci-et-code-naf-annee-2018), sélectionnez les données en filtrant par le nom "Sénart" et en cliquant sur le "Nom EPCI" qui contient "Sénart", en en filtrant l'année 2018. Vérifier dans l'onglet "carte" que l'IUT se trouve bien dans cette CA. Quel est le code NAF de l'enseignement supérieur ? (voir le fichier [INSEE_int_courts_naf_rev_2](docs/INSEE_int_courts_naf_rev_2.xls), source site [INSEE](https://www.insee.fr/fr/information/2120875)). Quel est le total de consommation de gaz pour l'enseignement dans la Communauté d'Agglomération ? Quelle est la part de l'IUT en % ? Résumez dans le tableau ci-dessous. 

| GEO\ACTIVITE | tous secteurs | NAF  | Moyenne NAF par site |
| ------------ | ------------- | ---- | -------------------- |
| Département  |               |      |                      |
| CA           |               |      |                      |



**QUESTION5**: avec le jeu de données [consommation-annuelle-gaz-agregee-maille-iris](https://opendata.grdf.fr/explore/dataset/consommation-annuelle-gaz-agregee-maille-iris/), sélectionnez les données de l'IRIS de Lieusaint (code iris=772510106). Vérifier dans l'onglet "carte" que l'IUT se trouve bien dans cet IRIS. Dans l'onglet analyse, cliquer sur empilement "Normal" pour visualiser les données secteur par secteur (résidentiel, tertiaire, industrie,...). Dans l'onglet "Export" téléchargez un fichier csv ou excel ("Seulement les enregistrements sélectionnés"), et reproduisez le graphe précédent avec libreoffice.  Quelle est la tendance de la consommation totale en fonction du temps ? Quelle est la tendance des 3 principaux secteurs ? 

**QUESTION6**: comparer la part de l'IUT dans l'IRIS de Lieusaint par rapport au reste du tertiaire, et par rapport à la moyenne de la consommation tertiaire dans cette IRIS (utiliser le nombre de points de livraison tertiaire "Tertiaire - nbr de PDL". La consommation annuelle de l'IUT est-elle très différente de la consommation moyenne tertiaire ?

| GEO\ACTIVITE | tous secteurs | secteur tertiaire | Moyenne tertiaire par site |
| ------------ | ------------- | ----------------- | -------------------------- |
| Département  |               |                   |                            |
| CA           |               |                   |                            |
| Commune      |               |                   |                            |
| IRIS         |               |                   |                            |



**QUESTION7:** il est utile de pouvoir comparer des consommations par $m^2$. Avec les différents jeux de données ci-dessus, est-on capable de comparer la consommation de gaz par $m^2$ ?

**QUESTION8**: peut-on faire mieux avec les données de l'Agence ORE ? Téléchargez le fichier préparé avec seulement la commune de Lieusaint dans le 77 [conso-elec-gaz-annuelle-par-naf-agregee-commune-Lieusaint-77](data/conso-elec-gaz-annuelle-par-naf-agregee-commune-Lieusaint-77.csv), importez-le sous libreoffice et dites s'il contient des renseignement importants que nous n'avons pas avec les deux bases ci-dessus.

(QUESTION 9: déplacements avec les véhicules de fonction de l'IUT)

### Emissions indirectes SCOPE2

En QUESTION1 du TP1 vous avez rempli le tableau des émissions directes et indirectes. Dans cette partie on va se concentrer sur l'utilisation d'énergie électrique qui rentre dans la catégorie SCOPE2.

**QUESTION1**: quelles sont les factures correspondantes dans le répertoire [docs](/docs) ? Saisir les données mensuelles en kWh dans libreoffice, par mois et par site, créer le graphe correspondant (exemple ci-dessous). Analyser le montant total, les poids relatifs des différents sites, des différentes périodes de l'année. 

| Année |Mois    | Consommation site 1 (kWh) | Consommation site 2 (kWh) |
|- |------- | ------------------------- | ------------------------- |
| |Janvier | 100                       | 50                        |
| |...     | ...                       | ...                       |
| |TOTAL   | 1200                      | 600                       |



**QUESTION2**: retrouver la surface de l'IUT dans le document "UPEC_Tableau_de_bord_de_caractérisation...". Ajouter une ligne "TOTAL par $m^2$" en bas du tableau précédent.

**Comparaison à d'autres batiments**: on se propose maintenant de comparer la consommation de **électricité** de l'IUT à d'autres bâtiments par plusieurs méthodes.  On aimerait se comparer aux autres IUT de même taille, mais nous n'avons pas accès à ces informations protégées par le *secret statistique*. Mais on peut connaître la consommations d'autres établissements voisins et qui ont une activité similaire. 

**QUESTION3** avec le jeu de données [consommation-electrique-par-secteur-dactivite-iris](https://data.enedis.fr/explore/dataset/consommation-electrique-par-secteur-dactivite-iris/), sélectionnez les données de l'IRIS de Lieusaint (code iris=772510106) pour 2018. Vérifier dans l'onglet "carte" que l'IUT se trouve bien dans cet IRIS. Dans l'onglet "Tableau" recherchez les lignes correspondant au code NAF de l'enseignement.  Dans l'onglet "Export" téléchargez un fichier csv, sélectionnez le bon code NAF et l'année, notez le nombre de clients du même code NAF.  Remplissez le tableau ci-dessous des consommations totales en MWh pour résumer. Comment se situe l'IUT ?

| GEO\ACTIVITE | secteur tertiaire | NAF  | Moyenne NAF par site |
| ------------ | ----------------- | ---- | -------------------- |
| Département  |                   |      |                      |
| CA           |                   |      |                      |
| Commune      |                   |      |                      |
| IRIS         |                   |      |                      |



**QUESTION4:** il est utile de pouvoir comparer des consommations par $m^2$. Avec les différents jeux de données ci-dessus, est-on capable de comparer la consommation par $m^2$ ?

**QUESTION5**: le territoire de Lieusaint est-il plutôt producteur net d'électricité ou consommateur net ? Quel est le grand secteur d'activité qui consomme le plus ? Vous pourrez comparer vos résultats avec l'application  [bilan-de-mon-territoire](https://espace-client-collectivites.enedis.fr/web/espace-collectivite/bilan-de-mon-territoire).

**QUESTION6**: sachant qu'en France on estime qu'il faut 2,5kWh d'énergie *primaire* (gaz, charbon,...) pour obtenir 1kWh d'*énergie finale* de type électrique, calculer la consommation annuellle d'énergie de l'IUT en équivalent énergie primaire. (source?)



### Autres emissions indirectes SCOPE3

En QUESTION1 du TP1 vous avez rempli le tableau des émissions directes et indirectes. Dans cette partie on va se concentrer sur l'utilisation d'énergie qui rentre dans la catégorie SCOPE3: consommations indirectes autres que celles du SCOPE2.

#### Transports

Pour évaluer les emissions indirectes liées aux transports, il faut connaître:

* le nombre de trajets quotidiens vers l'IUT.

* multiplier par les émissions de chaque trajet (voir [définitions](#définitions)).

Pour le nombre de trajets nous nous limiterons aux étudiants. Vous utiliserez les emplois du temps sur ADE (voir rubrique [ADE](#ade) ci-dessous). Pour les émissions de chaque trajet, vous réaliserez un sondage auprès des étudiants du DUT GEII pour connaître le pourcentage de trajets en fonction du moyen de transport (transports en commun, véhicule individuel). 



**QUESTION1**: calculez le nombre total de trajets d'étudiants aller/retour généré par l'activité d'enseignement de l'IUT au cours de l'année 2019-2020. Chaque binôme prendra un des départements de l'IUT (GIM,CS,GEA1/2,INFO,GEII,TC,GC,GB). Remplissez le tableau suivant pour votre département et partagez vos résultats avec les autres groupes.

| mois           | nombre de trajets A/R GEII | GIM  | ...  |
| -------------- | -------------------------- | ---- | ---- |
| septembre 2019 |                            |      |      |
| ...            |                            |      |      |
| juillet 2020   |                            |      |      |



**QUESTION2**: réaliser un questionnaire sur office 365, en indiquant le mode de transport, la distance parcourue, et le numéro étudiant (pour éviter les doublons). Faites un test avec les étudiants du groupe TP. L'enseignant se chargera alors de diffuser. Une fois que les réponses seront parvenues, vous calculerez le coût énergétique moyen d'un trajet aller/retour vers l'IUT. Vous pourrez consulter https://ecolab.ademe.fr, qui permet de compter les émissions de gaz à effet de serre.

|             | distance trajet aller | mode transport | consommation (kWh/personne.km) | consommation 1 trajet (kWh/personne) |
| ----------- | --------------------- | -------------- | ------------------------------ | ------------------------------------ |
| étudiant 1  |                       |                |                                |                                      |
| ...         |                       |                |                                |                                      |
| étudiant  n |                       |                |                                |                                      |
|             |                       |                | **MOYENNE : **                 |                                      |



**QUESTION3** en déduire la consommation énergétique annuelle moyenne totale générée indirectement par l'activité d'enseignement de l'IUT du fait des déplacements. Complétez le tableau précédent comme ci-dessous.



|                   | effectif | nb trajets AR par an | conso (kWh) par an |
| ----------------- | -------- | -------------------- | ------------------ |
| GEII              |          |                      |                    |
| GIM               |          |                      |                    |
| GEA Sénart        |          |                      |                    |
| GEA Fontainebleau |          |                      |                    |
| ...               |          |                      |                    |
| TOTAL             |          |                      |                    |





#### Nourriture

Pour évaluer la consommation énergétique de l'IUT liée à la nourriture, on mutliplie le nombre quotidien d'étudiants par le coût énergétique de chaque repas.  L'ADEME et l'INRAE proposent une base de données pour chaque aliment https://ecolab.ademe.fr/agribalyse . La réglementation donne des intervalles de masse pour les repas servis en restauration scolaire [Arrêté 30 sept.2011](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000024614763/2020-12-17/)

**QUESTION4**  en utilisant ces informations,  évaluer la consommation énergétique annuelle liée à la nourriture à l'IUT et remplissez le tableau ci-dessous (sauf les cases X).

|                                                     | entrée | plat | dessert | TOTAL |
| --------------------------------------------------- | ------ | ---- | ------- | ----- |
| consommation énergétique (MJ/kg)                    |        |      |         | X     |
| masse (kg)                                          |        |      |         | X     |
| consommation énergétique totale (MJ)                |        |      |         |       |
| consommation énergétique totale (kWh)               |        |      |         |       |
| nombre annuel Sénart+FB                             | X      | X    | X       |       |
| consommation énergétique totale annuelle S+FB (kWh) | X      | X    | X       |       |



#### Achats

Pour évaluer la consommation énergétique de l'IUT liée aux achats, on va supposer que l'essentiel provient de l'achat de matériel électronique, plus particulièrement de l'informatique. 

**QUESTION5** A l'aide d'ADE, évaluez le nombre d'ordinateurs sur chacun des sites de l'IUT. Cliquer sur Salles>CAMPUS X>Bat.Y>SALLES INFO
Faites l'hypothèse que 10% du parc est renouvelé annuellement, en déduire le nombre annuel d'ordinateurs achetés.

**QUESTION6** à l'aide de la [feuille de calcul](docs/bilan_carbone/docs/shift_Lean-ICT-Materials-REN-2018-updated-in-April-2019.xlsx) du shift project (onglet "build"), estimez le coût énergétique de production d'un ordinateur, en déduire le coût énergétique annuel lié à l'achat d'ordinateurs sur chaque site de l'IUT.

|                                    | Coût énergétique (MJ) | Coût energétique (kWh) |
| ---------------------------------- | --------------------- | ---------------------- |
| 1 ordinateur                       |                       |                        |
| ensemble du parc IUT               |                       |                        |
| 10% du parc renouvelé annuellement |                       |                        |



#### Bâtiments

Pour mener à bien ses missions d'enseignement et de recherche, l'IUT dispose de bâtiments. Le campus de Sénart se développe, et ces dernières années plusieurs bâtiments ont été livrés: nouvelle BU, batiment G, et bientôt un futur bâtiment pour la recherche. (Pour plus de précision, on pourrait inclure le coût énergétique des locaux loués par l'IUT pour héberger les nouveaux départements GB,GC).

La construction de bâtiments neufs nécessite une énergie importante. De même, les matériaux qui composent ces bâtiments (béton, acier, équipements électriques essentiellement) on dû être extraits et transformés, ce qui nécessite de l'énergie.

Ci-dessous une évaluation globale en kep/m² de bâtiment neuf à usage d'enseignement, tirée de la base [Carbone](https://www.bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/index/siGras/1) de l'ADEME, dans la rubrique SCOPE3>Achats de biens>[Batiments et ouvrages d'art](https://www.bilans-ges.ademe.fr/fr/accueil/documentation-gene/index/page/Batiments) . 

En termes de comptabilité, on étale la dépense énergétique sur une période de temps: le coût énergétique total est divisé par n années (par exemple 20) et imputé à chaque année.

|              | kep/m² béton |
| ------------ | ------------ |
| Enseignement | 162          |

**QUESTION7**: en supposant une surface de 1000 m² pour la nouvelle BU et pour le batiment G, une durée d'amortissement de 20 ans, quel est le coût énergétique annuel des nouveaux bâtiments. (pour les unités et les conversions, voir plus bas).

| surface totale (m²) | kWh/m²     | kWh surface totale  | kWh/an sur 20 ans |
| ------------------- | ---- | ------------ | - |
| 2000                |      |              | |



### Analyse et bilan

* **QUESTION1**: remplissez le tableau ci-dessous avec les résultats des question précédentes. Faites un graphique (camembert) au niveau agrégé (SCOPE1,2,3) et au niveau plus fin (poste par poste).
* **QUESTION2**: sur quoi peut-on agir pour réduire la consommation ? Proposer 3 actions en vous appuyant sur les fiches actions dans le guide de l'ADEME. Quel est le gain attendu pour chaque action ? 
* **BONUS1**: pensez-vous que la dématérialisation (cours en distanciel) fasse baisser le coût énergétique ? Liens: [shift](https://theshiftproject.org/article/deployer-la-sobriete-numerique-rapport-shift/), [cnrs](https://lejournal.cnrs.fr/articles/numerique-le-grand-gachis-energetique), [pour la science](https://www.pourlascience.fr/sd/environnement/le-vrai-cout-energetique-du-numerique-20490.php)
* **BONUS2**: étant donné la croissance du nombre d'étudiant.e.s, pensez-vous que ces actions suffiront à faire baisser la consommation totale ?

| SCOPE                    | poste d'émission | Consommation energétique annuelle (MWh) Sénart+FB |
| ------------------------ | ---------------- | ------------------------------------------------- |
| 1  direct                |                  |                                                   |
| 2 indirect (électricité) |                  |                                                   |
| 3 indirect (autres)      | transports       |                                                   |
|                          | nourriture       |                                                   |
|                          | achats           |                                                   |
|                          | bâtiments        |                                                   |
| TOTAL                    |                  |                                                   |

### Soutenances


*    10 minutes par binôme. Tout dépassement sera **fortement pénalisé**. Vous devez répéter au moins 2 fois avant pour vérifier le timing. 
*    2 minutes en anglais par étudiant.
*    Les deux premières pages de votre présentation doivent faire apparaitre clairement le cahier des charges et le résultat obtenu.
*    Vos diapositives de présentation seront numérotées.
*    Munissez vous d'un PC portable.



### A NE PAS TRAITER: DPE

En complément on peut utiliser le Diagnostic de Performance Énergétique (DPE), qui additionne toutes les consommations énergétiques. L'inconvénient est qu'on mélange toutes les consommations (gaz, électricité, bois) ce qui ne permet pas de distinguer SCOPE1 et SCOPE2. L'avantage est notamment qu'on a accès à la surface du batiment et son adresse précise (parfois géolocalisée), et donc à une consommation par $m^2$, qui permet de faire des comparaisons. 

Le DPE doit être réalisé lors de la construction de logements neufs, lors de la vente/location d'un logement, mais également pour une partie des batiments publics (voir site du ministère de l'[écologie](https://www.ecologie.gouv.fr/diagnostic-performance-energetique-dpe). Le DPE contient une étiquette "énergie" qui estime la consommation d’énergie primaire, une étiquette "climat" pour connaître la quantité de gaz à effet de serre émise. 

![](bilan_carbone/docs/Etiquette_energie-DPE.png)

Les DPE réalisés sont centralisés par l'ADEME sur 2 [bases de données publiques](https://data.ademe.fr/datasets/), que nous allons utiliser ci-dessous. Ces bases contiennent ([data.gouv.fr](https://www.data.gouv.fr/fr/posts/la-base-des-diagnostics-de-performance-energetique-dpe/)): 

> **les informations "clé" du DPE** : données  concernant les étiquettes énergie et émissions de gaz à effet de serre  du logement. Ces données correspondent aux indicateurs suivants :
>
> - la consommation énergie en kWhEP/[m².an](http://m².an);
> - le classement consommation d'énergie;
> - l'estimation d'émission de gaz à effet de serre (GES) en Kg eqCO2/[m².an](http://m².an);
> - le classement GES.
>
> **les éléments d'identification et de géolocalisation du logement ou du bâtiment** : adresse postale complète, coordonnées géographiques, type de bâtiment.

**QUESTION**:  complétez le tableau ci-dessus en ajoutant la colonne "DPE énergie" en bas:

| Année |Mois    | Conso. site 1 (kWh) | Conso. site 1 (kWh/$m^2$) | Conso. site 2 (kWh) |Conso. site 2 (kWh/$m^2$)|
| -| ------- | ------------------------- | ------------------------- | ------------------------- | ------------------------- |
| |...     | ...                       | ...                       |
| |TOTAL   | 1200    |    100              | 600       |      110        |
| |DPE énergie   |  |         C            |    | D           |

**QUESTION1**: avec la visualisation cartographique de la base de données DPE de l'ADEME sur les batiments publics, vérifiez si l'IUT a déjà réalisé son DPE. 

**QUESTION2:** pour le commune de Lieusaint, visualisez le tableau de données DPE tiré de la base "bâtiments publics". Enregistrez ce tableau sous forme d'un fichier .csv. Importez ce tableau dans libreoffice. Choisissez pour cela la virgule "," comme caractère séparateur. Vérifiez que les données des colonnes "consommation_energie" et "classe_consommation_energie" sont cohérentes. Est-ce que "consommation_energie_finale"/surface redonne "consommation_energie" ? Ajoutez l'IUT ce tableau comme s'il était une nouvelle ligne.

**QUESTION3**: avec libreoffice, nous allons calculer la fréquence de chacune des étiquettes. On va utiliser la colonne "consommation_energie", et la fonction "frequence" dans libreoffice. Le premier argument de la fonction est la colonne "consommation_energie", le second argument est le tableau des limites des classes de consommation ci-dessous. Video en anglais sur [youtube](https://www.youtube.com/watch?v=suFPnBV_NU4). Une fois que vous avez les fréquences, vous pouvez tracer un diagramme en bâtons. Attention, ne pas confondre un simple diagramme en bâtons avec ce diagramme des fréquences (qui s'appelle un histogramme). 

| classe| limite haute de la classe (kWh/$m^2$)|
| - | -|
|A|50|
|B|90|
|C|...|
|D||
|E||
|F||
|G||

**QUESTION4:** où se trouve l'IUT dans cet histogramme ? 



## TP bonus

**QUESTION:** recommencer le même travail avec la base de données "logements", toujours pour la commune de Lieusaint, y compris l'histogramme

**QUESTION**: évolution temporelle: que pouvez-vous dire de l'évolution dans le temps de la consommation par $m^2$ ?

**BONUS**: sous réserve de disposer des données, refaire le travail précédent sur 10 ans. Comparer avec la population étudiante des 2 sites de l'IUT. Quelle est votre prévision pour le futur ?





## Reférences

### définitions, unités

* énergie primaire "Une énergie qui se trouve dans l’environnement s’appelle une énergie primaire "  (cours Jancovici)
* énergie finale "L’énergie finale, c’est celle qui passe le compteur du consommateur final (...) une énergie qui va vous permettre de faire fonctionner une machine, ça s’appelle de l’énergie finale" (cours Jancovici)
* unités:  1kWh =3,6 MJ
* tonne équivalent pétrole (tep), kep (kilo d'équivalent pétrole): 1 tep ≈ 41,8 GJ ≈ 11.600 kWh (cours Jancovici)
* IRIS: cf [insee](https://www.insee.fr/fr/metadonnees/definition/c1523)



**Consommation énergétique par activité** source:  Jancovici,  cours 1 école des mines p.14, p.34, 2019, [www](https://jancovici.com/publications-et-co/cours-mines-paristech-2019/cours-mines-paris-tech-juin-2019/)

| Activité                                               | Consommation (kWh) |
| ------------------------------------------------------ | ------------------ |
| consommation moyenne 1 français.e, par an, tous usages | 50000              |
| 1km en voiture (en incluant fabrication voiture)       | 1                  |
| combustion 1L de carburant                             | 10                 |



**Emission Gaz à effet de serre par activité** source: ADEME, [base Carbone](https://www.bilans-ges.ademe.fr), login nécessaire.

| Activité                                                     | Emission                       |
| ------------------------------------------------------------ | ------------------------------ |
| Autobus moyen - Agglomération de 100 000 à 250 000 habitants | **0.146** kgCO2e/passager.km   |
| Voiture - Motorisation moyenne - 2018                        | **0.193** kgCO2e/km            |
| RER et transilien - 2019 - Ile de France                     | **4.10E-3** kgCO2e/passager.km |
| Gaz naturel - 2015 - mix moyen - consommation	[www](https://www.bilans-ges.ademe.fr/fr/accueil/documentation-gene/index/page/Gaz) | **0.205** kgCO2e/kWh PCS       |
| Electricité - 2020 - mix moyen - consommation                | **0.0599** kgCO2e/kWh          |





### BGES administrations, universités

* bilan GES: [ademe](https://www.bilans-ges.ademe.fr/)
  * réglementation
  * guide sectoriel tertiaire non marchand:[fiche](https://www.bilans-ges.ademe.fr/docutheque/secto/Fiche_Tertiaire_Non_Marchand.pdf), [guide](https://www.ademe.fr/sites/default/files/assets/documents/realisation-bilan-emissions-gaz-effet-serre-secteur-tertiaire-non-marchand-7642.pdf)
  * 77 univ [UPEM](https://www.bilans-ges.ademe.fr/fr/bilanenligne/detail/index/idElement/1678/back/bilans)
  * simu ademe [ecolab](https://ecolab.ademe.fr/), [github](https://github.com/betagouv/ecolab-data)
* labo1point5
  * Q: part des activités enseignement (e.g. TP electrotech)
* cours énergie/climat:
  * MOOC Energie climat [youtube](https://www.youtube.com/playlist?list=PLhYMUTjGulyJDNVWPunF8IHMnuunfSqNY)
  * jancovici.com
* The Shift Project, [projet](https://theshiftproject.org/article/pour-une-sobriete-numerique-rapport-shift/)  "ICT", calculateur [xls](https://theshiftproject.org/wp-content/uploads/2019/04/Lean-ICT-Materials-REN-2018-updated-in-April-2019.xlsx)
* Eco-conception, ACV [revue technologie](https://eduscol.education.fr/sti/ressources_techniques/lanalyse-du-cycle-de-vie-dun-produit-revue-technologie-n157), ACV+batiment ???
* divers:
  * Lenzen [Evaluating the environmental performance of a university](https://www.sciencedirect.com/science/article/pii/S0959652610001435) 
  * DUT hygSecEnv https://www.univ-lehavre.fr/spip.php?article1719
  * https://buildingos.com/s/cornell/storyboard3763/?chapterId=22033
  * https://www.sciencedirect.com/science/article/pii/S2212827117308910
  * https://www.researchgate.net/publication/233943037_Investigating_the_Carbon_Footprint_of_a_University_-_The_case_of_NTNU
  * https://www.salford.ac.uk/our-facilities/energy-house-labs



* DPE: diagnostic de performance énergétique, obligatoire en cas de vente, de location d'un logement ou d'un immeuble, ainsi que pour certains batiments publics.
  *  [ministère](https://www.ecologie.gouv.fr/diagnostic-performance-energetique-dpe), https://www.service-public.fr/particuliers/vosdroits/F16096
  * réglementaire: [batiments publics](https://www.ecologie.gouv.fr/diagnostic-performance-energetique-dpe#e7)
  * données: base ademe. Description: [blog](https://www.data.gouv.fr/fr/posts/la-base-des-diagnostics-de-performance-energetique-dpe/). Data: [batiments-publics](https://data.ademe.fr/datasets/dpe-batiments-publics)
  * logiciels: http://www.rt-batiment.fr/evaluation-des-logiciels-a50.html [ademe](https://www.ademe.fr/expertises/batiment/passer-a-laction/outils-services/logiciels-calcul-energetique)

### Python

* installation de python:
  * linux/mac: est installé de base sur la plupart des distributions (ubuntu, redhat,...)
  * windows: 
    * python + éditeur: [thonny](https://thonny.org/) (très simple), [pycharm](https://www.jetbrains.com/fr-fr/pycharm/) (plus complexe).
    * [python-xy](http://python-xy.github.io/)
    * [anaconda](https://www.anaconda.com/products/individual)
    * autres [distributions](https://www.python.org/download/alternatives/)
    * [python.org](https://www.python.org/downloads/windows/)
  * cloud:  attention: il faut créer un compte; pas sûr qu'on puisse installer un package:
    * https://cocalc.com/
    * google [colab](https://colab.research.google.com)
    * microsoft [azure](https://azure.microsoft.com/fr-fr/)

* installer des nouveaux packages (nécessaire pour icalendar): alller sur pypi.org, par exemple https://pypi.org/project/icalendar/, on nous indique de taper ```pip install icalendar``` mais cette étape peut dépendre de votre installation, si vous êtes **sous windows**:

    * doc [pip](https://pip.pypa.io/en/latest/user_guide/#user-installs):
        ```
        C:\> py -m pip install --user icalendar
        ```
    
    * thonny  [wiki](https://github.com/thonny/thonny/wiki/InstallingPackages)
    
    * visual c studio ?
    
    * pythonXY ?
    
    * pycharm ?
    
    * pas très propre: décompresser le package dans le répertoire de travail.
    
* auto-formation python:

    * https://parcours.algorea.org/
    * https://python.iutsf.org/
    * https://www.codecademy.com/courses/learn-python-3/   (nécessite un compte linkedin/gmail/github/FB)
    * [fun-mooc ULB](https://www.fun-mooc.fr/courses/course-v1:ulb+44013+session04/info)  (nécessite un compte fun-mooc)
    
* livre: cours de [G.Swinnen](https://inforef.be/swi/python.htm)

### ADE

* pré-requis: installer [python](#python)
* Exporter des Agenda depuis ADE: 
   * pour un étudiant: choisir un étudiant puis Options>Export Agenda
   * pour un groupe: dans l'onglet en bas à gauche choisir Groupe>IUT SF>GEII  puis Options>Export Agenda et choisir la date de début et de fin qui vous convient. On obtient un fichier .ics, qui contient un bloc de texte pour chaque activité prévue dans l'emploi du temps. 
* Analyser les fichier ADE sous python: voir [README.md](docs/bilan_carbone/code/README.md)  



## TODO

* TODO:
  * docs prof:

    * [BTS Appert mesure 13] p.3-4 sur bornier compteur vert fig.18 p.18
    * acceder-aux-donnees-de-mesure [edf](http://www.enedis.fr/acceder-aux-donnees-de-mesure), capfile.com
    * "analyse conso" collectivités [edf](https://www.edf.fr/collectivites/gestion-et-factures/vos-consommations/analyseconso/analyseconso-electricite)

  * docs étu:
    * plans bat A (surface) ??
    * codes NAF [insee](https://www.insee.fr/fr/information/2120875) (enseignement=85)
    * doc enedis [consommation-electrique-par-secteur-dactivite-iris](https://data.enedis.fr/api/datasets/1.0/consommation-electrique-par-secteur-dactivite-iris/attachments/description_du_jeu_de_donnees_consommation_et_thermosensibilite_electriques_annuelles_pdf/)
    * description GTB sénart
    
  * séquence:
    * avant: mail install soft. (python,)
    * séance 1-3: bilan carbone, 
       * prise en main documents
       * prise en main software
       * factures elec IUT, ~700MWh par an pour Sénart. (repeat: Fbleau)
         * comparer à lycée
         * comparer à: tpline valmoutier https://www.tpline.eu/valmoutier
           * pavillon [R.E.V 2013 06 06 - Pavillon A1 - Dossier Restitution enquete-donnees personnelles.pdf (1,15 MB)](https://www.tpline.eu/valmoutier/ressources valmoutier/Association REV/Pavillon A.pdf) 159kWh/m2/an ; 1259 MWh/an pour 74 pavillons.
        * comparaison gaz/elec: conversion MWh.
       * facture eau ??? compteur voitures parking ???
       * générés indirectement:
         * nourriture: combien repas data crous (sondage) ; bilan carbone repas: appli ademe (ou base libre?) 
           * fréquentation
           * menu crous: data.gouv.fr
           * MJ/kg de produit https://ecolab.ademe.fr/agribalyse  https://app.agribalyse.fr/
           * masse repas ??
         * transports: sondage (google doc) pct voiture + emploi du temps étudiants (comparer à capacité max dialog gestion).
         * achats. bilan carbone par euro
    * séances 4-6: focus conso elec: récupérer données brutes.
       * linky: from raspi AH
       * GTB sénart; read csv data from sql; plot; analyze
       * interface émeraude IC2Q IUT Sénart
       * exos: calculer les **énergies**, puissance apparente, réactive ?? [iutenligne baselecpro](http://public.iutenligne.net/electronique/piou_fruitet_fortun/baselecpro/acquisition/) [puissance efficace](public.iutenligne.net/electricite/piou/exercicelecpro/06_ValeurMoyenneEfficacePuissances.pdf)
  
* soutenance:
  
  * plateforme:
    * test wago+pymodbus scaper
    * tp distanciel: cd [enseignement_distanciel.md](/home/aurelien/synchro/boulot/cr/themes/enseignement_distanciel.md),  http://guacamole.apache.org
    

```
pipenv run csvgrep  -c libelle_commune -m Lieusaint  ~/local/git/iut-projets/docs/bilan_carbone/data/conso-elec-gaz-annuelle-par-naf-agregee-commune.csv -d ";" | pipenv run csvgrep -c code_departement -m 77 | pipenv run csvgrep -c code_naf -m 85 |pipenv run csvlook

```

# TODO

* clarifier S/F
* moyenne unitaire en kWh pour chaque poste (e.g. 1 repas)
* correc:
  * fournir format rapport
  * pour chaque tableau: eg. gaz: 2 factures sénart
  * pour chaque département DUT
* supprimer analyse IRIS ?
* missions recherche:
* trajets: 
  * attention nb aller/retour
  * étudiants internationaux (exemple quebec INFO)
* python: plus dur, plus de struct données/prog, comprendre ce qui se passe
* élec: décomposer les usages locaux (pour préconisations)
* ECS ?
* missing data: 
  * achats (info), autres ?
  * batiments: surface nouveaux batiments, date précise de livraison, amortissement de tous les batiments, règlementation amortissement bat ?
* préconisations:
  * demander chiffrage.
  * e.g. virtualisation
