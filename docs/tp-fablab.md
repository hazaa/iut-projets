Prototypage rapide au fablab
======

#    Mini-cours Inkscape

avec l'enseignant [diapo](sujet_decoupe_laser.odt)

Prise en main inkscape avec l'enseignant:

*  nouveau; Fichier>Propriétés du doc>unités par défaut, unité -> mm
*  Affichage	> Grilles, Aimanter (icône)
*  Editer les couleurs: fond (pas de remplissage), contour, épaisseur 0.05mm
*  Outils : Alignement
*  Comment assembler des pièces:
   *  tracé pour assemblage perpendiculaire: finger (à coller après), tslot (avec vis/écrou). Voir l'application [makercase](http://makercase.com)
   *  élastiques: voir bras robot MeArmV2 [instructions d'assemblage](https://hackaday.io/project/16537/instructions), [video](https://youtu.be/az8JMvvetyU)
         


>    **Question**: tracé de découpe simple (rond, carré) avec la bonne couleur pour la découpe.

>    **Question**: carré dont un des côtés est un demi-cercle (avec union).

>    **Question**: créer une boîte avec assemblage finger à l'intérieur de laquelle se trouve un arduino et un servo. Vous pourrez utiliser le fichier avec les [guides de découpe](https://gitlab.com/hazaa/iut-projets/tree/master/docs/notices_techniques/MeArm/library-pattern-cutouts.svg)


Liens: 

*  [inkscape](http://inkscape.org)


#    Mini-cours découpe laser

Liens:

*  un cours sur la découpe laser: http://hazan.xyz/lasercut
*  plugin [inkscape](http://carrefour-numerique.cite-sciences.fr/fablab/wiki/doku.php?id=projets:plugins_inkscape_fablab:tsf_export) pour générer un fichier trotec directement sous inkscape.

>    **Question**:  découpez un objet simple en carton.


# Travail à faire

>    **Question**:  concevez votre propre design. Vous pouvez vous inspirer de ce [projet](https://github.com/GemHunt/Conveyors)

>    **Question**:  réalisez un prototype de la partie mécanique en allant au fablab [SenartLab](http://senartlab.fr) grâce à la découpeuse laser.
>	 Testez votre prototype, s'il y a un problème, itérer les étapes ci-dessus.



FAQ
------

| Question | Réponse | 
| -------- | -------- | 
| | | 
