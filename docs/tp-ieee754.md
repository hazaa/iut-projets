




Les données renvoyées par instruments (e.g. le compteur SDM120, cf 
Tp [energimètres](modbus/README.md)) 
sont au format 32 bits IEEE 754 à virgule flottante. 

Ainsi, chaque grandeur mesurée par l'instrument est contenue dans 
**deux registres** de 16 bits, adjacents dans la trame réponse Modbus.


COmment faire la conversion ???

Liens:

* enseigner le NSI, mooc inria ???? https://mooc-nsi-snt.gitlab.io/ https://www.fun-mooc.fr/courses/course-v1:inria+41018+session01/about
* wikipedia: [float32](https://en.wikipedia.org/wiki/Single-precision_floating-point_format), [IEEE754](https://en.wikipedia.org/wiki/IEEE_754)
* code didactique IEEE754 Python [github](https://github.com/geusebi/float-ieee754-didactic/)


