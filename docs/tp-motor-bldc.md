Moteur brushless
======

bldc: brushless dc 

* https://fr.wikipedia.org/wiki/Moteur_sans_balais, https://www.electromecanique.net/2015/01/principe-de-fonctionnement-dun-moteur.html
* cours [cachan](http://www.geea.org/IMG/pdf/MSAP_ENS_Multon.pdf). Extrait  "Selon  la  forme  de  la  force  électromotrice  ou  d’autres  critères  (économique  notamment)  on  alimentera  la  machine    en  quasi-créneaux  de  courant  ou  de  tension  ou  en  courant  sinusoïdal.  Dans  le  premier  cas  on  parle  plutôt  de  «  machine  à  courant  continu  sans  balai  ou  brushless  DC  motor  »,  dans  lesecond cas de « machine synchrone autopilotée ou brushless synchronous motor"
* commande avec arduino, pont en H, pas de capteurs: http://elabz.com/bldc-motor-with-arduino-circuit-and-software/
* commande en position (step 8):https://www.instructables.com/id/DIY-Brushless-Gimbal-with-Arduino/
* modélisation matlab, commande avec onduleur http://dirac.epucfe.eu/projets/wakka.php?wiki=P09A03index
