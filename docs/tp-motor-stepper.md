Commande de moteur pas-à-pas
======

Les moteurs [pas-à-pas](https://en.wikipedia.org/wiki/Stepper_motor) sont présents
dans de nombreux appareils grand public (imprimante, lecteurs cd, etc...)
et sont commandés en boucle ouverte.

Ils permettent un positionnement fin, pas-à-pas. La précision des
moteurs les plus courants est de 1.8 degrés, c'est à dire qu'il faut 200
pas pour faire un tour complet.

Pour avancer d'un pas dans une direction ou l'autre, il faut envoyer 
une suite d'impulsions bien précise 
(cf [wikipedia](https://fr.wikipedia.org/wiki/Moteur_pas_%C3%A0_pas#Moteur_%C3%A0_aimant_permanent_bipolaires))
sur les 4 fils du moteur bipolaire.

Pour éviter de programmer "à la main" la mise sous tensions des deux 
bobines avec le micro-contrôleur Arduino, il existe des circuits de
commande tout faits, comme nous le verrons ci-dessous. Il suffit 
d'envoyer une impulsion (un step) au circuit de commande pour qu'il fasse
avancer le moteur d'un pas.

Ce circuit sera connecté à un Arduino, qu'il faudra programmer.


Commande directe avec driver pololu
------

Dans cette partie on branche directement l'arduino sur le driver pololu
A4988 comme indiqué sur ce montage.

![montage](fig/circuit_pololu.png)

ATTENTION :

*  faire vérifier le montage.
*  le condensateur est absolument nécessaire.
*  ne jamais débrancher le moteur de son driver lorsqu'il est en opération. Pour un arrêt d'urgence, il faut débrancher l'alimentation séparée.
*  ne pas dépasser 12V 0.5A pour l'alimentation de la carte Pololu.

Questions:

1.   Effectuez le branchement nécessaire.
1.   Ecrivez un programme qui pilote le moteur pas-à-pas en position.
1.   Idem en vitesse.


Commande avec RAMPS 1.4
------

ATTENTION :

*  faire vérifier le montage.
*  ne jamais débrancher le moteur de son driver lorsqu'il est en opération. Pour un arrêt d'urgence, il faut débrancher l'alimentation séparée.
*  ne pas dépasser 12V 4A pour l'alimentation de la carte RAMPS.

Dans cette partie on branche l'arduino Mega 2560 sur une carte d'extension 
nommée [RAMPS 1.4](https://reprap.org/wiki/RAMPS_1.4/fr).
Elle permet de connecter plusieurs drivers à l'arduino Mega comme 
indiqué sur ce montage. 

![montage](fig/750px-Rampswire14.svg.png)

Questions:

1.  Installez la librairie AccelStepper sur votre PC (voir ci-dessous).
1.  Effectuez les branchement nécessaire. Les pins sont fixés, comme vous le verrez dans le fichier ramps_test/ramps_test.ino
1.  Testez le fonctionnement avec le fichier contenu dans [code/ramps_test/](https://gitlab.com/hazaa/iut-projets/tree/master/code/ramps_test)
1.  Idem avec constant speed (déplacement à vitesse constante):  [code/ramps_AccelStepper/](https://gitlab.com/hazaa/iut-projets/tree/master/code/ramps_AccelStepper) 
1.  Idem avec moveTo (déplacement vers position absolue):  [code/ramps_AccelStepper_moveTo/](https://gitlab.com/hazaa/iut-projets/tree/master/code/ramps_AccelStepper_moveTo) 
1.  Idem avec move (déplacement relatif): [code/ramps_AccelStepper_move/](https://gitlab.com/hazaa/iut-projets/tree/master/code/ramps_AccelStepper_move) 
1.  Commande avec le moniteur série de Arduino IDE. Adaptez le code [adafruit-lesson-16](https://learn.adafruit.com/adafruit-arduino-lesson-16-stepper-motors/arduino-code), pour notre cas (avec carte RAMPS et AccelStepper).
1.  Commandez le moteur en position depuis Python. Le code Arduino de la
    question précédente sera repris tel quel. Il faudra ajouter un code python
    qui remplace le moniteur série Arduino IDE en envoyant des instructions.
    Vous utiliserez le [TP Arduino](tp-arduino.md), en particulier le dialogue série.



Installation de la librairie AccelStepper :

*  récupérez [AccelStepper-1.48.zip](https://gitlab.com/hazaa/iut-projets/blob/master/code/AccelStepper-1.48.zip)
*  décompresser la librairie sur votre PC dans le répertoire de travail de l'arduino (à vérifier selon les versions de Arduino IDE, Dossier Personnel/Arduino/libraries).
*  vos projets arduino qui utilisent cette librairies doivent se situer dans le répertoire Dossier Personnel/Arduino, sinon Arduino IDE ne trouvera pas la librairie AccelStepper lors de la compilation.
*  Fonctions utiles: déplacement vers une position absolue: moveTo(), vers une position relative: move()


Ressources
------

*  adafruit [learn steppers](https://learn.adafruit.com/adafruit-arduino-lesson-16-stepper-motors) avec driver L293D (pas de RAMPS).
*  reprap [List_of_Firmware](https://reprap.org/wiki/List_of_Firmware)
*  pololu [A4988](https://www.pololu.com/product/1182/resources), [github](https://github.com/laurb9/StepperDriver)
*  carte [RAMPS 1.4](https://reprap.org/wiki/RAMPS_1.4/fr)
*  ancien TP [scenari](http://eprel.u-pec.fr/eprel//nzltnwj/4513/document/2015_2016/co/activ_appren_arduino_motor_3.html)

FAQ
------

| Question |  | Réponse | 
| -------- | -------- | -------- | 
| Sur le robot: | | |


Extension
------




