Commande de moteur à courant continu
======

Le département GEII dispose de cartes de commande toutes faites 
pour commander des moteurs à courant continu (MCC) ou pas-à-pas.
Mais dans un premier temps nous devons comprendre la base:
le hacheur série, qui est en fait présent dans toute carte de 
commande de MCC.


# Hacheur série pour moteur à courant continu

Vous l'avez déjà rencontré en projet S1 (sujet TD "variation de vitesse d'une machine à courant continu").

On peut voir le hacheur comme un interrupteur qui serait ouvert et fermé avec un rapport cyclique donné.

>    **Question**: calculer la tension moyenne aux bornes d'une charge resistive connectée à un hacheur, en fonction du rapport cyclique alpha.

On peut réaliser un hacheur avec un port PWM d'un arduino et un transistor. 
Quand le PWM est à l'état bas (0V), le transistor est bloqué et se comporte comme un interrupteur ouvert.
(voir projet S1 Figure 6). 

>    **Question**: cabler le montage en remplaçant l'interrupteur par l'ensemble transistor + Arduino, et la charge resistive par une led+résistance. Pour la source de tension, on prendra le port +5V de l'arduino.

Puis on remplace la charge resistive par un MCC (=charge inductive).
Il faut une diode pour assurer la continuité du courant, et une
inductance de lissage.

![montage](fig/tp-motor_schema.png)

>    **Question**:  cabler le montage. Le transistor est un mosfet protégé par un résistance de 200 ohm. Le montrer à l'enseignant avant mise sous tension.

Matériel: 

*   Transistor mosfet "logic level" STP55NF06L
*   MCC 
*   Arduino
*   diodes
*   alimentation de bureau
*   résistance
*   inductance de lissage


>    **Question**: programmer l'arduino pour faire tourner le moteur à une vitesse réglable.

>    **BONUS**: régime transitoire. Résoudre, simuler avec simulink ou python.
 
>    **BONUS**: ordre de grandeurs des constantes de temps (impact des vibrations). 

# Pont en H

en cours...

# Ressources

*  sites:
   *   https://fr.wikipedia.org/wiki/Hacheur_(%C3%A9lectronique)
   *   http://www.lerepairedessciences.fr/sciences/agregation_fichiers/Electronique/Cours_hacheurs.pdf
   *   hacheur série sur [physique.vije.net](http://physique.vije.net/TSTI/index.php?page=hacheur)
   *   http://garnero.michel.free.fr/Docs-isen/ENP/1-Hacheurs.pdf
   *   ens cachan: [tp redresseur, hacheur](http://www.physique.ens-cachan.fr/pagregp/enseignement/TP/TP%20hacheur), [identification de ses paramètres](https://jflamant.files.wordpress.com/2014/02/m1-idmcc.pdf)
   *   Chevassu [ENSM](http://mach.elec.free.fr/electronique-puissance/cours-electronique-puissance.pdf)
   *   hacheur série bipolaire simplifié [iut nimes](http://www.perso.iut-nimes.fr/fgiamarchi/wp-content/uploads/2010/08/Commande-Moteur-CC-01-rev1.pdf)
   *   diode [wikipedia](https://fr.wikipedia.org/wiki/Diode)
*  livres:
   *   Séguier Labrique Delarue [ELectronique de puissance 10ed](https://www.dunod.com/sites/default/files/atoms/files/9782100738663/Feuilletage.pdf)
*  fiches techniques:
   *   tutoriel mosfet [instructables](http://www.instructables.com/id/Transistor-Basics-MOSFETs/), [oscarliang](https://oscarliang.com/how-to-use-mosfet-beginner-tutorial/)  
   *   transistor mosfet [STP55NF06L](http://www.st.com/resource/en/datasheet/stp55nf06l.pdf)
*  sujet ER1 "Variation de vitesse d'une machine à courant continu" module M1202-1203-1207
*  UPSTI: sujet [Arduino+MCC+capteurs effet hall](https://www.upsti.fr/documents-pedagogiques/arduino-une-carte-pour-la-robotique), sujet [hacheur](https://www.upsti.fr/documents-pedagogiques/tp-hacheurs)


Matériel disponible
------

## Cartes de commande toutes faites

*   PmodHB5:
	*    quantité disponible: >5
*   arduino [motor shield v2](https://www.adafruit.com/product/1438) (attention, le notre est le v1)
    *    alimentation: par le bornier M+/GND
    *    connection moteur: les deux bornes M1    
    *    quantité disponible: 1
*   2x carte MD22 + moteur DC: voir la [documentation](http://www.robot-electronics.co.uk/htm/md22tech.htm) ; (sinon pour le mode I2C, voir cet [exemple](http://www.robot-electronics.co.uk/htm/arduino_examples.htm))
    *    utiliser le mode analogique, le switch est On/On/On/Off
    *    connection: 0 et +5V sur l'arduino en entrée, pin PWM sur SCL et/ou SDA ; alimentation séparée en sortie; moteur DC sur M1/M1.
    *    quantité disponible: 2
*   moteur pas-à-pas: voir la partie correspondante dans [page](http://eprel.u-pec.fr/eprel//nzltnwj/4513/document/2015_2016/co/activ_appren_arduino_motor.html)
	*    quantité disponible: >7
*   makeblock + moteur DC: [makeblock](http://makeblock.cc) avec Me-base et drivers correspondants: v2.0?, [v3](https://github.com/Makeblock-official/Makeblock-Libraries)
	*    quantité disponible: 2
*   composant L??
    *    pas de carte électronique pour le monter.
    

## Moteurs

*   moteurs DC:
    *   kit makeblock: 2 ou 3 moteurs avec réducteur.
*   moteur pas-à-pas:
    *   nema 23: >10 disponibles


FAQ
------

*   à quoi sert la diode de roue libre: à assurer la continuité du courant dans le moteur, même quand le transistor est bloqué. Il existe un risque de claquage du transistor si cette diode n'est pas présente [forum](https://answers.yahoo.com/question/index?qid=20081224104622AAp3QeK)
*   à quoi sert l'inductance de lissage
*	interprétation du comportement d'une inductance quand on commute

Extension
------

*   asservissement courant+vitesse. cf infineon motor control shield arduino BTN8982TA
