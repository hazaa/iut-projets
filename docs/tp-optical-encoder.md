Codeur optiques et photocoupleurs
------

Cette page concerne les codeurs optiques et les photocoupleurs.
Ils permettent de connaître une vitesse de rotation ou la position 
d'un capteur par rapport à d'une bande striée (linéaire ou circulaire).
La position est calculée de manière incrémentale, on ne la connaît pas de 
manière absolue.

Il existe de nombreux dispositifs dans les imprimantes à jet d'encre pour
localiser la tête d'impression.

En général le capteur est composé d'un émetteur (LED) et d'un ou deux récepteur
(phototransistor ou photodiode). Le capteur seul ne sert pas à grand chose,
il faut aussi une bande striée.

Si le composant comporte deux capteurs, on pourra calculer la direction
du mouvement. Sinon, on ne peut pas.

Quand on récupère ces composants, en général on ne dispose pas de leur notice
technique. Il faut donc comprendre par nous mêmes comment ça marche.

*   Comment identifier le cablage de ces composants: site [reprap](http://reprap.org/wiki/Optical_encoders_01)
*   Comment vérifier que la LED marche ? en général, les LED sont infrarouges, on ne le s voit pas briller à l'oeil nu. Mais avec votre smartphone, vous pourrez capter le rayonnement.

Codeur optiques 
------

Les codeurs optiques sont constitués d'une LED et de *deux* capteurs.
Cela permet de connaître la direction du mouvement, et donc de calculer
une position et une vitesse.

Principe d'un encodeur *incrémental* sur [wikipedia](https://en.wikipedia.org/wiki/Optical_encoder#Incremental_rotary_encoder).


__Exemple__: carte QK1-3007-03 (capteur D15 7417)

La Led se trouve sur les bornes A,B. Pour les autres bornes, en prenant
la même convention de nommage que sur le site [reprap](http://reprap.org/wiki/Optical_encoders_01):


| noir/rouge | C | D | E | F | pin | 
| -------- | -------- | -------- | -------- | -------- | -------- |
| C | x  | 0.6  | 1.1   | 1.8 | Out 1 | 
| D | 1.6  | x | 1.8   | 1.6  | Gnd  | 
| E | 0.7 | 0.7  | x   | 0.7 | +5V  | 
| F | 1.9 | 0.7 | 1.9    | x | Out 2 | 


__Exemple__: capteur 6642 sur carte BJ4300...

le capteur est soudé sur une carte.

__Exemple__: capteur Avago 80058 L935

*  LED: on identifie facilement les deux pins pour la LED: jaune (+), orange (-)
*  les capteurs donnent une réponse anormale avec le voltmètre. 


Photocoupleurs
------

Les photocoupleurs sont constitués d'une LED et d'*un* capteur.
Cela ne permet pas de connaître le sens du mouvement.
Si le mouvement est toujours dans le même sens, on peut calculer 
une position et une vitesse.
Sinon, on ne peut pas, il faut ajouter un autre capteur.

La théorie est expliquée par exemple sur 
[wikipedia](https://fr.wikipedia.org/wiki/Photocoupleur)

A noter: la vitesse de commutation varie selon qu'on a un phototransistor 
(0,1ms typique) ou une photodiode (0,1us typique). Cela limite donc la vitesse
limite qu'on peut mesurer, en fonction de l'espacement des traits sur la bande.
Dans les imprimantes à jet d'encre, ces capteurs sont plutôt utilisés comme 
"fin de course" plutôt que pour estimer une position.

On trouve de nombreux projets sur [youtube](https://www.youtube.com/watch?v=-vAxXwLKzkI) 
ou autres [blogs](http://www.utopiamechanicus.com/article/arduino-photo-interruptor-slotted-optical-switch/)
mais pas toujours bien documentés.

__Exemple__: capteur OPB704

Pour ce capteur "à plat" nous disposons de la documentation.

*   https://solarbotics.com/product/opb704/
*   Exemple: https://www.youtube.com/watch?v=Em_RfpFjTvY

__Exemple__: carte HP C9017-80055-A 

Elle est munie d'un photocoupleur. Le cablage est le suivant:
*   jaune (+) et vert (-): LED.
*   le fil vert est partagé avec le récepteur de lumière (probablement un phototransistor).
  A priori, le noir est le collecteur (C) et le vert l'émetteur (E). Le courant va du C vers le E.
  
Schéma de [cablage théorique](https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Optocoupler_Circuit.svg/250px-Optocoupler_Circuit.svg.png)
Exemple de valeurs pour les resistances [sparkfun](https://www.sparkfun.com/datasheets/PCB/PI-1120%20Breakout.pdf)
R1 =220 Ohm, R2=10kOhm  


__Exemple__: carte HP Q8293-6055 B

composant non identifié. régulateur de tension ?

__Exemple__: capteur P1230C56A sur carte TK-MFI 94V-O

voir la carte HP C9017-80055-A.

__Exemple__: capteur 6B sur carte QHI-1206-03

voir la carte HP C9017-80055-A.
