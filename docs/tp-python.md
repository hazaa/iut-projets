TP Python
======

Plan du TP:

1.   Avec le professeur: mini-cours interactif en classe (voir les [diapos](python_diapo_v3.odp))
2.   Première série d'exercices dans un [terminal](#dans-le-terminal)
3.   Deuxième série d'exercices sur la plateforme [algorea](#algorea)  



# Dans le terminal

Cette première série d'exercices se fera sur votre machine, grâce au **terminal**. (taper Ctrl+Alt+T)
La version installée est python3.



## Premiers pas en Python

Calcul simple: Lancer Python en mode interactif. Taper les calculs suivants :
```python
2+3
a=2
print(a)
```

Listes: Toujours dans le mode interactif, taper les commandes suivantes. Interprétation : que fait la commande len() ?
```python
couleur = ['bleu','rouge','vert']
len(couleur)
couleur[1]
```

Boucles: On va créer une boucle for. Attention : il faut introduire une tabulation avant print, puis sauter une ligne après en tapant Entrée. Qu'observez-vous ? Recommencer en remplaçant [0,1,2] par range(10).
```python
for i in [0,1,2]:
	print(i)
```

Fonctions: on va créer une fonction simple à l'aide de def. Attention : il faut introduire une tabulation et sauter deux lignes comme ci-dessus.
```python
def carre(x):
   x_carre = x * x
   return x_carre
```
Choisissez un entier n et tapez c=carre(n). Qu'observez-vous ?


Saisie clavier:  pour demander à l'utilisateur de saisir une valeur au clavier
```python
myname = raw_input('quel est ton nom ?')
```
NB: pour convertir en entier, utiliser int():
```python
age = int(raw_input('quel est ton age ?'))
```



## Scripts

Editer et exécuter un script: pour l'instant nous sommes restés dans l'interpréteur Python. Nous allons maintenant écrire un script simple, afin de l'exécuter depuis la ligne de commande. Créer le nouveau fichier monscript.py avec l'éditeur de texte Geany, avec le contenu suivant :

```python
# -*- coding: utf-8
def mafonction():
    for i in [0,1,2,3,4,5]:
		print("i=".format(i))
mafonction()
```

Puis ouvrir un terminal (Ctrl+Alt+T), exécutez le script :
```python
python3 monscript.py
```



## Python et réseau

*  Sockets: [doc python](https://docs.python.org/2/library/socket.html), voir les exemples en bas. Voir un exemple avec la vision dans [TP vision whycon](tp-vision-whycon.md)
*  Extension: [zmq](http://eprel.u-pec.fr/eprel//nzltnwj/4513/document/2015_2016/co/activ_appren_python_5.html)



# Algorea

Cette plateforme en ligne vous permettra de vous exercer chez vous. 
Depuis votre navigateur (firefox), vous vous connectez sur un serveur qui fait tourner Python3.
Python3 n'est pas installé sur la machine.


1.   Rendez-vous sur http://parcours.algorea.org
2.   Connectez-vous en créant un login
3.   Rejoignez le groupe 2017.geii.dut2.A1 (ou B1) 
4.   Cliquer sur l'onglet "Progresser" et faites les exercices suivants: 
   *   chapitre 1: exo "Hello world!"
   *   chapitre 2: exo "Punie!"
   *   chapitre 3: exo "Algoreathlon"
   *   chapitre 4: exo "Awalong"
   *   chapitre 5: exo "Voyagez léger!"



# En savoir plus

*   Livre de G.Swinnen [python3](https://inforef.be/swi/python.htm)
*   Python est un logiciel libre
*   Plates-formes d'auto-apprentissages: ????????????




# FAQ


| Question | Réponse | 
| -------- | -------- | 
| | | 
