Raspberry Pi
======

La Raspberry Pi (ou raspi) est un mini-ordinateur capable de faire tourner un système
d'exploitation complet (linux ou windows IoT), et capable de traitement d'images complexe
nécessaire pour la vision artificielle.

Vous trouverez de nombreux stages "systèmes embarqués" centrés sur cette plateforme [exemples](https://www.indeed.fr/Emplois-Raspberry).
Il existe des formations payantes pour apprendre à utiliser cette plateforme [exemple](https://www.orsys.fr/formation-raspberry-pi-3.html)


On va voir comment :

*  se connecter à la Raspberry Pi.
*  effectuer les opérations courantes en ligne de commande.
*  copier des programmes de/vers la Raspberry pi.



Matériel nécessaire:

* 1 Raspberry Pi avec alimentation.
* 1 écran, 1 clavier, 1 hub USB jaune, 1 cable HDMI+convertisseur DVI.
* 1 écran HDMI ou DVI.
* 1 prise réseau.
* 1 caméra raspberry pi



### Installer un système d'exploitation Buster sur une Raspberry Pi

*  Préparatifs:
   *  télécharger l'image du système d'exploitation "Raspberry Pi OS (Legacy) with desktop"  [raspi OS](https://www.raspberrypi.com/software/operating-systems/)
   *  décompacter le fichier .zip. On obtient un fichier .img, qui est une **image** du système d'exploitation.
   *  Créer une SD bootable : avec [Raspberry Pi Imager](https://www.raspberrypi.com/documentation/computers/getting-started.html#using-raspberry-pi-imager) (ou méthode avancée: [installing-images-on-linux](https://www.raspberrypi.com/documentation/computers/getting-started.html#installing-images-on-linux))
   *  Une fois la copie terminée, aller dans le répertoire /boot de la carte SD, puis créer un fichier vide nommé "ssh". ("For headless setup, SSH can be enabled by placing a file named ssh, without any extension, onto the boot partition of the SD Card." [enabling-the-server](https://www.raspberrypi.com/documentation/computers/remote-access.html#enabling-the-server))
   *  Insérer la carte SD dans le slot prévu sur la Raspberry. 
   *  pour brancher la caméra, suivez la [vidéo](https://www.raspberrypi.com/documentation/accessories/camera.html#installing-a-raspberry-pi-camera)
   *  Brancher un écran, un hub usb (pour le clavier et la souris), une prise réseau si possible. **Alimentez en dernier**.

*  **ATTENTION**: 

   * ne **jamais** débrancher la raspberry sans avoir fait d'abord l'extinction logicielle. Demander à l'enseignant comment l'éteindre.


   * ne pas brancher/débrancher le clavier/souris en cours d'utilisation. 

* Alimentez, laissez le système d'exploitation s'installer, répondez aux questions posées (choix de la langue,...).

  *  créer une utilisateur supplémentaire 'geii' avec un mot de passe 'geii'. 
  *  Ne pas changer le mot de passe administrateur pour l'utilisateur "pi"  lorsqu'on vous le propose.

* Une fois l'installation terminée, on va **configurer**. Il faut pour cela être loggé en tant qu'**administrateur** (login: pi, password: raspberry). Ouvrir le Menu framboise>préférences>configuration du raspberrypi >

  * Sous-menu Système:

    *  connexion automatique: 'disabled'

  * Sous-menu Interfaces

    *  ssh: activer

    *  camera: activer

  * Sous-menu Performances: 
    *   overclock: 900MHz

* Configuration des droits de l'utilisateur 'geii': il faut donner des droits à l'utlisateur 'geii', en se connectant en tant qu'administrateur (login 'pi'). Puis ouvrez un **terminal** et:

  *  Si vous n'avez pas encore crée l'utilisateur geii lors des étapes ci-dessus, tapez ```sudo adduser geii```
  *  ```sudo adduser geii video```
  *  ```sudo adduser geii dialout```

* Logiciels déja installés par défaut sur Buster:   paquets apt déjà installé: git ; modules python déja installés: picamera, pyserial

* Logiciels à installer, en tant qu'utilisateur 'geii': modules python ```nanpy aiohttp aiortc```

  * connexion internet: si vous avez un navigateur web: loguez vous sur le portail IUT en allant sur [https://portail.iutsf.org:8003/index.php?zone=etu_upec](https://portail.iutsf.org:8003/index.php?zone=etu_upec))
 
  * si votre navigateur ne s'ouvre pas: téléchargez ```script_curl_login.sh``` sur le PC de bureau (ce fichier se trouve sur gitlab dans le répertoire [code](https://gitlab.com/hazaa/iut-projets/-/tree/master/code)). Copiez-le vers la raspi. Rendez ce fichier executable en tapant dans un console ```chmod u+x script_curl_login.sh```. Enfin executez ce script: ```./script_curl_login```. Entrez votre login et votre password IUT. 

  * vérifier d'abord que les modules sont absents: lancer python3 puis essayer d'importer les modules. Exemple: ```import nanpy```
  
  * modules Python: ouvrir un terminal et taper ```pip3 install nanpy aiohttp aiortc```

  * si vous n'avez pas de connexion internet: 

     *  nanpy: avec le PC de la salle TP, récupérer nanpy-firmware-0.9.6.tar.gz sur [pypi](https://pypi.org/project/nanpy/#files). Copier ce fichier sur la raspi. En tant qu'utlisateur geii, taper: ```pip3 install nanpy-firmware-0.9.6.tar.gz```

     * aiohttp, aiortc: trouver une raspi qui a réussi à installer ces modules et leurs dépendances.
        * sur votre raspi vérifiez que le répertoire suivant existe: ```~/.local/lib/python3.7/site-packages/```. Il faut créer chacun des répertoires, en commençant par ```.local```. Puis faites ```cd ~/.local/lib/python3.7/site-packages/```
        * depuis votre raspi faites la copie récursive des modules ```scp -r geii@10.14.74.XXX:~/.local/lib/python3.7/site-packages/*  ./```


Se connecter à la Raspberry Pi avec écran/clavier
------

**Pré-requis**: installation effectuée.



*  Brancher un écran, un hub usb (pour le clavier et la souris), une prise réseau si possible. **Alimentez en dernier**.
*  Se logguer en tant que 'geii'.
*  vous pouvez rester en mode console, ou lancer une interface graphique: ./startx
*  Quelle est mon adresse IP sur le réseau: dans la console, taper ifconfig (ou /sbin/ifconfig). Chercher "inet adr:" 

**ATTENTION**: 

* ne **jamais** débrancher la raspberry sans avoir fait d'abord l'extinction logicielle. 
   
   *  Pour **éteindre proprement**: connectez-vous en tant qu'administrateur: ```su pi``` puis tapez ```sudo /sbin/halt``` et attendez 2min avant de débrancher.

* ne pas brancher/débrancher le clavier/souris en cours d'utilisation. 





Se connecter à Raspberry Pi à distance 
------

**Attention**: Brancher la Raspberry sur une prise réseau **AVANT** de l'alimenter.



Dans cette partie, la Raspberry Pi n'est pas reliée à un écran, mais elle est reliée au réseau. On va se connecter depuis un autre ordinateur à travers le réseau Ethernet, grâce à la commande **ssh** (=*secure shell*). 

Puis, une fois connecté à la raspberry pi, on utilisera les commandes Linux pour effectuer diverses opérations. Un rappel sur les commandes linux est donné plus bas.

L'IP de votre raspi est donnée par le Tableau ci-dessous, à partir de l'adresse MAC inscrite sur l' étiquette. 

|Nom| Adresse physique (MAC) | Adresse logique (IP) |
|--------| -------- | -------- |
|pi1|b8 :27 :eb :67 :b3 :2d | 10.14.74.176|
|pi5|b8 :27 :eb :a3 :00 :69 | 10.14.74.174|
|pi2|b8 :27 :eb :a0 :32 :10 | 10.14.74.172|
|pi0|b8 :27 :eb :45 :00 :bd | 10.14.74.173|
|pi4|b8 :27 :eb :c9 :ce :42 | 10.14.74.175 |
|pi6|b8 :27 :eb :e9 :10 :f4| 10.14.74.177|
|pi3|b8 :27 :eb :d6 :5f :7b | 10.14.74.178|
||b8 :27 :eb :81 :2b :f3||
||b8 :27 :eb :e1 :16 :99||


Ouvrez un terminal de ligne de commande depuis votre PC de bureau sous Ubuntu (pas besoin des machines virtuelles windows). Puis connectez-vous avec le logiciel ssh qui permet une connexion sécurisée comme ci-dessous. Remplacer ci-dessous login par geii ou mctr ou er3. (le mot de passe est le même que le login) :

>    ssh -X login@10.14.74.172



Se connecter via l'interface série
------

A faire...

Liens: [uart](https://elinux.org/RPi_Serial_Connection) (arduino as USB/uart converter [create.arduino.cc](https://create.arduino.cc/projecthub/PatelDarshil/ways-to-use-arduino-as-usb-to-ttl-converter-475533), resistor level shifter necessary), réseau croisé sans routeur, wifi.



Opérations système de base 
------

**Attention**: se connecter en tant que 'geii', surtout pas en tant qu'administrateur (on va faire des effacements de fichiers).

Ouvrez un terminal.

Liste de commandes Unix/Linux, à faire avec l'enseignant. 

| Commande | Description | Usage |
| -------- | -------- | -------- |
|cd	|se déplacer dans un autre répertoire |	$cd MONREPERTOIRE |
|man | obtenir de l’aide sur une commande |	$man cmd |
|cp | copier des fichiers | $cp SOURCE CIBLE |
|ls | lister le contenu d’un répertoire	| $ls REPERTOIRE|
|mkdir | créer un répertoire |	$mkdir REPERTOIRE|
|nano |	éditer un fichier |	$nano MON_FICHIER.txt |
|less |	afficher le contenu d’un fichier texte | $less MON_FICHIER.txt|
|rm | effacer un fichier | $rm MON_FICHIER.txt |
|ps	| voir la liste des processus qui tournent | $ps -aux|
|kill |	tuer un processus génant | $kill -9 5623|
|scp | copier des programmes de la raspberry pi vers votre PC dans la salle de TP |$scp er3@10.14.72.60:~/monprog.py ./ |
|ifconfig| affiche la configuration réseau| $/sbin/ifconfig|
|touch| créer un fichier vide|$touch mon_fichier_vide.txt|
|halt | extinction   | #/sbin/halt  |

**Exercice 1:**

1. Afficher le contenu du répertoire courant à l’aide de la commande ls.
1. Créer un nouveau répertoire portant le nom MCTR_nom1_nom2 pour votre binôme à l’aide de la commande mkdir.
1. Créer un nouveau fichier avec l’éditeur de texte nano et écrivez quelques mots au hasard dans ce fichier : nano mon_fichier.txt
1. Copier ce fichier dans votre répertoire MCTR_nom1_nom2 à l’aide de la commande cp.
1. Déplacez-vous dans votre nouveau répertoire MCTR_nom1_nom2 à l’aide de la commande cd.
1. Une fois dans votre nouveau répertoire, affichez le contenu du répertoire avec la commande ls. Afffichez le contenu de votre nouveau fichier mon_fichier.txt à l’aide de la commande less

Plus d'informations: [documentation](https://www.raspberrypi.com/documentation/computers/using_linux.html)


Applications: serveur de fichier, capture image/vidéo
------

**Pré-requis**: avoir installé et activé la caméra (activation dans le menu de configuration).



Votre Raspberry peut se comporter comme un serveur de fichiers. 
Dans la console de votre raspi dont vous connaissez l'IP (exemple: 10.14.74.172), taper :

>python3 -m http.server

Sur un PC de la salle TP, ouvrez un navigateur et dans la barre de navigation, taper ip-de-votre-raspi:8000 (exemple: 10.14.74.172:8000)

Votre raspi peut capturer des images et des video, et streamer une video sur un autre pc, en utilisant les commandes [raspicam](https://www.raspberrypi.com/documentation/accessories/camera.html#raspicam-commands).



**Exercice 2:**

1. trouver comment capturer une image. Copier cette image sur un des PC de la salle de TP avec scp puis avec le serveur python ci-dessus. Vérifier sur le PC de la salle TP que l'image est la bonne.
1. trouver comment capturer une vidéo. Copier cette image sur un des PC de la salle de TP avec scp puis avec le serveur python ci-dessus. Vérifier sur le PC de la salle TP que la vidéo est la bonne. (Note: le fichier produit sera en h264, pas lisible directement par un lecteur de videos).
1. trouver comment streamer un flux video en résolution 320x200 depuis la raspi vers un PC de la salle de TP.




Raspberry + Python
------

Pré-requis: [TP Python](tp-python.md)

Votre Raspberry peut faire des appels à des programmes du système en utilisant un langage de script tel que Python.

**Exercice 3:**

1.  trouver comment prendre à intervalles réguliers (exemple: 1 minute) des photos depuis la Raspberry. Les ranger automatiquement dans un répertoire. 
1.  Même chose, mais en plus: dans un fichier nommé log.txt enregistrer l'heure de création de la photo, le nom du fichier enregistré. 


Rappels Python:

*   Pour appeler un programme système depuis Python: subprocess
	```python
	import subprocess as sp
	import time
	cmd = "touch monfich"   
	proc = sp.Popen([cmd], shell=True, stdin=None, stdout=None, stderr=None)
	```
	
* créer un fichier texte: livre de Swinnen [python 3](https://inforef.be/swi/python.htm), chapitre "Les fichiers" 

* ajouter une ligne dans un fichier

* heure: [python3: time](https://docs.python.org/3/library/time.html#module-time)



Raspberry + Arduino
------

Pré-requis: [TP Arduino](tp-arduino.md) 

Dans cette partie on veut commander l'Arduino depuis la raspi.

Solution 1 : faites la partie Dialogue série dans le TP [Arduino](tp-arduino.md)

Solution 2 : utiliser l'arduino en mode "slave" avec une librairie telle que nanpy.
Le programme Nanpy.io est installé sur l'arduino, et ne doit pas être modifié.
Nanpy.io permet à Python de "prendre le contrôle" de l'arduino de manière transparente.

**Exercice 4:**

1.  Flasher nanpy-firmware depuis le PC de bureau. Pour cela, téléchargez-le depuis [github](https://github.com/nanpy/nanpy-firmware), décompactez-le, ouvrez un terminal et tapez ./configure.sh. Puis depuis le logiciel de programmation arduino, cliquer sur Fichier/Ouvrir, et sélectionner Nanpy.ino
1.  Sur la Raspberry, ouvrir un terminal et lancer python en mode interactif. Taper :
	```python
	import time
	from nanpy import SerialManager, ArduinoApi
	connection = SerialManager(device=’/dev/ttyACM0’)
	a = ArduinoApi(connection=connection) # on se connecte
	a.pinMode(13, a.OUTPUT) # on met le pin 13 en mode "output"
	a.digitalWrite(13, a.HIGH) # observer la Led sur l’arduino
	a.digitalWrite(13, a.LOW) # idem
	```
1.  Avec une boucle Python, et la commande sleep, faire clignoter la led.
1.  Programmez un chenillard avec 3 leds, c’est-à-dire que chacune des diodes s’allume à tour de rôle pendant une courte durée. Les pins doivent être remis à 0 à la fin. (Conseil : servez-vous d’une boucle for).
1.  On peut moduler l’intensité d’éclairage de la Led en envoyant un signal PWM 3 . Il faut écrire sur le pin en mode analogique, mais tous les pin ne le permettent pas (il faut le symbole devant le pin). On passe au pin 11 :
	```python
	a.pinMode(11, a.OUTPUT) # on met le pin 11 en mode ’output’
	a.analogWrite(11, 128) # on écrit la valeur 128 sur le pin 11
	```



Raspberry Pi + Arduino + Python
------

Pré-requis: [TP Python](tp-python.md)

Dans cette partie on reprend les exemples précédents en ajoutant une couche de python pour ajouter des nouveaux services: interface graphique, réseau.

Outils: En mode console, on ouvre un éditeur de texte (nano, console). Sinon, avec l'interface graphique (si vous avez lancé startx), avec les éditeurs de texte: leafpad ou midori.

**Exercice 5:**

*  interface graphique (GUI): ajouter une interface graphique pour les programmes de la partie [Raspberry + Arduino](#Raspberry-+-Arduino) avec Tkinter ou ncurses, sous Python.
*  réseau: sous Python, créer un serveur server.py qui tourne sur la raspberry et qui écoute sur un certain socket. Quand il est contacté par le client, le serveur allume les leds comme dans la partie [Raspberry + Arduino](#Raspberry-+-Arduino). Sur les PC de la salle de TP, créer un programme client.py qui envoie des ordres au serveur, situé sur le raspberry.

Outils:

*  créer une interface [Tkinter](http://nguyen.univ-tln.fr/share/IHM/transp_tkinter.pdf), livre de Swinnen [python3](https://inforef.be/swi/python.htm), chapitre "Tkinter" ,  [ncurses](https://docs.python.org/2/howto/curses.html)
*  [socket](https://docs.python.org/3/library/socket.html#example) sous python 



FAQ/Debug
------

| Question | Réponse |
| -------- | -------- |
| la caméra ne marche pas. | commencer par vérifier le branchement des nappes (voir lien dans la bibliographie) sur la carte sur laquelle la caméra est soudée, il y a un micro-connecteur. |
|  | Vérifier qu'il est bien branché. vérifier que la raspberry a détecté la caméra :  se connecter à la raspberry avec ssh -X login@ip |
|  | initialiser la caméra: sudo modprobe bcm2835-v4l2 |
|  | taper ls /dev/v* et voir si video apparaît. Si on ne le voit pas, la vidéo n'est pas trouvée. |
| pas d'accès à internet pour récupérer de paquets debian | liens [iut](https://espace-ressources.iutsf.org/public/informatique/informations/proxy?s[]=apt) , [raspberrypi.org](https://www.raspberrypi.org/documentation/configuration/use-a-proxy.md) |
|  | dans /etc/apt/apt.conf: Acquire::http::proxy "http://arwen.iutsf.lan:3128"; |
|  | utiliser [script_curl_login.sh](https://gitlab.com/hazaa/iut-projets/tree/master/code/vision_track/script_curl_login.sh) |
|  | Mirror local: https://www.raspbian.org/RaspbianMirrors/ |



Ressources
------

* site officiel [raspi](http://www.raspberrypi.org), [documentation](https://www.raspberrypi.com/documentation/computers/getting-started.html)

* magazines francophones: http://www.gnulinuxmag.com/

* livres: http://libgen.io/search.php?&req=raspberry

* TP IUT: https://bvdp.inetdoc.net/wiki/doku.php, https://hal.archives-ouvertes.fr/hal-02298767/,  https://eduscol.education.fr/sti/sites/eduscol.education.fr.sti/files/ressources/techniques/13294/13294-art-11-openplc-transformer-un-raspberry-en-api.pdf

*   e-book ENI [via BU upec](https://athena.u-pec.fr/view/action/uresolver.do?operation=resolveService&package_service_id=6862326540004611&institutionId=4611&customerId=4610): 
   *   François MOCQ Raspberry Pi "Exploitez tout le potentiel de votre nano-ordinateur", 2014, Réf. ENI : RIRASP | ISBN : 9782746087774. Chapitre "Que faire avec le raspberry pi"> un serveur de flux video.
   *   MoCQ "Raspberry 4" influxdb+Chronograf 
   
   

POUR LE PROF: install/config
------

### FAQ

* chromium ne marche pas sur raspi buster:
   * this is known issue: [forum](https://forums.raspberrypi.com/viewtopic.php?t=323478) [forum](https://forums.raspberrypi.com/viewtopic.php?t=324663)
   * installer chromium-browser à la place de chromium ```sudo apt-get install chromium-browser```
   * rebooter 

* apt package
   * proxy ??

*  nanpy : 
   *  python side: ```pip3 install nanpy```
   *  arduino side, sur machine salle TP: ```git clone https://github.com/nanpy/nanpy-firmware.git```, then follow instructions (cf firmware ; ./config.sh); then copy to arduino sketchbook


* installer des paquets python sans connexion internet (portail captif):
  *  apt: 
  *  pip:  raspi wheels  https://www.piwheels.org; ```pip install some-package.whl```
  *  quelles sont les wheels installées: ```~/.local/lib/python3.7/site-packages/aiohttp-3.8.1.dist-info```. Dans METADATA on trouve le numéro de version "3.8.1" et les dépendances. Dans WHEEL on trouve le suffixe pour la version et la plateforme: cp37-cp37m-linux_armv7l. Sur https://www.piwheels.org/simple/aiohttp/ on trouve le lien
  *  Télécharger une wheel raspi: ```wget https://www.piwheels.org/simple/aiohttp/aiohttp-3.8.1-cp37-cp37m-linux_armv7l.whl```
  *  Récupérer toutes les dépendances pour usage offline [stackoverflow](https://stackoverflow.com/questions/60182080/install-wheel-file-on-off-line-machine-which-has-different-processor), 
     *  FAIL ```pip3 download --index-url=https://www.piwheels.org/simple --platform linux_armv7l --no-deps -r requirements.txt```, avec dans requirements.txt: aiohttp==3.8.1 nanpy==0.9.6 aiortc==1.2.1
  *  Créer un tar.gz [stackoverflow](https://stackoverflow.com/questions/36725843/installing-python-packages-without-internet-and-using-source-code-as-tar-gz-and)
  *  pip own repository: https://packaging.python.org/en/latest/guides/hosting-your-own-index/, pip caching local packages: https://pip.pypa.io/en/latest/user_guide/#installing-from-local-packages



* streaming video embedded dans website: aiortc: 
   *  ```pip3 install aiohttp aiortc picamera```
   *  ```git clone https://github.com/sonkm3/aiortc-example.git```
   *  ```cd examples/raspberrypicamera; python3 python rpicamera.py```
   *  depuis un navigateur, se connecter à http://IP:8080
   *  NB: works for user=geii

* bug aiortc: sous Firefox dans le PC de la salle TP, on ne voit pas le flux video sur le serveur aiortc. 
   *  (depuis un browser en local sur la raspi. Il faut réinstaller chromium-browser.  FAILED: chromium-browser, dillo)
   *  sur lan: avec autres browser lightweight. SUCCESS: avec chrome. FAIL: midori, dillo, netsurf. 
   *  (tunnel SSH)
   *  (deprecated: embed vlc plugin in web page https://wiki.videolan.org/VLC_HowTo/Stream_to_a_website_(simple_version)/  )
   *  (port filtering: check which port is used. Can change it ? Ask which port is filtered. Which port is used by vlc ?O )

* streaming video avec vlc: 
   * vlc+tcp des deux côtés: (le plus fluide)
       * côté raspi: ```raspivid -t 0 -l -o tcp://10.14.74.176:3333```
       * côté PC salle TP: taper dans un terminal ```vlc tcp/h264://10.14.74.176:3333```   
   * vlc+http des deux côtés:
       * côté raspi: ```raspivid -o - -t 0 -hf -w 227 -h 227 -fps 5 | cvlc -vvv stream:///dev/stdin --sout '#http{mux=ffmpeg{mux=flv},dst=:8080}' :demux=h264```
       * côté PC salle TP: lancer vlc. Cliquer Media>ouvrir un flux réseau ; puis entrer l'url qui correspond à votre raspberry, par exemple```http://10.14.74.176:8080```
   * vlc+rtsp des deux côtés: bug???
       *  côté raspi: ```raspivid -o - -t 0 -w 800 -h 600 -fps 12 | cvlc -vvv stream:///dev/stdin --sout '#rtp{sdp=rtsp://:8080/}' :demux=h264```
       *  côté PC salle TP: lancer vlc. Cliquer Media>ouvrir un flux réseau ; puis entrer l'url qui correspond à votre raspberry, par exemple ```rtsp://<IP>:8080/```

* streaming + embedding in website:
   * vlc rtsp + 
   * aiortc: OK mais il faut chrome sur les PC de la salle TP (firefox ne marche pas)            
   * https://elinux.org/RPi-Cam-Web-Interface  = complete web interface for camera module. Apache.
   * https://www.rs-online.com/designspark/building-a-raspberry-pi-2-webrtc-camera Janus
   * rtsp server with gstreamer https://davidmays.io/streaming-raspberry-pi-video-via-rtsp-with-gstreamer-and-video4linux/
   * https://www.linux-projects.org/uv4l/installation/,  https://www.linux-projects.org/uv4l/tutorials/video-tracking-with-tensor-flow/

* TODO:
  * add exo: su, sudo
  * add geii to sudoers (halt, reboot)
  * add exo: cron 
  * uv4l+webrtc+tensorflow lite+MQTT (pour récuperer infos detection): https://www.linux-projects.org/uv4l/tutorials/video-tracking-with-tensor-flow/
  * expliciter: installation de paquet système / module python
  * migrer vers non-legacy, retest all (streaming is different) ; aiortc???
  * (node-red)


### Old OS

*  connecter camera: camera port: next to the ethernet port ; Place the strip in the connector (blue side facing the ethernet port); see [raspberrypi.org](http://www.raspberrypi.org/help/camera-module-setup/), [raspberrypi.org](http://www.raspberrypi.org/learning/python-picamera-setup/)
*  installation camera:
  *  flasher le nouveau firmware: sudo apt-get install rpi-update; sudo rpi-update
  *  configurer le support de la camera: sudo raspi-config
*  load module:   sudo modprobe bcm2835-v4l2


* configurer camera: [forum1](http://www.raspberrypi.org/forums/viewtopic.php?f=43&t=62364), [forum2](http://www.linux-projects.org/modules/sections/index.php?op=viewarticle&artid=16)
   >   $sudo apt-get install v4l-utils
   >   $v4l2-ctl --all
   >   $v4l2-ctl --set-fmt-video=width=320,height=200

* capture image/vidéo : raspistill , raspivid


OpenCV:

* dépendances: sudo apt-get install --no-install-recommends cmake rpi-update python-opencv libopencv-dev python-matplotlib git


ADMIN:

*  mssh dès le début: ssh mctr@10.14.74.174  ; mssh pi@10.14.74.17{0,1,2,3,4,5,6,7,8}
0
