Robot de téléprésence
======

Un robot de téléprésence est commandé à distance. Il est muni d'une caméra qui 
permet à un opérateur situé à distance de visualiser l'environnement du robot. 

Le robot doit donc faire tourner un serveur capable de:

* envoyer un flux vidéo à un client connecté sur un même réseau.
* écouter les ordres qui viennent du client par le réseau et commander des actionneurs via l'arduino.




Serveur de streaming
------

Sur la raspberry pi on fait tourner un serveur web, écrit en Python.
Ce serveur web va streamer le flux vidéo capté par la camera du raspi vers un client qui se connecte.
Le client se trouve sur une autre machine sur le même réseau
que la raspi, et se connecte au serveur.


*  installation sur la raspi:
   *  ```pip3 install aiohttp aiortc picamera```
   *  ```git clone https://github.com/sonkm3/aiortc-example.git```

*  Execution :
   *  ```cd aiortc-example```
   *  ```cd examples/raspberrypicamera```
   *  ```python3 rpicamera.py```
   *  depuis un navigateur de la salle TP, se connecter à ```http://IP:8080```
   

**Attention:** Firefox ne semble pas compatible, il faut essayer un autre navigateur.
Par exemple, téléchargez chrome sur chromium.org. Choisissez le fichier ```.deb```. Vous obtenez un fichier ```google-chrome-stable_current_amd64.deb```. Le décompacter (il s'agit d'une archive). Aller dans le répertoire google-chrome-stable_current_amd64 et décompactez ```data.tar.gz```.
Ouvrez un terminal, allez dans ```data/opt/google``` puis tapez ```./chrome```





**Si ça ne marche pas**, :

* solution de repli avec ```webcam.py```, cf [github](https://github.com/aiortc/aiortc/tree/main/examples/webcam)
* solutions de repli avec VLC (sans intégration dans une page web): 
   * vlc+tcp des deux côtés: (le plus fluide)
       * côté raspi: ```raspivid -t 0 -l -o tcp://10.14.74.176:3333```
       * côté PC salle TP: taper dans un terminal ```vlc tcp/h264://10.14.74.176:3333```   
   * vlc+http des deux côtés:
       * côté raspi: ```raspivid -o - -t 0 -hf -w 227 -h 227 -fps 5 | cvlc -vvv stream:///dev/stdin --sout '#http{mux=ffmpeg{mux=flv},dst=:8080}' :demux=h264```
       * côté PC salle TP: lancer vlc. Cliquer Media>ouvrir un flux réseau ; puis entrer l'url qui correspond à votre raspberry, par exemple```http://10.14.74.176:8080```
   * vlc+rtsp des deux côtés:
       *  côté raspi: ```raspivid -o - -t 0 -w 800 -h 600 -fps 12 | cvlc -vvv stream:///dev/stdin --sout '#rtp{sdp=rtsp://:8080/}' :demux=h264```
       *  côté PC salle TP: lancer vlc. Cliquer Media>ouvrir un flux réseau ; puis entrer l'url qui correspond à votre raspberry, par exemple ```rtsp://<IP>:8080/```





* Liens: streaming + embedding in website:
   * https://elinux.org/RPi-Cam-Web-Interface  = complete web interface for camera module. Apache.
   * https://www.rs-online.com/designspark/building-a-raspberry-pi-2-webrtc-camera Janus
   * rtsp server with gstreamer https://davidmays.io/streaming-raspberry-pi-video-via-rtsp-with-gstreamer-and-video4linux/
   * https://www.linux-projects.org/uv4l/installation/,  https://www.linux-projects.org/uv4l/tutorials/video-tracking-with-tensor-flow/

  


Serveur de commande à distance avec websockets
------

Dans cette partie on crée un serveur qui permet à un utlisateur qui dispose uniquement d'un navigateur web de commander un service (par exemple sur un robot). L'utilisateur peut faire des requêtes au serveur. Le serveur peut informer le client qu'un changement s'est produit, sans que l'utilisateur rafraîchisse sa page.

Pour cela on a besoin d'utiliser les websockets, ci-dessous la définition données par [wikipedia](https://fr.wikipedia.org/wiki/WebSocket):

> Le protocole WebSocket permet d'ouvrir un canal de communication bidirectionnel (ou "[full-duplex](https://fr.wikipedia.org/wiki/Duplex_(canal_de_communication))") sur un [socket](https://fr.wikipedia.org/wiki/Berkeley_sockets) TCP pour les [navigateurs](https://fr.wikipedia.org/wiki/Navigateur_web) et les [serveurs web](https://fr.wikipedia.org/wiki/HTTPd). Plus spécifiquement, il permet donc : 
>
> - la notification au client d'un changement d'état du serveur,
> - l'envoi de données en mode « pousser » (méthode Push) du serveur  vers le client, sans que ce dernier ait à effectuer une requête.



La figure ci-dessous explique comment procéder. On utilisera les fichiers dans le répertoire [code/UI/websocket](../code/UI/websocket). Le serveur de fichier permet au client de récupérer la page html qui contient le code javascript permettant de dialoguer avec le serveur de websocket.

![schema](fig/schema_websocket_telepresence_novideo.png) 

**Côté serveur**: dans ```server_echo_asyncio.py```

```js
async def echo(websocket, path):    
	print("A client just connected")
    try:
        async for message in websocket:
            print("Received message from client: " + message)
            await websocket.send("Pong: " + message)
    except websockets.exceptions.ConnectionClosed as e:
        print("A client just disconnected")

start_server = websockets.serve(echo, "localhost", PORT)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
```

**Côté navigateur**: dans ```websocket.html```

```html
<!DOCTYPE html>
<meta charset="utf-8" />
<title>WebSocket Test</title>
<script language="javascript" type="text/javascript">
  function init()
  {
	document.myform.url.value = "ws://localhost:8000/"
	document.myform.inputtext.value = "Hello World!"
	document.myform.disconnectButton.disabled = true;
  }
 function doConnect()
  {
    websocket = new WebSocket(document.myform.url.value);
	(...)
  }  
 function sendText() {
		doSend( document.myform.inputtext.value );
   }   
 function doSend(message)
  {
    writeToScreen("sent: " + message + '\n'); 
    websocket.send(message);
  }
  (...)  
</script>

<div id="output"></div>
<form name="myform">
(...)
<input type="button" name=sendButton value="Send" onClick="sendText();">
(...)
<input type="button" name=connectButton value="Connect" onClick="doConnect();">


</form>
</html> 
```







**Travail à faire** coté raspi, lancer 2 serveurs:


* un serveur qui écoute le traffic websocket, et peut commander l'Arduino. Deux serveurs sont proposés:
   * option 1 : ```server_echo_asyncio.py```. Il nécessite le module python websockets (si vous ne l'avez pas, ouvrez un terminal et tapez ```pip3 install websockets```). Ce serveur utilise asyncio, commme le serveur ```rpicamera.py``` ci-dessus.
   * option 2: ```SuperSimpleExampleServer.py```. Il nécessite le fichier ```SimpleExampleServer.py``` dans le même répertoire.
* (optionnel: un qui sert le fichier ```websocket.html``` au browser du client sur une autre machine. On a déjà vu comment faire avec Python. Si vous préférez, copiez le fichier ```websocket.html``` sur le PC de bureau et ouvrez-le avec firefox).


Côté PC de bureau, lancer un navigateur et connectez-vous à la raspi pour récupérer la page ```websocket.html```.
Puis cliquer sur connecter.
Vérifier que la communication fonctionne entre votre navigateur et le serveur de websocket sur la raspberry.

Modifiez le code côté serveur et la page html pour ajouter les fonctions suivantes:

1.  un bouton qui permet d'allumer une led sur l'arduino.
1.  une information sur l'état du robot: état d'un pin digital 0/1, état d'un pin analogique.

Liens:

*  Websockets avec nodejs [iut-fbleau](https://www.iut-fbleau.fr/site/site_web/nodejs/tp), connexion à un werveur de websockets [iut Nîmes](https://mmi.unilim.fr/~gres3/TP3/tp-websockets.pdf)
*  [reefwingrobotics](http://reefwingrobotics.blogspot.com/2017/03/controlling-raspberry-pi-via-web-browser.html)
*  ([blog.miguelgrinberg](https://blog.miguelgrinberg.com/post/stream-video-from-the-raspberry-pi-camera-to-web-browsers-even-on-ios-and-android) ). 

BONUS:

1. un bouton pour faire tourner un moteur. (attention, il faut un circuit spécifique pour commander un moteur depuis l'Arduino).
1. un affichage qui retourne une emsure du courant traversant le moteur, ou la vitesse de rotation de celui-ci.


Serveur de streaming + commande à distance du robot
------

En utilisant ce qui précède, codez un serveur qui permet à la fois de servir le fichier html, de streamer un flux video, et de dialoguer en websockets (pour commander un robot).

Deux notions **importantes** sont à retenir:

* il vaut mieux avoir **plusieurs petits serveurs**, chacun s'occupant d'un service précis, plutôt qu'un gros serveur qui fait tout.
* une partie des calculs est effectuée **par le client, dans son navigateur**, cas les scripts **javascript** sont executés dans le navigateur, et pas sur les serveurs.



On peut modifier le serveur dans le fichier ```rpicamera.py``` dans ```aiortc-examples``` (car il s'occupe déjà de streamer la video et de servir un fichier html), en lui ajoutant le code de ```websocket.html```.

![schema](fig/schema_websocket_telepresence.png)



**Si ça ne marche pas**, utiliser une solution de repli avec VLC.





BONUS:
------

*  Node-Red
*  tensorflow.js dans le browser: [real-time-object-detection](https://hackernoon.com/tensorflow-js-real-time-object-detection-in-10-lines-of-code-baf15dfb95b2)
*  uv4l+webrtc+tensorflow lite+MQTT (pour récuperer infos detection): https://www.linux-projects.org/uv4l/tutorials/video-tracking-with-tensor-flow/
* interface utilisateur avec notebook jupyter cf [NVIDIA-AI-IOT/jetbot](https://github.com/NVIDIA-AI-IOT/jetbot/tree/master/notebooks/teleoperation)



FAQ:
------

* TODO prof: tester la fusion de aiortc et websocket ; changer les images.

* bug aiortc: sous Firefox dans le PC de la salle TP, on ne voit pas le flux video sur le serveur aiortc. 
   *  (depuis un browser en local sur la raspi. Il faut réinstaller chromium-browser.  FAILED: chromium-browser, dillo)
   *  sur lan: avec autres browser lightweight. SUCCESS: avec chrome. FAIL: midori, dillo, netsurf. 
   *  (tunnel SSH)
   *  (deprecated: embed vlc plugin in web page https://wiki.videolan.org/VLC_HowTo/Stream_to_a_website_(simple_version)/  )
   *  (port filtering: check which port is used. Can change it ? Ask which port is filtered. Which port is used by vlc ?O )




OLDIES:
------- 


### streming mjpeg opencv

Sur la raspberry pi on fait tourner un serveur web, écrit en Python.
Ce serveur web va streamer le flux vidéo capté par la camera du raspi vers un client qui se connecte.
Le client se trouve sur une autre machine sur le même réseau
que la raspi, et se connecte au serveur.

1.  Copiez sur votre raspi les fichiers mjpeg_server_base.py et Camera.py à partir de [code/UI/mjpeg-server](../code/UI/mjpeg-server)
1.  Connectez-vous sur la raspi, notez son IP, et lancez le serveur mjpeg_server_base.py
1.  Sur une machine de bureau, lancez un navigateur connectez-vous sur le serveur grâce à son IP.


Liens:

*  [reefwingrobotics](http://reefwingrobotics.blogspot.com/2017/03/streaming-video-from-raspberry-pi-camera.html), 
*  [mjpeg_streamer.py](https://walchko.github.io/blog/Vision/Python-MJpeg-Streamer.html): comparatif BaseHTTPServer/Flask/bottle. problem: bottle dépend de gevent
*  https://github.com/MomsFriendlyRobotCompany/opencvutils
*  https://bottlepy.org/docs/dev/ https://github.com/bottlepy/bottle/

