Tracking visuel
------


On trouve de la vision dans les TP suivants : 


| nom | année | lien |  module  | hardware | software | code test |
| -------- | -------- | -------- | -------- | -------- | -------- | -------- |
| tracking_visuel  | | [tracking_visuel](tracking_visuel.md)  | mctr | raspi | opencv | test_track_wheezy.py |
| convoyeur qr code | | [convoyeur](tp_vision_nn_reseau.md) | ER3 | pc/webcam | html5/js | SuperSimpleExampleServer.py |
| air hockey? | | | |cmucam3 | | |
| seed watch | 18-19 | [tp-vision-whycon](tp-vision-whycon.md) | ER3 | pc/webcam | whycon x86 | client.py |
| mobile_robot2 | 18-19 | [tp-vision-whycon](tp-vision-whycon.md)  | ER3 | raspi | whycon ARM| ? |



cmucam 3
------

*   cmucam3 doc:
   *  [Hardware-serial](http://www.cmucam.org/projects/cmucam3/wiki/Hardware-serial)
   *  [Hardware-power](http://www.cmucam.org/projects/cmucam3/wiki/Hardware-power)
*   Example arduino/cmucam3 [project](https://www.thisismyrobot.com/2008/09/details-of-cmucam.html) 
   *  sample [code](https://www.robotshop.com/letsmakerobots/files/robot006.txt)
   *  [video](https://www.youtube.com/watch?v=kWG2EbhZUgc)
*   connection to pc:
   *   ftdi [TTL-232R](http://www.ftdichip.com/Support/Documents/DataSheets/Cables/DS_TTL-232R_CABLES.pdf) cable
   *   black brown red orange yellow green = gnd cts vcc tx rx rts (cts=Clear to Send; rts=request to send)
   *   [minicom](http://www.cmucam.org/projects/cmucam3/wiki/Minicom)
       >   miniterm.py /dev/ttyUSB0 115200

capteurs optique de souris
------

Les capteurs présents dans les souris récentes sont en fait des mini caméras
de résolutions 9x9 juqu'à 30x30.
En fonction du protocole utilisé (par exemple PS2), il peut être simple de 
récupérér l'image.

* A2610 http://tim.cexx.org/?p=613, [youtube](https://www.youtube.com/watch?v=SXQfT7c-9rU)
* A5020 http://frenki.net/2013/12/convert-optical-mouse-into-arduino-web-camera/
* A5020 notice technique [A5020E.pdf](notices_techniques/A5020E.pdf), [youtube](https://www.youtube.com/watch?v=Ix6532mrIKA)
* A3080 breakout board [github](https://github.com/acrobotic/Ai_Breakout_ADNS3080), [youtube](https://www.youtube.com/watch?v=o4IfkTGKw3c)

Liens
------

*   microsoft ELL [github](https://github.com/Microsoft/ELL)
*   aiy vision bonnet , raspi zero+bonnet , https://hackaday.com/2019/01/31/ai-on-raspberry-pi-with-the-intel-neural-compute-stick/
*   rccar , raspi+remote pc https://github.com/hamuchiwa/AutoRCCar
*   android tensorflow (mobile/lite) + socket ?
*   appinventor 
*   processing
*   pixy (=cmucam>=4)
*   fwd
