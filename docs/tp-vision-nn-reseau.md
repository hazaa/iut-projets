TP vision/réseau
======

Dans ce TP nous utiliserons la vision pour commander des moteurs.
La vision sera prise en charge par le pc de bureau, relié à une webcam usb (ou à une kinect).

Les moteurs sont un servomoteur et un moteur à courant continu.
Leur commande sera effectuée par un arduino, comme dans le TP précédent.




Commande de servos depuis python via l'interface série
------
Ouvrez un terminal, et l'éditeur de texte geany.
On veut établir un dialogue série avec l'arduino, il faut donc envoyer des données sur l'interface série depuis le PC sous python, et côté arduino il faut décoder ces informations et les utiliser.

On va utiliser l'exemple suivant, où on transmet le numéro d'une LED et son niveau d'éclairement.
Les fichiers se trouvent dans le répertoire [code](https://gitlab.com/hazaa/iut-projets/tree/master/code).

*    Côté python, on envoie des informations avec le programme SerialNumericParser.py. Les informations sont la chaîne de caractères "s5,200\n". Le caractère "s" annonce le début du message. Le caractère de saut de ligne "\n" marque la fin du message. L'information utile est donc constituée de deux entiers, 5 et 20, séparés par une virgule.
*    Côté arduino, on lit les informations avec le programme SerialNumericParser.ino, et on commande la LED en fonction des informations recues. On renvoie le tout à l'envoyeur, pour vérification, avec la commande Serial.print("...") .


>    **Question**: téléchargez le programme .ino vers l'arduino. Ouvrez le moniteur série et taper "s5,200"

>    **Question**: fermez le moniteur série et lancez le programme .py sur votre PC. 

Puis, on veut transformer cet exemple pour commander un servomoteur et le moteur à courant continu PmodHB5, en envoyant un angle entier compris entre 0 et 180, ainsi qu'une vitesse de 
	rotation comprise entre 0 et 100.

>    **Question**: modifier l'exemple ci-dessus, ainsi que la commande du servo vue plus haut pour commander le servo et le moteur à courant continu depuis le pc.



#  Vision, détection d'objets

On va utiliser un algorithme de vision artificielle pour détecter des [QR](https://en.wikipedia.org/wiki/QR_code)-codes.
Pour cela connectez-vous sur le site de la [demo](https://schmich.github.io/instascan/) qui utilise la librairie [instascan](https://github.com/schmich/instascan), et regardez un peu ce qui se passe en présentant un QR-code à la webcam.
On peut générer des QR-code facilement, par exemple avec l'application [QR code generator](http://www.the-qrcode-generator.com)

L'idée est la suivante: quand instascan va reconnaître un objet, le pc enverra une commande à l'arduino pour qu'il enclenche un moteur. 
Pour cela il faut récupérer la description de l'objet reconnu. Pour cela on utilise une modification de la demo ci-dessus.

>    **Question**: connectez-vous sur le site indiqué par l'enseignant ([http://10.14.72.146:8000](http://10.14.72.146:8000)). Branchez la caméra et vérifiez que la reconnaissance de QR-code fonctionne aussi.

Puis on veut récupérer la description du QR-code reconnu grâce à un système de websocket.
On aura besoin d'installer des programme python. 
Récupérez les scripts:  SuperSimpleExampleServer.py et SimpleWebSocketServer.py dans le répertoire [code/vision_track/websocket](https://gitlab.com/hazaa/iut-projets/tree/master/code/vision_track/websocket)
Vous trouverez des exemples de QR-codes dans [code/vision_track/qr/img](https://gitlab.com/hazaa/iut-projets/tree/master/code/vision_track/qr/img)
Le schéma de communication entre les différents programmes est le suivant:

![convoyeur](fig/schema_websocket_reseau.png)


>    **Question**: ouvrez un terminal et lancez le serveur de websocket en tapant python SuperSimpleExampleServer.py --port 9999
>    Puis faites "reload" dans votre navigateur firefox, avec le site indiqué ci-dessus.
>    Que se passe-t-il ?     

#  Vision, détection d'objets et commande des moteurs

On doit maintenant modifier le script SuperSimpleExampleServer.py pour qu'il envoie une commande au moteur
quand un objet est reconnu par instascan. Pour cela on va créer un nouveau programme SuperSimpleExampleServer.py qui sera une copie de SuperSimpleExampleServer.py, et que vous allez modifier.

>    **Question**: écrivez le GRAFCET/l'algorithme du comportement attendu.

>    **Question**: utilisez les programmez précédents pour programmer le GRAFCET/algorithme en python (conseil: partir de SuperSimpleExampleServer.py et ajoutez-y la commande des moteurs à partir des programmes ci-dessus).

# FAQ

| Question | Réponse | 
| -------- | -------- | 
| je travaille avec mon pc portable. Quand je clique sur http://10.14.72.146:8000 il ne se passe rien | c'est normal votre pc n'est pas sur le réseau filaire de la salle TP. Téléchargez le répertoire [code/vision_track/qr/](https://gitlab.com/hazaa/iut-projets/tree/master/code/vision_track/qr/) sur votre PC. Ouvrez un firefox, faites Fichier>Ouvrir un fichier, puis cliquer sur qr/index.html  | 
| Je veux lancer le script depuis windows | lancez un terminal avec cmd.exe; allez dans le répertoire où se trouve votre script avec la commande cd CHEMIN ; puis tapez py monscript.py ; [explications ici](https://docs.python.org/3/using/windows.html?highlight=shebang#python-launcher-for-windows) |


Extensions
------

*    la source video n'est plus une webcam mais un téléphone portable. Utiliser une application telle que ip webcam. Il faut aussi modifier le code su site web imagenet. 
*    ajouter un arrêt d'urgence.
*    deprecated: Aller sur le site de [websocket-client](https://github.com/websocket-client/websocket-client) et cliquer sur "Clone or Download" pour récupérer le fichier zip, que vous aller décompacter dans votre répertoire de travail.  [ws-client.py](https://gitlab.com/hazaa/iut-projets/tree/master/code/vision_track/ws-client.py) sur gitlab dans le répertoire code/vision_track.
*    qr: accélerer; creation de qr-codes: [qtqr](https://packages.debian.org/stretch/qtqr), [python-qrtools](https://packages.debian.org/stretch/python-qrtools)



## instascan + téléphone portable

C'est possible mais plus compliqué qu'avec une webcam:

*    télécharger les deux répertoire code/vision_track/websocket et code/vision_track/qr avec leur contenu, sur votre machine de la salle TP.
*    lancer le serveur websocket comme ci-dessus:

    >    python SimpleExampleServer.py --port 9999

*    brancher votre téléphone sur le port USB de la machine de bureau et faites un partage de connexion. Trouvez l'IP de votre téléphone en cherchant dans les paramètres. On la note IP1.
*    Trouvez l'IP de votre machine de bureau sur le réseau crée par le téléphone, en ouvrant un terminal et en tapant /sbin/ifconfig. On la note IP2.
*    chargement du fichier html dans le navigateur: 
    *  option 1:  lancer un serveur web sur votre machine de bureau en vous plaçant dans le répertoire /websocket:

	      >    python -m SimpleHTTPServer
	
	sur votre téléphone, ouvrez un navigateur et connectez-vous sur la page http://IP2:8000
	
	* option 2: copier les répertoires /websocket et /qr sur votre téléphone

*    sur votre téléphone, cliquer sur websocket_qr.html , dans la case URL, tapez ws://IP2:9999 , puis cliquer sur "connect"
*    normalement, le serveur websocket affiche "connect". Quand vous passez un QR code devant la caméra du téléphone, le serveur websocket doit afficher le contenu du QR-code.

## deeplearnjs + imagenet 

On va utiliser un algorithme d'[apprentissage automatique](https://fr.wikipedia.org/wiki/Apprentissage_automatique) pour détecter les objets présents dans une image.
Pour cela connectez-vous sur le site de la demo [imagenet](http://deeplearnjs.org/demos/imagenet/), qui utilise la librairie [deeplearnjs](http://deeplearnjs.org), et regardez un peu ce qui se passe.

L'idée est la suivante: quand imagenet va reconnaître un objet, le pc enverra une commande à l'arduino pour qu'il enclenche un moteur. 
Pour cela il faut récupérer la description de l'objet reconnu. Pour cela on utilise une modification de la demo imagenet ci-dessus.

>    **Question**: connectez-vous sur le site indiqué par l'enseignant. Branchez la caméra et trouvez des objets qui sont facilement reconnus par imagenet (exemple: les photos dans l'onglet 'input' de la démo).


*    raspi zero+ vision chip: [google AI vision](https://aiyprojects.withgoogle.com/vision)
*    raspi 3 + intel movidus stick https://github.com/leswright1977/RPi3_NCS2
*    camera cmucam pixy 

# Crédits

*    images:
     *    [http://openclipart.org](http://openclipart.org), contributeurs: krypt (Webcam), jean_victor_balin (Long Arrow Right, Long Arrow Up)
         
