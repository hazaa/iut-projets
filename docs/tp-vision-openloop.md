TP vision simple
======

Idée: positionner précisément la caméra au début et à la fin à l'aide de la caméra, puis lancer une boucle de mouvement des moteurs en boucle ouverte.

Algorithme: 

1.  sur le PC de bureau, ouvrir un terminal (Ctrl+Alt+T), puis lancer un programme d'affichage de l'image de la webcam (cf ci-dessous).
1.  ouvrir un autre terminal, executer un script python qui permet à l'utilisateur de positionner la caméra au début et à la fin de la séquence avec les flèches.
1.  fermer l'affichage de l'image de la webcam
1.  revenez au terminal où tourne le script python, et lancer la phase d'acquisition des images


Afficher l'image camera depuis le terminal
------

Brancher la camera usb, puis ouvrir un terminal (Ctrl+Alt+T), puis:

>   cheese

ou:
 
>   guvcview

Vous devez voir apparaître l'image provenant de la camera.

Script python 
------

Pré-requis: [TP Python](tp-python.md)

La saisie au clavier en Python:

```python
i=raw_input("prompt")
```
Mais cette saisie pose problème car il faut taper entrée à la fin. Voir ci-dessous pour des méthodes plus adaptées.

Puis vous utiliserez une boucle de type while pour lire.
Pour sortir de la boucle, un test sera utile, par exemple ci-dessous, qui appelle break. (NB: break permet de sortir de la boucle while)

```python
if i=='F': break
```

Algorithme:

1.   demander à l'utilisateur la taille de la grille (par exemple 5x5)
1.   demander à l'utilisateur d'utiliser les touches 2/4/6/8 pour positionner le robot à son point initial, ou F quand c'est fini. Par exemple quand 2 est selectionne, le moteur de l'axe X tourne de 10 pas.
1.   idem pour le point final.
1.   retourner automatiquement au point de départ
1.   calculer les différentes étapes du balayage de la grille
1.   attendre que l'utilisateur lance la phase d'acquisition automatique des images

BONUS: utiliser [curses](http://gnosis.cx/publish/programming/charming_python_6.html) ou tkinter pour faire une interface graphique plus sympathique.


Capture image depuis la ligne de commande
------

Ouvrir un terminal (Ctrl+Alt+T) puis:

>   ffmpeg -f video4linux2 -s 640x480 -i /dev/video0 -ss 0:0:2 -frames 1 out.jpg

NB: on peut remplacer out.jpg par ./$(date +\%Y\%m\%d\%H\%M).jpg

ou:

>   guvcview -n 1  -t 1/15 -x 640x480  -i out.jpg


NB: Depuis Python, on peut appeler l'un de ces executables:

Avec ffmpeg:
```python
import subprocess as sp
import time
cmd = "ffmpeg -f video4linux2 -s 640x480 -i /dev/video0 -ss 0:0:2 -frames 1 out.jpg"
proc = sp.Popen([cmd], shell=True, stdin=None, stdout=None, stderr=None)
```


Avec guvcview :
```python
import subprocess as sp
import time
proc = sp.Popen(["guvcview -n 1 -t 1/15 -x 640x480 -i out.jpg"], shell=True, stdin=None, stdout=None, stderr=None)
time.sleep(5)
proc2 = sp.Popen(["killall -s SIGINT guvcview"], shell=True, stdin=None, stdout=None, stderr=None)
```

Variante avec guvcview:

```python
import subprocess as sp
import time
path = "/home/etudiant/Bureau"; out = "-i %s/out.jpg"%path
proc = sp.Popen(["/usr/bin/guvcview", "-n 1", "-t 1/15", "-x 640x480", out], shell=False, stdin=None, stdout=None, stderr=None)
time.sleep(5)
proc.send_signal(sp.signal.SIGINT)
```

Liens: 

*   doc [guvcview](http://guvcview.sourceforge.net/)
*   doc python [subprocess](https://docs.python.org/2/library/subprocess.html)
*   killall: https://askubuntu.com/questions/468282/guvc-video-capture-commandline

Lecture des touches clavier pressées en Python
------

Première solution, avec tkinter (voir [forum](https://stackoverflow.com/questions/17815686/detect-key-input-in-python)):
```python
try:                        # In order to be able to import tkinter for
    import tkinter as tk    # either in python 2 or in python 3
except ImportError:
    import Tkinter as tk


def event_handle(event):
    # Replace the window's title with event.type: input key
    root.title("{}: {}".format(str(event.type), event.keysym))


if __name__ == '__main__':
    root = tk.Tk()
    event_sequence = '<KeyPress>'
    root.bind(event_sequence, event_handle)
    root.bind('<KeyRelease>', event_handle)
    root.mainloop()
```


Deuxième solution, avec curses (voir [forum](https://stackoverflow.com/questions/24072790/detect-key-press-in-python?noredirect=1&lq=1)):
```python
import curses

def main(win):
    win.nodelay(True)
    key=""
    win.clear()                
    win.addstr("Detected key:")
    while 1:          
        try:                 
           key = win.getkey()         
           win.clear()                
           win.addstr("Detected key:")
           win.addstr(str(key)) 
           if key == os.linesep:
              break           
        except Exception as e:
           # No input   
           pass         

curses.wrapper(main)
```


Extensions
------

pour la vision:

*  depuis la ligne de commande: vlc, avconv, uvccapture
*  en python: [pygame](https://www.pygame.org) [stackoverflow](https://stackoverflow.com/questions/39003106/python-access-camera-without-opencv) (léger) ; opencv (lourd).


Interface utilisateur:

*  console: urwid
*  pyqt, qt [designer](http://pyqt.sourceforge.net/Docs/PyQt5/designer.html)

sur la raspberry pi: cf [TP raspi](tp-raspi.md)
