Vision avec Whycon
======

Le logiciel [Whycon](https://github.com/gestom/whycon-orig) permet 
à une mini-ordinateur de type Raspberry muni d'une caméra de repérer la position 
d'une cible prédéfinie dans une image, avec une fréquence élevée.

Ce logiciel fonctionne aussi avec un PC de bureau muni d'une Webcam.


[![](http://img.youtube.com/vi/KgKrN8_EmUA/0.jpg)](http://www.youtube.com/watch?v=KgKrN8_EmUA "Whycon Demo")

Whycon utilise des cibles en forme de cercle dont il calcule la position dans l'image: [1 cible pdf](https://gitlab.com/hazaa/iut-projets/tree/master/code/vision_track/whycon/circle.pdf), [4 cibles svg](https://github.com/lrse/whycon/blob/master/patterns/circle-medium.svg)


Sur PC desktop
------

Installation:

*  télécharger et dezipper whycon dans le répertoire data_etudiant
*  Ouvrir un terminal (Ctrl+Alt+T). Taper: cd Bureau/data_etudiants/whycon-orig-master/src
*  Compiler en tapant: make
*  Puis cd ../bin car l'executable se trouve dans whycon-orig-master/bin
*  créez un répertoire avec la commande: mkdir output
*  (si la compilation échoue, aller sur gitlab dans iut-projets/code/vision_track/whycon et télécharger l'éxecutable linux whycon_x86. lien direct [code/vision_track/whycon](https://gitlab.com/hazaa/iut-projets/tree/master/code/vision_track/whycon/whycon_x86))

Démarrage du serveur:

*   Brancher la webcam.
*   Imprimer ou affichez à l'écran la cible circle.pdf
*   Ouvrir un terminal (Ctrl+Alt+T)
*   cd Bureau/data_etudiants/whycon-orig-master/bin
*   Exectuer le programme: ./whycon /dev/video0 1
*   Si vous ne voulez pas l'interface utilisateur:     ./whycon /dev/video0 1 nogui

Puis lancer un programme client qui se connecte au serveur et écoute:


*   Ouvrir un terminal (Ctrl+Alt+T)
*   Télécharger le script python client.py sur le gitlab, dans iut-projets/code/vision_track/whycon 
*   Le lancer depuis le terminal: python client.py


Sur Raspberry Pi
------

Faites d'abord le [TP raspberry pi](tp-raspi.md).


Installation:

*  télécharger et dezipper whycon dans le répertoire local/bin
*  Ouvrir un terminal (Ctrl+Alt+T). Taper: cd local/bin/whycon-orig-master/src
*  Compiler en tapant: make
*  Puis cd ../../bin car l'executable se trouve dans whycon-orig-master/bin
*  créez un répertoire avec la commande: mkdir output
*  (si la compilation échoue, aller sur gitlab dans iut-projets/code/vision_track/whycon et télécharger l'éxecutable linux whycon_arm. lien direct [code/vision_track/whycon_arm](https://gitlab.com/hazaa/iut-projets/tree/master/code/vision_track/whycon_arm))

Démarrage du serveur:

*   Demandez à l'enseignant d'activer la webcam: sudo modprobe bcm2835-v4l2 
*   Imprimer ou affichez sur l'écran du PC la cible circle.pdf
*   cd local/bin/whycon-orig-master/bin
*   Exectuer le programme: ./whycon /dev/video0 1
*   Si vous ne voulez pas l'interface utilisateur, pour aller plus vite:     ./whycon /dev/video0 1 nogui

Puis lancer un programme client qui se connecte au serveur et écoute:

*   Ouvrir un autre terminal (Ctrl+Alt+T)
*   Télécharger le script python client.py sur le gitlab, dans iut-projets/code/vision_track/whycon 
*   Le lancer depuis le terminal: python client.py

Système de coordonnées:
------

*   Lire dans la doc whycon la section [setting-up-the-coordinate-system](https://github.com/gestom/whycon-orig#setting-up-the-coordinate-system)
*   En résumé: après avoir lancé le programme, il faut placer 4 marqueurs fixes qui délimitent l'espace de travail plus un marqueur mobile à l'intérieur du rectangle délimité par les 4 marqueurs fixes, qui définissent le repère (0,0),(1,0),(0,1),(1,1). 
*   Quand tous ces marqueurs sont présents, il faut taper "a".
*   On obtient alors les coordonnées du marqueur mobile.


Exercices
------

Pour les exercices ci-dessous, on fera une copie du script python
ci-dessous en modifiant son nom. 
Puis on modifiera cette copie pour répondre à la question.

Méthode: on fait un changement dans le script, on le teste. Puis on recommence.

1.   Calculer à chaque itération la distance D algébrique (D>0 si la cible est à droite du centre de l'image, sinon D<0) entre le centre de l'image et le centre de la cible.
1.   A partir de D, calculer à chaque itération une force de rappel qui permettrait de ramener la cible au centre de l'image.



##  Projet Seed Watch uniquement

Système : PC de bureau

Moteurs: stepper

Objectif: le client donne une coordonnée (i,j) dans [0,imax[x[0,jmax[. Le robot doit positionner la caméra au dessus.

Liens utiles Projet ER3 [convoyeur QR code](tp-vision-nn-reseau.md)


##  Projet Robot mobile uniquement

Système: PC de bureau (option: Raspberry Pi)

Moteurs: MCC

Camera: elle peut être fixe ou embarquée sur le robot.

Objectifs: 

*   la commande des deux moteurs doit permettre de positionner la cible dans une bande [y0-dy,y0+dy] de l'image.
*   peut-on faire suivre au robot une ligne composée de cibles qui mène à un objectif ?
*   peut-on récuperer une information de distance par rapport à la cible ?

#  Liens 

*   version ROS [github](https://github.com/lrse/whycon)
*   https://www.researchgate.net/publication/311811749_WhyCon_An_Efficent_Marker-based_Localization_System
