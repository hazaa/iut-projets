Interface Homme Machine avec Android
------

Dans ce TP on va utiliser un téléphone Android comme une Interface Homme Machine (IHM) 
pour envoyer des commandes à un micro-controlleur, via le wifi ou le bluetooth.
Le micro-controlleur commandera des Leds, des moteurs, etc... 
La création de l'app Android sera faite grâce à appinventor, une méthode
NB: [appinventor](http://ai2.appinventor.mit.edu) nécessite un compte gmail


*    [Commander un Arduino Yun](#commander-un-arduino-yun)
*    Commander un ESP8266: avec [Lua](#commander-un-esp8266-avec-lua), avec [Arduino IDE](#commander-un-esp8266-avec-arduino-ide), avec [MicroPython](#commander-un-esp8266-avec-MicroPython)
*    [Commander un Arduino Uno avec bluetooth](#commander-un-arduino-uno-avec-bluetooth)

Commander un Arduino Yun
------

*   A faire par l'enseignant: [configurer le Yun](https://www.arduino.cc/en/Guide/ArduinoYun) (turn on REST API, reset password).
    *    alimenter par le cable usb
    *    se connecter en wifi sur le réseau Arduino Yun-***
    *    se connecter sur http://192.168.240.1 et se logguer (mdp: a***ino)
        *    si echec: miniterm.py /dev/ttyACM0            
    *    cliquer sur "configure", mettre REST API access sur "open"

>    **Question**: lancer Arduino IDE, ouvrir Fichier/Exemple/Bridge/Bridge ;
>        Puis choisir le type de carte "Arduino Yun".
>        Puis choisir le bon port (/dev/ttyACM0, ou port réseau "arduino at 192.168.240.1"), puis téleverser.

Une fois le programme Bridge.ino téléversé sur l'arduino, on peut lire les
états des pins de l'arduino avec un simple navigateur web. On peut aussi les
commander (par exemple passer le pin 13 de l'état 0 à l'état 1).

>    **Question**:  avec votre navigateur Firefox: 
>   *    lire l'état du pin 13 (il doit être éteint): http://192.168.240.1/arduino/digital/13
>   *    écrire: http://192.168.240.1/arduino/digital/13/1 puis http://192.168.240.1/arduino/digital/13/0
>   *    Que se passe-t-il avec la led sur la carte Yun ?
>   *    (optionnel: le faire depuis un terminal avec le programme curl)

Finalement, on va utiliser un appareil Android pour créer une IHM (interface homme machine), qui
servira à commander l'arduino. On pourrait se contenter d'utiliser le navigateur
de l'appareil Android pour faire comme ci-dessus. Mais on peut créer une IHM
qui va lancer des requête http comme ci-dessus. Pour cela on utilise [appinventor](http://appinventor.mit.edu/explore/),
qui utilise un langage graphique facilitant la programmation.


>    **Question**: adapter le tutoriel [randomnerdtutorials](https://randomnerdtutorials.com/esp8266-controlled-with-android-app-mit-app-inventor/) en tenant compte de la forme des requêtes vues ci-dessus (exemple: pour fixer le port 13 à la valeur 1: http://192.168.240.1/arduino/digital/13/1)



*    Documents utiles: utilisation du Yun avec la [librairie Bridge ](https://www.arduino.cc/en/Tutorial/Bridge)


Commander un ESP8266 avec Lua
------

ATTENTION: pas de 5V sur les pins du nodemcu.


*    A faire par l'enseignant: 
    *    télécharger esptool (pip install esptool) ou [nodemcu-pyflasher](https://github.com/marcelstoer/nodemcu-pyflasher)  
    *    télécharger le firmware:   https://nodemcu-build.com/, exemple dans le répertoire [code](https://gitlab.com/hazaa/iut-projets/blob/master/code/)
    *    flasher.   

Testons le fonctionnement du nodemcu:

>    **Question**: testez le fonctionnement avec l'une des méthodes ci-dessous. 

*    avec [miniterm](https://learn.adafruit.com/adafruit-huzzah-esp8266-breakout/using-nodemcu-lua): connecter miniterm, faire reset avec le nodemcu, envoyer des commandes lua.
*    avec [esplorer](https://esp8266.ru/esplorer/#tutorials)


Puis nous suivons le déroulé des questions de la partie Arduino Yun.


>    **Question**: suivre le tutoriel [randomnerdtutorials](https://randomnerdtutorials.com/esp8266-controlled-with-android-app-mit-app-inventor/) pour téléverser le fichier [lua](https://raw.githubusercontent.com/RuiSantosdotme/Random-Nerd-Tutorials/master/Projects/ESP8266_Controller.lua)

>    **Question**: faire la section "navigateur Firefox" dans la partie Arduino Yun ci-dessus.

>    **Question**: suivre le tutoriel [randomnerdtutorials](https://randomnerdtutorials.com/esp8266-controlled-with-android-app-mit-app-inventor/) pour créer la partie appinventor.

Documents utiles:   
 
*  [nodemcu](http://nodemcu.com/index_en.html), [github](https://github.com/nodemcu/nodemcu-firmware), [readthedocs](https://nodemcu.readthedocs.io/en/).
*  flasher le [firmware](https://nodemcu.readthedocs.io/en/master/en/flash/)
*  matériel:   [limpulsion](http://www.limpulsion.fr/web/resultatRecherche.php?re=esp)

Commander un ESP8266 avec Arduino IDE
------

ATTENTION: pas de 5V sur les pins du nodemcu.

A la place de l'interpréteur Lua, nous allons directement coder en C sur
Arduino IDE et téléverser le programme sur l'ESP8266.

>    **Question**: installer Arduino IDE 1.8 et l'extension ESP8266 en suivant ces [instructions](https://github.com/esp8266/Arduino#installing-with-boards-manager). Ouvrir Arduino IDE, configuer la carte (NodeMCU 1.0) et le port (/dev/ttyUSB0).

>    **Question**: lancer Arduino IDE, ouvrir Exemple/ESP8266/Blink et téléverser. Faire le montage correspondant au step 6 du tutoriel [instructables](http://www.instructables.com/id/Programming-ESP8266-ESP-12E-NodeMCU-Using-Arduino-/). La led sur le module doit clignoter, ainsi que la led que vous avez branchée.

>    **Question**: création d'un serveur http sur l'esp8266. Choisir un réseau wifi sur lequel le serveur va se connecter. Vous pouvez créer un réseau avec votre téléphone, et choisir le password. Puis sur Arduino IDE, ouvrir Exemple/ESP8266WebServer/HelloServer. Modifiez-le avec le SSID et le password de votre réseau wifi. Téléversez ce programme. Avec Arduino IDE lancez un moniteur série et récupérez l'adresse IP de l'ESP8266. Puis avec le navigateur de votre téléphone, connectez-vous sur cette IP. Que se passe-t-il ?

>    **Question**: serveur http sur l'esp8266 pour controller une led. Avec le programme de la question précédente, il est possible de commander une led. Mais cela ne suffit pas pour en controller plusieurs. Pour cela, suivre le tutoriel [randomnerdtutorials](http://randomnerdtutorials.com/esp8266-web-server/).

>    **Question**: utiliser app inventor pour controller la led commandée par le serveur http précédent.


*    Documents utiles: 
   *   https://github.com/esp8266/Arduino#installing-with-boards-manager
   *   led + arduino IDE: [instructables](http://www.instructables.com/id/Programming-ESP8266-ESP-12E-NodeMCU-Using-Arduino-/)
   *   led + http server + arduino IDE: [randomnerdtutorials](http://randomnerdtutorials.com/esp8266-web-server/), [teachmemicro](https://www.teachmemicro.com/simple-nodemcu-web-server/)
   *   http server + arduino IDE+ websocket [gist](https://gist.github.com/bbx10/667e3d4f5f2c0831d00b)

*   Extensions: 
   *   hostpot: configurer l'ESP en mode 'access point':  WiFi.mode(WIFI_AP); par exemple dans le projet [hackster.io](https://www.hackster.io/rayburne/esp8266-access-point-using-arduino-ide-19f632), voir [exemple](../code/wifi_http_server_access_point/wifi_http_server_access_point.ino)

Commander un ESP8266 avec MicroPython
------
ATTENTION: pas de 5V sur les pins du nodemcu.

En cours...

*   https://medium.com/@rxseger/esp8266-first-project-home-automation-with-relays-switches-pwm-and-an-adc-ad25f317c74f
*   avec websocket (pour plus de réactivité), comme http://micropython.org/webrepl/
*   mode 'access point' [docs.micropython](https://docs.micropython.org/en/latest/esp8266/esp8266/tutorial/network_basics.html)

Commander un Arduino Uno avec bluetooth
------

*    Branchement arduino: prendre un Meduino avec la MeBase. Brancher le module bluetooth sur le port 4 de la MeBase.
*    Installer la librairie [Makeblock](https://gitlab.com/hazaa/iut-projets/blob/master/code/Makeblock) dans le répertoire arduino de votre machine. 
*    Tester le bluetooth:
    *    Téleverser l'exemple Exemple/Makeblock_Library/Me_bluetooth après avoir supprimé toute la partie: if(Serial.available()){...}
    *    Ouvrir la console série dans Arduino IDE pour voir ce qui vient de l'arduino.  
    *    Avec l'app [LED Controller](https://create.arduino.cc/projecthub/user206876468/arduino-bluetooth-basic-tutorial-d8b737#toc-how-to-use-the-app-4) qu'on peut télécharger directement sur [github](https://github.com/Mayoogh/Arduino-Bluetooth-Basic): 
        *     Installez cette app sur votre téléphone Android.
        *     Connectez votre téléphone avec le module bluetooth (device: Makeblock, code 1234)
        *     Lancez l'app et appuyez sur les boutons ON et OFF. Que se passe-t-il sur la console série du logiciel Arduino IDE ?    
    *    (optionnel: utiliser l'appli [mBlock](http://learn.makeblock.com/en/software/) sur android pour envoyer des informations sur le bluetooth. On doit voir des données apparaître sur la console série. (NB: obtenir un apk sans utilise google play: [apps.evozi.com](https://apps.evozi.com/apk-downloader))

Maintenant qu'on a testé la communication Android/Arduino grâce au bluetooth, on va pouvoir améliorer l'IHM côté Android.
On peut créer une IHM qui va communiquer en bluetooth comme ci-dessus. 
Pour cela on utilise [appinventor](http://appinventor.mit.edu/explore/), qui utilise un langage graphique facilitant la programmation.

>    **Question**: suivre le tutoriel: [instructables.com](http://www.instructables.com/id/How-to-control-servo-motor-from-android-app/)

>    **Question**: modifiez ce tutoriel pour commander des led comme dans la partie Yun.

*    Documents utiles: 
   *   makeblock + moteur DC: [makeblock](http://makeblock.cc) avec Me-base et drivers correspondants: v2.0?, [v3](https://github.com/Makeblock-official/Makeblock-Libraries)
   *   fichiers [Eagle](https://github.com/Makeblock-official/Me-Base-Shield-v2.0-Egale-File) de la carte Me-Base
   *   détail du schéma de la PCB au niveau du connecteur [RJ11 N°1](fig/capture_kicad_mebase.png)


Extensions 
------
*    UPSTI: [python+ESP](https://www.upsti.fr/documents-pedagogiques/python-et-les-microcontroleurs-esp)
*    traiter les cas 'sta' (station) et 'ap' (access point).
*    socket: la méthode ci-dessus pour Yun et ESP8266 nécessite un serveur http et une interface REST. Si on veut rester plus bas-niveau, on peut:
    *    appinventor write socket: [stackoverflow](https://stackoverflow.com/questions/27993150/in-app-inventor-make-tcp-ip-client), [groups.google](https://groups.google.com/forum/#!category-topic/mitappinventortest/app-inventor-extensions/OCzEZC4FpEU)
    *    yun read socket: [stackexchange](https://arduino.stackexchange.com/questions/35682/how-to-run-tcp-socket-server-on-arduino-uno-wifi)
    *    esp8266 + read socket ?
*    android + processing + arduino+ face tracking: [circuitdigest](https://circuitdigest.com/microcontroller-projects/arduino-face-tracking-robot), http://android.processing.org/
*    wifi + [micropython +ESP8266](https://docs.micropython.org/en/latest/esp8266/esp8266/tutorial/network_tcp.html)
*   alternatives à appinventor:
    *    http://evothings.com/, utilise apache cordova. Exemple avec bluetooth [instructables](http://www.instructables.com/id/Remotely-Control-LED-using-HC-05-Bluetooth-Arduino/)
    *    https://thunkable.com
    
